import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from '../../../shared/local-storage.service';
import { AppConstant } from '../../../app.constant';
import * as _ from "lodash";
import { MasterService } from '../../../services/master.service';
import { PurchasesService } from '../../../services/purchases/purchases.service';
import { ProductallService } from '../../../products/productall.service';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { DateformatPipe } from '../../../pipes/dateformat.pipe';
import { PaymentService } from '../../../payment/payment.service';
import { Message } from 'primeng/primeng';
import { Router, NavigationExtras } from '@angular/router';
import { MessagesService } from '../../../shared/messages.service';
import { FeaturesService } from '../../../services/features.service';

@Component({
  selector: 'app-vendoradvance-addedit',
  templateUrl: './vendoradvance-addedit.component.html',
  styleUrls: ['./vendoradvance-addedit.component.scss']
})
export class VendoradvanceAddeditComponent implements OnInit {

  filteredContacts: any[];
  contactlist: any[];
  productList: any[];
  Invoicetype: any[];
  recno: String;
  seqid: String;
  contact: any;
  userstoragedata: any;
  heads: any;
  allpaymode: any[];
  msgs: Message[] = [];
  finyear: any;
  invoiceDate: Date;
  selectedInvoiceType: any = {};
  menuItems: MenuItem[];
  payterm: any[];
  Selectedpaymode: any;
  Allinvoicelist: any[];
  contactdtls: any;
  customerinvoices: any[];
  payingAmount: number;
  tempPayingAmount: any;
  sugesstedSplits: any[];
  paymentApplied: any;
  paymentDate: Date=new Date();
  receiptdate: any;
  Selectedpayterm: any;
  accconfig: any;
  paymentRemarks: any;
  contacthead: any;
  menuitems: any;
  paymentReference: any;

  constructor(
    private masterservice: MasterService,
    private paymentService: PaymentService,
    private purchasesService: PurchasesService,
    private LocalStorageService: LocalStorageService,
    private productservice: ProductallService,
    private dateFormatPipeFilter: DateformatPipe,
    private router: Router,
    private featureService: FeaturesService,
    private messageService: MessagesService
  ) {
    this.userstoragedata = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.heads = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.HEADS);
  }

  ngOnInit() {
    // this.seqgenerator();
    this.loadpayterm();
    this.loadbilllist();
    this.allbook();
    this.accountconfig();
    this.menuItems = [
      // label: 'Save',
      // items: [
      {
        label: 'Save & List', icon: 'fa-check', command: () => {
          this.AddPayment("");
        }
      },
      {
        label: 'Save', icon: 'fa-check', command: () => {
          this.AddPayment("createnew");
        }
      },
      {
        label: 'Cancel', icon: 'fa-times', command: () => {
          this.router.navigate(['/purchase/vendoradvance']);
        }
      }
      // ]
    ];

    var self = this;

    var data = {
      tenantid: this.userstoragedata.tenantid,
      status: "Active",
      contactype: "Vendor"
    };
    // load contact list
    this.featureService.contactGetAll(data)
      .then(function (res) {
        self.contactlist = res.data;
        console.log("Contact list: ", self.contactlist);
      });

  }

  // seqgenerator() {
  //   var data = {
  //     tenantid: this.userstoragedata.tenantid,
  //     "refkey": "PYMT"
  //   };
  //   this.masterservice.seqgenerator(data)
  //     .then((res) => {
  //       console.log("recno", res);
  //       this.recno = res.data[0].Nextseqno;
  //       this.seqid = res.data[0].seqid
  //     });
  // }

  accountconfig() {
    var data =
      {
        "feature": "Payment",
      }
    this.featureService.getacconfigList(data).then((res) => {
      if (res.status) {
        console.log("res", JSON.stringify(res));
        this.accconfig = res.data
      }
    });
  };

  bankledger: any;
  allbookaccs: any;

  allbook() {
    var data =
      {
        // tenantid: this.userstoragedata.tenantid,
        status: "Active"
      }
    this.bankledger = [];
    this.featureService.BookofAccList(data).then((res) => {
      console.log("res", res)
      this.allbookaccs = res.data;
      var books = _.filter(this.allbookaccs, { "accheadname": "Bank" });
      console.log("bankledger", books);
      this.bankledger.push(books);
      console.log("bankledgeraaaaaaaaaa", this.bankledger);
    })
  };

  loadbilllist() {
    var data = {
      tenantid: this.userstoragedata.tenantid,
      finyear: this.finyear.finyear,
    }
    this.purchasesService.getBillList(data).then((res) => {
      console.log("invoiceList", res.data);
      this.Allinvoicelist = _.filter(res.data, function (v: any) {
        v.paymentApplied = 0
        return (0 < v.balamount)
      })
    })
  }

  calcualtePayingSuggestion() {
    var self = this;
    self.tempPayingAmount = +(self.payingAmount);
    self.payingAmount = +self.payingAmount;
    if (isNaN(self.payingAmount)) {
      _.each(self.customerinvoices, function (v, key) {
        self.customerinvoices[key].paymentApplied = 0.00;
      });
    }

    if (self.tempPayingAmount != undefined) {
      self.tempPayingAmount = self.tempPayingAmount;
    }
    else {
      self.tempPayingAmount = 0;
    }

    if (self.tempPayingAmount > 0) {
      self.sugesstedSplits = _.filter(self.customerinvoices, (v, k) => {
        var c = false;
        self.customerinvoices[k].paymentApplied = 0;
        if (0 < self.tempPayingAmount) {
          var substractAmt: any = 0;
          if (self.customerinvoices[k].balamount >= self.tempPayingAmount) {
            substractAmt = self.tempPayingAmount;
            self.customerinvoices[k].paymentApplied = substractAmt;
            console.log("pyapplied", self.customerinvoices[k].paymentApplied);
            substractAmt = 0;
          }
          else if (self.customerinvoices[k].balamount > 0) {
            substractAmt = (self.tempPayingAmount - self.customerinvoices[k].balamount);
            self.customerinvoices[k].paymentApplied = self.tempPayingAmount - substractAmt;
            console.log("pyapplied2", self.customerinvoices[k].paymentApplied);
          }
          else {
            substractAmt = self.tempPayingAmount;
            self.customerinvoices[k].paymentApplied = substractAmt;
            console.log("pyapplied3", self.customerinvoices[k].paymentApplied);
          }
        }
        self.tempPayingAmount = substractAmt;
        c = true;
        return (c == true)

      });
    }
    else {
      self.tempPayingAmount = 0.00;
      _.each(self.customerinvoices, function (v, key) {
        self.customerinvoices[key].paymentApplied = 0.00;
      });
    }
    self.customerinvoices = [...self.customerinvoices];
  }

  calcualtepaymentApplied(event, index) {
    console.log(index);
    if (this.customerinvoices[index].paymentApplied == "") {
      this.customerinvoices[index].paymentApplied = 0;
    }
    var pmt = this.calcPayingTotal();
    if (this.payingAmount < this.customerinvoices[index].paymentApplied) {
      this.customerinvoices[index].paymentApplied = this.payingAmount;
    }
    else if (parseFloat(this.customerinvoices[index].paymentApplied) > parseFloat(this.customerinvoices[index].balamount)) {
      this.customerinvoices[index].paymentApplied = this.customerinvoices[index].balamount;
    }
  }
  total: any;
  calcPayingTotal() {
    var total = 0.00;
    _.forEach(this.customerinvoices, function (value, key: any) {
      if (!isNaN(value.paymentApplied)) {
        total += parseFloat(value.paymentApplied);
      }
      return total;
    });
    this.total = total
  }

  calcBalanceAmount(balance, applied) {
    if (!isNaN(applied)) {
      return balance - applied;
    }
  }

  loadpayterm() {
    var data = {
      type: "PAYMENT"
    };
    this.featureService.paytermGetAll(data)
      .then((res) => {
        console.log("paymode", res.data)
        var paymode = res.data;
        this.payterm = this.masterservice.formatDataforDropdown("name", paymode, "Select Type"); // params name->formatting label name,fdata->value object,third param->empty selection or place holder value
        console.log("payterm", this.payterm)
      })
  }

  selpayterm(item) {
    this.Selectedpayterm = item;
    this.Selectedpaymode = "";
    var data = {
      "tenantid": this.userstoragedata.tenantid,
      "accountgroup": item.accountgroup,
      "groupname": item.groupname
    }
    this.featureService.paymodeGetAll(data)
      .then((res) => {
        console.log("paymodefor selected payterm", res.data);
        var paymodes = res.data;
        this.allpaymode = this.masterservice.formatDataforDropdown("subaccheadname", paymodes, "Select Type"); // params name->formatting label name,fdata->value object,third param->empty selection or place holder value
        console.log("paymodes", this.allpaymode)
      })
  }

  selpaymode(item) {
    this.Selectedpaymode = item
    console.log("paymode", item);
  }

  filterContacts(event) {
    this.filteredContacts = [];
    this.filteredContacts = _.filter(this.contactlist, function (c) {
      var cname = c.firstname + c.lastname;
      return (cname.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
    });
  }
  handleDropdownClick() {
    this.filteredContacts = [];

    //mimic remote call
    setTimeout(() => {
      this.filteredContacts = this.contactlist;
    }, 100)
  }
  selectedCustomer(contact: any) {
    console.log("selected contact: ", contact);
    this.contactdtls = contact;
    // this.contact = contact.firstname;
    this.filterInvoiceByCustomer(this.contactdtls.contactid);
  }
  filterInvoiceByCustomer(contactid) {
    if (contactid != undefined) {
      var data =
        {
          "tenantid": this.userstoragedata.tenantid,
          "finyear": this.finyear.finyear,
          "contactid": contactid,
          "feature": "proformo_bill",
          "limit": 1000,
          "offset": 0
        }
      this.purchasesService.getBillList(data)
        .then((res) => {
          this.customerinvoices = _.filter(res.data, function (v: any) {
            v.paymentApplied = 0
            return (0 < v.balamount)
          })
        })
      console.log("customerinvoices", JSON.stringify(this.customerinvoices));
    }
  }

  AddPayment(flag) {
    var inDate: Date = this.dateFormatPipeFilter.transform(this.paymentDate, 'y-MM-dd');
    console.log("recdate", inDate);

    var validdata = true;
    if (!this.Selectedpayterm || !this.Selectedpaymode || !this.contactdtls.contactid || !this.paymentDate || !this.payingAmount)
      var validdata = false;
    if (validdata) {
      var paymentDtData = [];
      let updateHeaderData = [];
      for (var i = this.customerinvoices.length - 1; i >= 0; i--) {
        if (this.customerinvoices[i].paymentApplied > 0) {
          var dt;
          var billdt;
          if (this.customerinvoices[i].externalentry) {
            dt = {
              "finyear": this.finyear,
              "tenantid": this.userstoragedata.tenantid,
              "tenantname": this.userstoragedata.tenantname,
              "contactid": this.contactdtls.contactid,
              "companyname": this.contactdtls.companyname,
              "pymntapplied": this.customerinvoices[i].paymentApplied
            }
          } else {
            dt = {
              "finyear": this.finyear.finyear,
              "tenantid": this.userstoragedata.tenantid,
              "tenantname": this.userstoragedata.tenantname,
              "contactid": this.customerinvoices[i].contactid,
              "companyname": this.customerinvoices[i].companyname,
              "refid": this.customerinvoices[i].billid,
              "refno": this.customerinvoices[i].billno,
              "refdt": this.customerinvoices[i].billdt,
              "reftotal": this.customerinvoices[i].billtotal,
              "pymntamount": this.customerinvoices[i].pymntamount,
              "pymntapplied": this.customerinvoices[i].paymentApplied,
              "balamount": (this.customerinvoices[i].balamount - this.customerinvoices[i].paymentApplied),
              "feature": "Payment"
            };
            var headerObj: any = {};
            headerObj.billid = this.customerinvoices[i].billid;
            headerObj.tenantid = this.userstoragedata.tenantid;
            headerObj.balamount = (this.customerinvoices[i].balamount - this.customerinvoices[i].paymentApplied);
            headerObj.pymntapplied = this.customerinvoices[i].paymentApplied;
            headerObj.pymntamount = parseFloat(this.customerinvoices[i].paymentApplied) + parseFloat(this.customerinvoices[i].pymntamount);

            updateHeaderData.push(headerObj);
          }
          paymentDtData.push(dt);
          console.log("paymentdata", paymentDtData);
        }

        var calcPayingTotal = this.calcPayingTotal();
        var acchead = this.heads;
        var cashinhand = _.find(this.allbookaccs, { "subaccheadname": AppConstant.API_CONFIG.EXCLEDGER[0] });
        var bank = _.find(this.allbookaccs, { "subaccheadname": AppConstant.API_CONFIG.EXCLEDGER[3] });
        this.contacthead = _.find(this.allbookaccs, { "subaccheadid": this.contactdtls.accheadid });
        var crdr = "";
        if (this.contacthead.crdr == "C") {
          crdr = "D";
        } else {
          crdr = "C";
        }

        this.accconfig[0].cramount = this.total;
        this.accconfig[0].contactid = this.contactdtls.contactid;
        this.accconfig[0].contactname = this.contactdtls.firstname;
        this.accconfig[0].tenantid = this.userstoragedata.tenantid;
        this.accconfig[0].tenantname = this.userstoragedata.tenantName;
        this.accconfig[0].feature = "Payment";

        this.accconfig[1].tenantid = this.userstoragedata.tenantid;
        this.accconfig[1].tenantname = this.userstoragedata.tenantname;
        this.accconfig[1].contactname = this.contactdtls.firstname;
        this.accconfig[1].contactid = this.contactdtls.contactid;
        this.accconfig[1].accheadid = this.Selectedpaymode.accheadid;
        this.accconfig[1].accheadname = this.Selectedpaymode.accheadname;
        this.accconfig[1].leadaccheadid = this.Selectedpaymode.subaccheadid;
        this.accconfig[1].leadaccheadname = this.Selectedpaymode.subaccheadname;
        this.accconfig[1].feature = "Payment";
        if (this.accconfig[1].crdr == "C") {
          this.accconfig[1].cramount = this.total;
        } else {
          this.accconfig[1].dramount = this.total;
        }
        if (this.Selectedpaymode.subaccheadname != "CASH ON HAND") {
          var bankid = this.Selectedpaymode.subaccheadid;
          var bankname = this.Selectedpaymode.subaccheadname;
        }
        console.log("config", this.accconfig);
      }

      var headerDetails = {
        "tenantid": this.userstoragedata.tenantid,
        "tenantname": this.userstoragedata.tenantname,
        "status": "Active",
        "createdby": this.userstoragedata.loginname,
        "createddt": new Date(),
        "finyear": this.finyear.finyear,
        "contactid": this.contactdtls.contactid,
        "contactname": this.contactdtls.firstname,
        "companyname": this.contactdtls.companyname,
        "pymtmethod": this.Selectedpayterm.name,
        "pymtrectdt": inDate,
        "pymtamount": this.total,
        "pymtref": this.paymentReference,
        "rectaccheadid": this.contacthead.accheadid,
        "rectaccheadname": this.contacthead.accheadname,
        "subaccheadid": this.contactdtls.accheadid,
        "subaccheadname": this.contactdtls.accheadname,
        "openingbalance": this.contacthead.openingbalance,
        "currentbalance": this.contacthead.currentbalance,
        "nativecrdr": this.contacthead.crdr,
        "crdr": crdr,
        "remarks": this.paymentRemarks,
        "refkey": "PYMT",
        "feature": "Payment",
        "bankid": bankid,
        "bankname": bankname,
        "pymtrecttype": "ADVANCE"
      };

      var formData: any = {};
      formData.header = headerDetails;
      formData.ledgers = this.accconfig;
      formData.paymentReceiptDetails = paymentDtData;
      formData.updateHeader = updateHeaderData;
      console.log("formdata", JSON.stringify(formData));

      if (this.total > 0 && this.contactdtls.contactid != "") {
        this.paymentService.PaymentCreate(formData)
          .then((res) => {
            if (res.status == true) {
              console.log("created", res)
              this.messageService.showMessage({
                severity: 'success', summary: 'success',
                detail: res.message
              });
              if (flag) {
                this.clearform();
              }
              else {
                this.router.navigate(['/purchase/vendoradvance']);
              }

            } else {
              this.msgs = [];
              this.msgs.push({
                severity: 'success', summary: 'Success',
                detail: res.message
              });
            }
          })
      } else {

      }
    }
    else {
      if (!this.contactdtls || !this.contactdtls.contactid) {
        this.messageService.showMessage({
          severity: 'warn', summary: 'warning',
          detail: "Please Select Contact"
        });
      }
      else if (!this.paymentDate) {
        this.messageService.showMessage({
          severity: 'warn', summary: 'warning',
          detail: "Please Select Date"
        });
      }
      else if (!this.Selectedpayterm) {
        this.messageService.showMessage({
          severity: 'warn', summary: 'warning',
          detail: "Please select Payment Mode"
        });
      }
      else if (!this.Selectedpaymode) {
        this.messageService.showMessage({
          severity: 'warn', summary: 'warning',
          detail: "Please Select Account"
        });
      }
      else if (!this.payingAmount) {
        this.messageService.showMessage({
          severity: 'warn', summary: 'warning',
          detail: "Please Enter Receipt Amount"
        });
      }
    }
  }

  clearform() {
    this.customerinvoices = [];
    this.contact = "";
    this.Selectedpayterm = "",
      this.Selectedpaymode = "",
      this.paymentRemarks = "";
    this.paymentApplied = "";
    this.payingAmount = 0;
    this.paymentReference = "";
    this.loadbilllist()
  };

}
