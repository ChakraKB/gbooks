import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import * as _ from "lodash";
import { MasterService } from '../../services/master.service';
import { PurchasesService } from '../../services/purchases/purchases.service';
import { Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-bill',
  templateUrl: './view-bill.component.html',
  styleUrls: ['./view-bill.component.scss']
})
export class ViewBillComponent implements OnInit {
  public currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  public date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  @Input() billid: any;
  userstoragedata: any = {};
  resources: any = {};
  reqdata: any = {};
  billDeta: any;
  billdatas: Array<any> = [];
  billtaxs: Array<any> = [];
  bill_feature: string = "Bill";
  mode: string;
  constructor(private masterservice: MasterService, private storageservice: LocalStorageService,
    private route: ActivatedRoute, private purchaseservice: PurchasesService,
    private router: Router,private location: Location) {
    this.route.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.billid = params.billid;
        this.mode = params.bill;
        console.log("url params", params);
      }
    });
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.resources = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.RESOURCE);
  }

  ngOnInit() {
    this.reqdata = {
      "billid": this.billid
    };
    console.log("invoicedetails", this.billid);
    if (this.billid && this.billid != undefined) {
      this.loadBillDetails(this.reqdata);
    }
  }

  redirecttoprev() {
    if (this.mode == "fromledger") {
      this.router.navigate(['/reports/trailbalance']);
    }
    else if (this.mode == "bill") {
      this.router.navigate(['/purchase/list']);
    }
    this.location.back();
  }


  loadBillDetails(reqdata) {
    this.billDeta = [];
    var reqdata: any = {
      "feature": "bill",
      "billid": this.billid
    };
    this.purchaseservice.getBillById(reqdata)
      .then((res) => {
        if (res.status) {
          this.billDeta = res.data[0];
          this.billdatas = res.data[0].billDetails;
          this.billtaxs = res.data[0].billTaxes;
          console.log("bill data: ", this.billDeta);
          // if (res.data[0].feature == "proforma_bill") {
          //   this.bill_feature = "Pro-Forma Bill";
          // }
        }
      });

  }

}
