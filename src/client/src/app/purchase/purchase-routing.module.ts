import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BillListComponent } from './bill-list/bill-list.component';
import { BillEditComponent } from './bill-edit/bill-edit.component';
import { VendorAdvanceComponent } from './vendor-advance/vendoradvance-List/vendor-advance.component';
import { ViewBillComponent } from './view-bill/view-bill.component';
import { EditComponent } from './edit/edit.component';
import { VendoradvanceAddeditComponent } from './vendor-advance/vendoradvance-addedit/vendoradvance-addedit.component'
import {VendoradvanceUpdateComponent} from '../purchase/vendor-advance/vendoradvance-update/vendoradvance-update.component' 
import {ViewVendorAdvanceComponent} from './vendor-advance/view-vendor-advance/view-vendor-advance.component'
const routes: Routes = [
  {
    path: 'list',
    component: BillListComponent,
    data: {
      title: 'Bill List'
    }
  },
  {
    path: 'billedit',
    component: BillEditComponent,
    data: {
      title: 'Add/Edit Bill'
    }
  },
  {
    path: 'editbill/:billid',
    component: EditComponent,
    data: {
      title: 'Edit Bill'
    }
  },
  {
    path: 'viewbill/:bill/:billid',
    component: ViewBillComponent
  },
  {
    path: 'vendoradvance',
    component: VendorAdvanceComponent,
    data: {
      title: 'Vendar Advance List'
    }
  },
  {
    path: 'vendoradvanceaddedit',
    component: VendoradvanceAddeditComponent,
    data: {
      title: 'VendarAdvance-AddEdit '
    }
  },
  {
    path: 'vendoradvanceupdate/:pymtrectid',
    component: VendoradvanceUpdateComponent,
    data: {
    title: 'VendarAdvance-Edit'
    }
  },
  {
    path: 'vendoradvanceview/:pymtrectid',
    component: ViewVendorAdvanceComponent
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
})
export class PurchaseRoutingModule { }
