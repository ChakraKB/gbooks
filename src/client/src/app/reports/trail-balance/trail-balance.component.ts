import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import * as _ from "lodash";
import { FeaturesService } from '../../services/features.service';
import { MasterService } from '../../services/master.service';
import { TreeTableModule, TreeNode, SharedModule } from 'primeng/primeng';
import { DateformatPipe } from '../../pipes/dateformat.pipe';
import { Router, NavigationExtras } from '@angular/router';
import { ContextMenuModule, MenuItem } from 'primeng/primeng';
import { MessagesService } from '../../shared/messages.service';
import { UtilsService } from '../../services/utils.service';
import { ReportService } from '../../services/reports/reports.service';
import { LedgerComponent } from '../ledger/ledger.component';

@Component({
  selector: 'app-trail-balance',
  templateUrl: './trail-balance.component.html',
  styleUrls: ['./trail-balance.component.scss']
})
export class TrailBalanceComponent implements OnInit {
  public currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  public date_dformat = AppConstant.API_CONFIG.ANG_DATE.displayFormat;
  TBtreeData: TreeNode[];
  TBopeningTotal: any;
  TBclodingTotal: any;
  TBcTotal: any;
  TBdTotal: any;
  selectedAccount: TreeNode;
  cmenu_items: MenuItem[];
  userstoragedata: any;
  finyear: any;
  allheads: any;
  tbreportList: any;
  allbookaccs: any;
  activetabindex: number = 0;
  tabbedledgers: any = [];
  constructor(private masterservice: MasterService, private featureservice: FeaturesService, private storageservice: LocalStorageService,
    private dateFormatPipeFilter: DateformatPipe, private router: Router,
    private messageservice: MessagesService, private utilservice: UtilsService, private reportservice: ReportService) {
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
  }
  getHeadsList() {
    var self = this;
    var data = {
    };
    self.allheads = [];
    this.featureservice.AccHeadList(data)
      .then(function (res) {
        if (res.status) {
          self.allheads = res.data;
          console.log("heads", self.allheads);
          self.loadBookofAcc();
        }
      });
  }
  loadBookofAcc() {
    var self = this;
    var data = {
      tenantid: [this.userstoragedata.tenantid, 0],
    };
    self.allbookaccs = [];
    this.masterservice.BookGetAll(data)
      .then(function (res) {
        self.allbookaccs = res.data;
        self.getTbList();
      });
  }
  getTbList() {
    var self = this;
    var data = {
      tenantid: this.userstoragedata.tenantid,
      finyear: this.finyear.finyear,
    };
    self.tbreportList = [];
    this.reportservice.getTBreport(data)
      .then(function (res) {
        if (res.status) {
          self.tbreportList = res.data;
          console.log("report", self.tbreportList);
          self.formattreestructure(self.allheads, self.allbookaccs, self.tbreportList);
          // self.loadBookofAccList();
        }
        else{
          self.formattreestructure(self.allheads, self.allbookaccs, []);
        }
      });
  }
  ngOnInit() {
    this.getHeadsList();
    // this.TBtreeData = this.getTBformatedData().data;
    this.cmenu_items = [
      { label: 'View Leger', icon: 'fa-search', command: (event) => this.viewNode(this.selectedAccount) },
      // { label: 'Expand', icon: 'fa-expand', command: (event) => this.expandNode(this.selectedAccount) },
      // { label: 'Collapse', icon: 'fa-compress', command: (event) => this.collapseNode(this.selectedAccount) }
    ];
  }
  viewNode(node: TreeNode) {
    // this.msgs = [];
    // this.msgs.push({severity: 'info', summary: 'Node Selected', detail: node.data.name});
  }
  collapseNode(node: TreeNode) {
    // this.msgs = [];
    // this.msgs.push({severity: 'info', summary: 'Node Selected', detail: node.data.name});
  }
  expandNode(node: TreeNode) {
    // node.parent.children = node.parent.children.filter( n => n.data !== node.data);
    // this.msgs = [];
    // this.msgs.push({severity: 'info', summary: 'Node Deleted', detail: node.data.name});
  }
  expandNodeAll(event) {
    console.log(event);
  }
  formattreestructure(head, level1, level2) {
    var treedata: any = this.utilservice.tbtree_formater(head, level1, level2, true, { feature: "TB" });
    this.TBtreeData = treedata.data;
    this.TBopeningTotal = treedata.openingtoal;
    this.TBclodingTotal = treedata.closingtotal;
    this.TBcTotal = treedata.ctotal;
    this.TBdTotal = treedata.dtotal;
    console.log("treedata", treedata);
  }
  addTabViewledger(item, addTabViewinvoice) {
    if (item.data.level == 2) {
      this.utilservice.activate_multitab(this.tabbedledgers, item.data, addTabViewinvoice, "accountname");
    }
    else return false;
  }
  addTabViewledger1(ledgeraccount, addTabViewledger, index) {
    var self = this;
    console.log(addTabViewledger);
    if (this.tabbedledgers.length > 0) {
      this.tabbedledgers.unshift(ledgeraccount);
    }
    else {
      this.tabbedledgers.push(ledgeraccount);
    }
    setTimeout(function () {
      // alert("hi");
      self.activetabindex = 1;
      addTabViewledger.activeIndex = 1;
      addTabViewledger.tabs = _.map(addTabViewledger.tabs, function (data: any) {
        data.selected = false;
        return data;
      });
      addTabViewledger.tabs[1].selected = true;
      // addTabViewledger = [...addTabViewledger];
    }, 0);
  }
  onnodeSelect(event) {

  }
  handleTabClose(event, tbtabview) {
    this.utilservice.deactivate_multitab(this.tabbedledgers, event, tbtabview, "invoiceno");
  }

}
