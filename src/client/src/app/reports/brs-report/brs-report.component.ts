import { Component, OnInit } from '@angular/core';
import {DropdownModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {AppConstant} from '../../app.constant';
import{BanksService} from '../../bank/service/banks.service';
import { DateformatPipe } from '../../pipes/dateformat.pipe';
import { LocalStorageService } from '../../shared/local-storage.service';
import { MessagesService } from '../../shared/messages.service';
import * as _ from "lodash";


@Component({
  selector: 'app-brs-report',
  templateUrl: './brs-report.component.html',
  styleUrls: ['./brs-report.component.scss']
})
export class BrsReportComponent implements OnInit {
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  private date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  private date_apiformat = AppConstant.API_CONFIG.ANG_DATE.apiFormat;
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  cities: SelectItem[];
  show : boolean = false;
  value: Date;
  selectedCity: string;
  brsreqData:any;
  startdate:Date;
  enddate:Date;
  sendstartdatefilter:Date;
  sendendatefilter:Date;
  currentDate:Date;
  userdetails:any
  validation:boolean=false;
  validationMsg:string
  brslist:any
  
  constructor(private BanksService:BanksService,
  private DateformatPipe:DateformatPipe,
  private LocalStorageService:LocalStorageService,
  private messageService:MessagesService
) {
    this.userdetails = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
}

  ngOnInit() {
    this.currentDate=new Date();
    this.startdate=this.currentDate;
    this.enddate=this.currentDate;
   this.getbrsListAll(this.startdate,this.enddate)
  }
  getbrsListAll(strdate,enddate){
    this.sendstartdatefilter = this.DateformatPipe.transform(strdate, this.date_apiformat);
    this.sendendatefilter = this.DateformatPipe.transform(enddate, this.date_apiformat);
    this.validation = true;
    this.validationMsg = ""
    if (_.isEmpty(this.sendstartdatefilter)) {
      this.validation = false;
      this.validationMsg = "Select From Date"
    }
    else if (_.isEmpty(this.sendendatefilter)) {
      this.validation = false;
      this.validationMsg = "Select To Date"
    }
    if (this.sendstartdatefilter > this.sendendatefilter) {
      this.validation = false;
      this.validationMsg = "Start date should be smaller than to date"
    }
    this.brsreqData = {
      "startdate":this.sendstartdatefilter,
     "enddate":this.sendendatefilter,
      "startCreatedDt":"",
      "endCreatedDt":"",
      "gtAmount":"",
      "ltAmount":"",
      "amountKey":"",
      // "offset": 0,
      // "limit": 10,
      "query": {
        tenantid: this.userdetails.tenantid,
           }
    };
    console.log("list",JSON.stringify(this.brsreqData));
    if (this.validation) {
      this.BanksService.brsListAll(this.brsreqData)
        .then(res => {
          if(res.status==true){
            this.brslist = res.data;
            console.log(" this.brslist", this.brslist)
            // this.messageService.showSuccessMessage({
            //   severity: 'success', summary: 'success',
            //   detail: res.message
            // });
          }else if(res.status==false){
            // this.messageService.showMessage({
            //   severity: 'error', summary: 'error',
            //   detail: res.message
            // });
          }
        });
    }
    else {
      this.messageService.showMessage({
        severity: 'warn', summary: 'warning',
        detail: this.validationMsg
      });
    }


  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }
}
