import { DropdownModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SalesService } from '../../services/sales/sales.service';
import { LocalStorageService } from '../../shared/local-storage.service';
import { DashboardService } from '../../services/dashboard.service';
import { AppConstant } from '../../app.constant';
import { ContextMenuModule, MenuItem } from 'primeng/primeng';
import { Router, NavigationExtras } from '@angular/router';
import * as _ from "lodash";
import { CurrencyPipe } from "@angular/common";
import { FeaturesService } from '../../services/features.service';
import { MasterService } from '../../services/master.service';
import { ReportService } from '../../services/reports/reports.service'
import { DateformatPipe } from '../../pipes/dateformat.pipe';
import { MessagesService } from '../../shared/messages.service';
import { ViewPaymentComponent } from '../../payment/view-payment/view-payment.component';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-payment-register',
  templateUrl: './payment-register.component.html',
  styleUrls: ['./payment-register.component.scss']
})
export class PaymentRegisterComponent implements OnInit {
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  private date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  private date_apiformat = AppConstant.API_CONFIG.ANG_DATE.apiFormat;
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  cities: SelectItem[];
  value: Date;
  selectedCity: string;
  userstoragedata: any;
  finyear: any;
  currencyFilter: any;
  list: any;
  contactlist: any;
  allcontacts: any;
  selectedCon:any;
  selectedContact: any;
  invoiceDate: any;
  currentDate: any;
  DueDate: any;
  validation: any;
  validationMsg: any;
  InvoiceNumber: any;
  fromdate: any;
  todate: any;
  pymtrectno: any;
  selectedpymntrec: any = [];
  activeTab: String = "-";
  invoicereqData: any = [];
  proformainvoicereqData: any = [];
  activetabindex:number=0;
  constructor(
    private salesService: SalesService,
    private storageservice: LocalStorageService,
    private dashboardService: DashboardService,
    private router: Router,
    private featureservice: FeaturesService,
    private masterservice: MasterService,
    private reportService: ReportService,
    private dateFormatPipeFilter: DateformatPipe,
    private messageService: MessagesService,
    private UtilsService:UtilsService
  ) {
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.currencyFilter = new CurrencyPipe("en-in");
  }

  ngOnInit() {
    this.currentDate = new Date();
    this.invoiceDate = this.currentDate;
    this.DueDate = this.currentDate;
    this.getAllPayments(this.currentDate, this.currentDate);
    this.getAllContacts();
  }
  getAllContacts() {
    var data = {
      tenantid: this.userstoragedata.tenantid,
      status: "Active",
      contactype: "Vendor"
    };
    this.featureservice.contactGetAll(data)
      .then((res) => {
        this.allcontacts = res.data;
        this.contactlist = this.masterservice.formatDataforDropdown("firstname", this.allcontacts, "All");
        console.log("contactlist", this.contactlist)
      });
  }
  onContactSelect(item) {
    this.selectedContact = item.value;
    console.log("SelectedContact", this.selectedContact);
  }

  getAllPayments(frmdt, todt) {
    this.fromdate = this.dateFormatPipeFilter.transform(frmdt, this.date_apiformat);
    this.todate = this.dateFormatPipeFilter.transform(todt, this.date_apiformat);
    this.validation = true;
    this.validationMsg = ""
    if (_.isEmpty(this.fromdate)) {
      this.validation = false;
      this.validationMsg = "Select From Date"
    }
    else if (_.isEmpty(this.todate)) {
      this.validation = false;
      this.validationMsg = "Select To Date"
    }
    if (this.fromdate > this.todate) {
      this.validation = false;
      this.validationMsg = "Start date should be smaller than to date"
    }
    var data: any = {
      "startdt": this.fromdate,
      "enddt": this.todate,
      'tenantid': this.userstoragedata.tenantid,
      'feature': "Payment",
      'finyear': this.finyear.finyear,
      // 'limit': 10,
      // 'offset': 0
    }
    if (!_.isEmpty(this.selectedContact)) {
      data.contactid = this.selectedContact.contactid;
    }
    if (this.validation) {
      this.reportService.GetPayRecReportlist(data).then((res) => {
        console.log("reclist", JSON.stringify(res));
        this.list = res.data;
      })
    }
    else {
      this.messageService.showMessage({
        severity: 'error', summary: 'error',
        detail: this.validationMsg
      });
    }
  }
  addTabViewinvoice(item,addTabViewinvoice) {
    this.UtilsService.activate_multitab(this.selectedpymntrec,item,addTabViewinvoice,"pymtrectno");
  }
 
  handleTabClose(event, closetab) {
    this.UtilsService.deactivate_multitab(this.selectedpymntrec,event,closetab,"pymtrectno");
  }

}
