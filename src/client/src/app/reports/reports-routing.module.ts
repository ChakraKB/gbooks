import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { ReceiptRegisterComponent } from './receipt-register/receipt-register.component';
import { LedgerComponent } from './ledger/ledger.component';
import { TrailBalanceComponent } from './trail-balance/trail-balance.component';
import { ProfitLossComponent } from './profit-loss/profit-loss.component';
import { BalanceSheetComponent } from './balance-sheet/balance-sheet.component';
import { InvoiceRegisterComponent } from './invoice-register/invoice-register.component';
import { BillRegisterComponent } from './bill-register/bill-register.component';
import { PaymentRegisterComponent } from './payment-register/payment-register.component';
import { BrsReportComponent } from './brs-report/brs-report.component';

const routes: Routes = [
  {
    path: 'list',
    component: ReportsComponent,
    data: {
      title: 'Receipt Register'
    }
  },
  {
    path: 'ledger',
    component: LedgerComponent,
    data: {
      title: 'Ledger'
    }
  },
  {
    path: 'trailbalance',
    component: TrailBalanceComponent,
    data: {
      title: 'Trail Balance'
    }
  },
  {
    path: 'profitloss',
    component: ProfitLossComponent,
    data: {
      title: 'Profit Loss'
    }
  },
  {
    path: 'balancesheet',
    component: BalanceSheetComponent,
    data: {
      title: 'Balance Sheet'
    }
  },
  {
    path: 'invoiceregister',
    component: InvoiceRegisterComponent,
    data: {
      title: 'Invoice Register'
    }
  },
  {
    path: 'billregister',
    component: BillRegisterComponent,
    data: {
      title: 'Bill Register'
    }
  },
  {
    path: 'receipt-register',
    component: ReceiptRegisterComponent,
    data: {
      title: 'Receipt Register'
    }
  },
  {
    path: 'paymentregister',
    component: PaymentRegisterComponent,
    data: {
      title: 'Payment Register'
    }
  },
  {
    path: 'brsreport',
    component: BrsReportComponent,
    data: {
      title: 'Brs Report'
    }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
