import { DropdownModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { InvoiceDataTable } from '../../sales/invoice-data-table.interface';
import { LocalStorageService } from '../../shared/local-storage.service';
import { DashboardService } from '../../services/dashboard.service';
import { AppConstant } from '../../app.constant';
import { ViewBillComponent } from '../../purchase/view-bill/view-bill.component';
import { ContextMenuModule, MenuItem } from 'primeng/primeng';
import { Router, NavigationExtras } from '@angular/router';
import * as _ from "lodash";
import { CurrencyPipe } from "@angular/common";
import { FeaturesService } from '../../services/features.service';
import { MasterService } from '../../services/master.service';
import { ReportService } from '../../services/reports/reports.service'
import { DateformatPipe } from '../../pipes/dateformat.pipe';
import { MessagesService } from '../../shared/messages.service';
import { PurchasesService } from '../../services/purchases/purchases.service';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-bill-register',
  templateUrl: './bill-register.component.html',
  styleUrls: ['./bill-register.component.scss']
})
export class BillRegisterComponent implements OnInit {
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  private date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  private date_apiformat = AppConstant.API_CONFIG.ANG_DATE.apiFormat;
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  value: Date;
  selectedCar: string;
  billlist: any = [];
  userstoragedata: any;
  finyear: any;
  selectedbills: any = [];
  activeTab: String = "-";
  invoicereqData: any = [];
  proformainvoicereqData: any = [];
  currencyFilter: CurrencyPipe;
  contactlist: any;
  allcontacts: any;
  selectedContact: any;
  invoiceDate: any;
  currentDate: any;
  DueDate: any;
  validation: any;
  validationMsg: any;
  InvoiceNumber: any;
  cars: SelectItem[];
  billno: any;
  fromdate: any;
  todate: any;
  activetabindex:number=0;

  constructor(
    private purchaseservice: PurchasesService,
    private storageservice: LocalStorageService,
    private dashboardService: DashboardService,
    private router: Router,
    private featureservice: FeaturesService,
    private masterservice: MasterService,
    private reportService: ReportService,
    private dateFormatPipeFilter: DateformatPipe,
    private messageService: MessagesService,
    private UtilsService:UtilsService
  ) {
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.currencyFilter = new CurrencyPipe("en-in");
  }

  ngOnInit() {
    this.currentDate = new Date();
    this.invoiceDate = this.currentDate;
    this.DueDate = this.currentDate;
    this.getBillList(this.currentDate, this.currentDate);
    this.getAllContacts();
  }

  getAllContacts() {
    var data = {
      tenantid: this.userstoragedata.tenantid,
      status: "Active",
      contactype: "Vendor"
    };
    this.featureservice.contactGetAll(data)
      .then((res) => {
        this.allcontacts = res.data;
        this.contactlist = this.masterservice.formatDataforDropdown("firstname", this.allcontacts, "All");
        console.log("contactlist", this.contactlist)
      });
  }
  onContactSelect(item) {
    this.selectedContact = item.value;
    console.log("SelectedContact", this.selectedContact);
  }

  getBillList(frmdt, todt) {
    this.fromdate = this.dateFormatPipeFilter.transform(frmdt, this.date_apiformat);
    this.todate = this.dateFormatPipeFilter.transform(todt, this.date_apiformat);
    this.validation = true;
    this.validationMsg = ""
    if (_.isEmpty(this.fromdate)) {
      this.validation = false;
      this.validationMsg = "Select From Date"
    }
    else if (_.isEmpty(this.todate)) {
      this.validation = false;
      this.validationMsg = "Select To Date"
    }
    if (this.fromdate > this.todate) {
      this.validation = false;
      this.validationMsg = "Start date should be smaller than to date"
    }
    this.invoicereqData = {
      startdt: this.fromdate,
      enddt: this.todate,
      tenantid: this.userstoragedata.tenantid,
      // offset: 0,
      // limit: 10
    };
    if (!_.isEmpty(this.selectedContact)) {
      this.invoicereqData.contactid = this.selectedContact.contactid;
    }
    if (!_.isEmpty(this.billno)) {
      this.invoicereqData.billno = this.billno;
    }
    if (this.validation) {
      this.reportService.GetBillReportlist(this.invoicereqData)
        .then(res => {
          this.billlist = res.data;
          console.log(this.billlist);
        });
    }
    else {
      this.messageService.showMessage({
        severity: 'error', summary: 'error',
        detail: this.validationMsg
      });
    }

  }
  addTabViewinvoice(item,addTabViewinvoice) {
    this.UtilsService.activate_multitab(this.selectedbills,item,addTabViewinvoice,"billno");
  }
  handletabClose(event,closetab) {
    this.UtilsService.deactivate_multitab(this.selectedbills,event,closetab,"billno");

  }
 

}
