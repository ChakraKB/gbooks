import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import * as _ from "lodash";
import { FeaturesService } from '../../services/features.service';
import { MasterService } from '../../services/master.service';
import { TreeTableModule, TreeNode, SharedModule } from 'primeng/primeng';
import { DateformatPipe } from '../../pipes/dateformat.pipe';
import { Router, NavigationExtras } from '@angular/router';
import { ContextMenuModule, MenuItem } from 'primeng/primeng';
import { MessagesService } from '../../shared/messages.service';
import { UtilsService } from '../../services/utils.service';
import { ReportService } from '../../services/reports/reports.service';

@Component({
  selector: 'app-balance-sheet',
  templateUrl: './balance-sheet.component.html',
  styleUrls: ['./balance-sheet.component.scss']
})
export class BalanceSheetComponent implements OnInit {

  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  private date_dformat = AppConstant.API_CONFIG.ANG_DATE.displayFormat;
  cmenu_items: MenuItem[];
  userstoragedata: any;
  finyear: any;
  allheads: any;
  bsreportList: any;
  bs_assetsList: any = [];
  bs_liabsList: any = [];
  allbookaccs: any;
  constructor(private masterservice: MasterService, private featureservice: FeaturesService, private storageservice: LocalStorageService,
    private dateFormatPipeFilter: DateformatPipe, private router: Router,
    private messageservice: MessagesService, private utilservice: UtilsService, private reportservice: ReportService) {
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
  }
  getHeadsList() {
    var self = this;
    var data = {
      "accgroup": "BALANCE SHEET"
    };
    self.allheads = [];
    this.featureservice.AccHeadList(data)
      .then(function (res) {
        if (res.status) {
          self.allheads = res.data;
          console.log("heads", self.allheads);
          self.loadBookofAcc();
        }
      });
  }
  loadBookofAcc() {
    var self = this;
    var data = {
      tenantid: [this.userstoragedata.tenantid, 0],
    };
    self.allbookaccs = [];
    this.masterservice.BookGetAll(data)
      .then(function (res) {
        self.allbookaccs = res.data;
        self.getBSList();
      });
  }
  getBSList() {
    var self = this;
    var data = {
      tenantid: this.userstoragedata.tenantid,
      finyear: this.finyear.finyear,
    };
    self.bsreportList = [];
    this.reportservice.getBalanceSheetreport(data)
      .then(function (res) {
        if (res.status) {
          self.bsreportList = res.data;
          console.log("report", self.bsreportList);
          self.formatBStructure(self.allheads, self.allbookaccs, self.bsreportList);
          // self.loadBookofAccList();
        }
        else{
          self.formatBStructure(self.allheads, self.allbookaccs,[]);
        }
      });
  }
  ngOnInit() {
    this.getHeadsList();
    // this.TBtreeData = this.getTBformatedData().data;
    this.cmenu_items = [
      { label: 'View Leger', icon: 'fa-search' },
      // { label: 'View Leger', icon: 'fa-search', command: (event) => this.viewNode(this.selectedAccount) },
      // { label: 'Expand', icon: 'fa-expand', command: (event) => this.expandNode(this.selectedAccount) },
      // { label: 'Collapse', icon: 'fa-compress', command: (event) => this.collapseNode(this.selectedAccount) }
    ];
  }

  formatBStructure(head, level1, level2) {
    var assets_head = _.filter(head, function (h: any) {
      return (h.ALIE === "A")
    });
    var liabs_head = _.filter(head, function (h: any) {
      return (h.ALIE === "L")
    });
    var assetdata:any = this.utilservice.tbtree_formater(assets_head, level1, level2,true,{feature:"BS","prevyear":false});
    this.bs_assetsList = assetdata.flatedArray;
    var liasdata:any = this.utilservice.tbtree_formater(liabs_head, level1, level2,true,{feature:"BS","prevyear":false});
   this.bs_liabsList = liasdata.flatedArray;
    console.log("Asset data", assetdata);
    console.log("Lias data", liasdata);
  }
  calculateGroupTotal(accounts)
  {
    console.log("accounts table",accounts);
    return 1000;
  }
}