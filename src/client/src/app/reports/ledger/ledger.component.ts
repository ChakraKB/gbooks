import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ReportService } from '../../services/reports/reports.service';
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import { Router, NavigationExtras } from '@angular/router';
import { Message } from 'primeng/primeng';
import { MasterService } from '../../services/master.service';
import { MessagesService } from '../../shared/messages.service';
import { DateformatPipe } from '../../pipes/dateformat.pipe';
import * as moment from 'moment';
@Component({
  selector: 'app-ledger',
  templateUrl: './ledger.component.html',
  styleUrls: ['./ledger.component.scss']
})
export class LedgerComponent implements OnInit {
  @Input()
  externalinput: boolean = false;
  @Input()
  externalsubaccheadid: any;
  @Input()
  externalstartdate: any;
  @Input()
  externalenddate: any;

  ledgerlist: any[] = [];
  FilteredLedgerList: any[] = [];
  localstorageDetails: any;
  finyear: any;
  ledgerFilterForm: FormGroup;
  startdt: FormControl;
  enddt: FormControl;
  accheadname: FormControl;
  currentDate: any;
  dispDateFormat: string = AppConstant.API_CONFIG.DATE.displayFormat;
  public date_apiformat = AppConstant.API_CONFIG.ANG_DATE.apiFormat;
  public currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  selectedLedger: any;
  creditTotal: any = 0;
  debitTotal: any = 0;
  msgs: Message[] = [];
  Allledger: any;
  constructor(
    private fb: FormBuilder,
    private reportservice: ReportService,
    private localstorageservice: LocalStorageService,
    private dateFormatPipeFilter: DateformatPipe,
    private router: Router,
    private messageService: MessagesService,
    private masterservice: MasterService
  ) {
    this.localstorageDetails = this.localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.ledgerFilterForm = fb.group({
      'startdt': [new Date(), Validators.required],
      'enddt': [new Date(), Validators.required],
      'accheadname': [null, Validators.required]
    })
    this.dispDateFormat = AppConstant.API_CONFIG.DATE.displayFormat;
  }
  ngOnInit() {
    this.currentDate = new Date();
    this.startdt = this.currentDate;
    this.enddt = this.currentDate;
    this.filterLedger();
    if (this.externalinput) {
      console.log("enternal data", this.externalsubaccheadid);
      if (this.externalstartdate == "" || this.externalenddate == "") {
        var sdate = moment(this.finyear.YearStartsFrom, "DD-MM-YYYY").toDate();
        var edate = moment(this.finyear.YearEndsOn, "DD-MM-YYYY").toDate();
        this.externalstartdate = this.dateFormatPipeFilter.transform(sdate, this.date_apiformat);
        this.externalenddate = this.dateFormatPipeFilter.transform(edate, this.date_apiformat);
      }
      var formdata = {
        'tenantid': this.localstorageDetails.tenantid,
        'finyear': this.finyear.finyear,
        'accheadid': this.externalsubaccheadid,
        'startdt': this.externalstartdate,
        'enddt': this.externalenddate
      }
      this.getledgerreport(formdata);
    }
  }
  filterLedger() {
    this.reportservice.getLedgerlist({ type: "Ledger", tenantid: [this.localstorageDetails.tenantid, 0] }).then((res) => {
      if (res.status == true) {
        this.Allledger = res.data;
        this.ledgerlist = this.masterservice.formatDataforDropdown("subaccheadname", this.Allledger, "Select ledger");
        // this.ledgerlist.push({ label: 'Select Ledger', value: null });
        // for (var i = 0; i < res.data.length; i++) {
        //   this.ledgerlist.push({
        //     label: res.data[i].subaccheadname, value: {
        //       subaccheadname: res.data[i].subaccheadname,
        //       subaccheadid: res.data[i].subaccheadid
        //     }
        //   });
        // }
      }
    });

  }
  formObj: any = {
    startdt: {
      required: "Please select from date",
    },
    enddt: {
      required: "Please select to date"
    },
    accheadname: {
      required: "Please select ledger"
    }
  }

  search(data) {

    if (this.ledgerFilterForm.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.ledgerFilterForm, this.formObj);
      this.messageService.showMessage({ severity: 'error', summary: 'Error Message', detail: errorMessage });
      return false;
    }

    var formdata = {
      'tenantid': this.localstorageDetails.tenantid,
      'finyear': this.finyear.finyear,
      'accheadid': data.accheadname.subaccheadid,
      'startdt': data.startdt,
      'enddt': data.enddt
    }
    this.getledgerreport(formdata);
  }
  getledgerreport(formdata) {
     this.creditTotal = 0;
     this.debitTotal = 0;
    console.log("formdata", formdata);
    this.reportservice.getFilteredLedgerlist(formdata).then((res) => {
      console.log("table data", res);
      if (res.status == true) {
        this.FilteredLedgerList = res.data;
        console.log("ledger data:", this.FilteredLedgerList);
        for (var i = 0; i < res.data.length; i++) {
          this.creditTotal += parseFloat(res.data[i].credit);
          this.debitTotal += parseFloat(res.data[i].debit);
        }
      }
      else {
        this.FilteredLedgerList = [];
      }
    });


  }
  viewfeatures(featuredata) {
    if (featuredata.feature == "Journal") {
      this.router.navigate(['accounts', 'viewjournal', 'fromledger', featuredata.txnid, featuredata.txnrefno]);
    }
    else if (featuredata.feature == "Payment") {
      this.router.navigate(['payment', 'viewpayment', 'fromledger', featuredata.txnid]);
    }
    else if (featuredata.feature == "Receipt") {
      this.router.navigate(['receipts', 'viewreceipt', 'fromledger', featuredata.txnid]);
    }
    else if (featuredata.feature == "Bill") {
      this.router.navigate(['purchase', 'viewbill', 'fromledger', featuredata.txnid]);
    }
    else if (featuredata.feature == "invoice") {
      this.router.navigate(['sales', 'viewinvoice', 'fromledger', featuredata.txnid]);
    }
    else if (featuredata.feature == "proforma_invoice") {
      this.router.navigate(['sales', 'viewinvoice', 'fromledger', featuredata.txnid]);
    }
    else if (featuredata.feature == "CRNOTE") {
      this.router.navigate(['accounts', 'viewcrnote', 'fromledger', featuredata.txnid, featuredata.txnrefno]);
    }
    // else if (featuredata.feature == "DRNOTE") {
    //   this.router.navigate(['sales', 'viewinvoice', 'fromledger', featuredata.txnid]);
    // }
    console.log(featuredata);
    // this.router.navigate(['/sales/viewinvoice'], { queryParams: { "invoiceid": "2" } });
  }
  transactionDetails() {
    console.log("tansaction");
  }
}




