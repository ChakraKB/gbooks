import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import * as $ from 'jquery';
@Injectable()
export class CommonHttpService {
  constructor(private http: HttpClient) { }
  public globalPostService(url: string, data: any) {
    return this.http.post(url, data).toPromise().catch(e=>{
      console.log("error happend",e);
      if(e.status == 401)
      {
        console.log(e.statusText);
        // window.location.href = "../../access.html"; 
      }
    });
    
  }
  public globalGetService(url: string, data: any) {
    var querystring = "?" + $.param( data );
    return this.http.get(url + querystring).toPromise().
    catch(e=>{
      console.log("error happend",e);
    });      
  }  

  public globalGetServiceByUrl(url: string, data: any) {
    return this.http.get(url+  data ).toPromise().
    catch(e=>{
      console.log("error Shappend",e);
    });      
  }  
  
}
