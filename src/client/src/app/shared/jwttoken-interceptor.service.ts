import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { AppConstant } from '../app.constant';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class JWTTokenInterceptorService {

    constructor(private localStorageService: LocalStorageService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        var authToken: any = this.localStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.TOKEN);
        if (authToken && authToken.token) {
            req = req.clone({ setHeaders: { "x-access-token": authToken.token } });
        }
        else {
            console.error("Token Empty");
        }

        return next.handle(req)
            .do(event => { },
            error => {

                if (error instanceof HttpErrorResponse && error.status >= 400) {
                    // Handle Error
                    console.error("Error Happend while communicating API");
                    console.error(`Info: url : ${error.url} status: ${error.status}`);
                }
            }
            );
    }

}
