import { Component, OnInit } from '@angular/core';
// import {CarService} from '../demo/service/carservice';
// import {EventService} from '../demo/service/eventservice';
// import {Car} from '../demo/domain/car';
import {SelectItem} from 'primeng/primeng';
import {MenuItem} from 'primeng/primeng';
import {DashboardService} from '../services/dashboard.service';
import { LocalStorageService } from '../shared/local-storage.service';
import { AppConstant } from '../app.constant';
import * as _ from "lodash";
import { DateformatPipe } from '../pipes/dateformat.pipe';
import { CurrencyPipe } from "@angular/common";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
 
})
export class DashboardComponent implements OnInit {
    
    cities: SelectItem[];
    
    
    chartData: any;
    
    events: any[];
    
    selectedCity: any;
    
    items: MenuItem[];

    tenantInfo;
    finyear;   
    defaultBarChartdata;
    defaultPieChartData;

    invoiceSummaryData;
    billSummaryData;
    topSalesData;
    taxSummaryData;
    profitLossSummaryData;  
    currencyFilter:CurrencyPipe;
    currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
    transactionCount={
        invoice:0,
        bill:0,
        payment:0,
        receipt:0,
    }; 
    transactionNoRecordDisp={
        invoice:false,
        bill:false,
        topsales:false,
        profitLoss:false,
    }
  
    currentDate:string;
    self ;
    barChartConfig:any={
        self :this,
        legend:{
                 position: 'bottom'
                },
        scales: {
            yAxes:[{
                    ticks: {                                                                  
                        //max: parseInt(maxamount.toFixed(2)) - 2,
                        callback(value, index, values){                                        
                            return this.currencyFilter.transform(value,"USD",true,"1.0-0").substr(3);
                        }
                    }
                }] 
        },
        tooltips: {
                enabled: true,
                mode: 'single',
                callbacks: {
                    label (tooltipItems, data) {
                        return this._options.self.currencyFilter.transform( tooltipItems.yLabel,"USD",true,"1.0-0").substr(3);
                    }
                }
            },
            
    };

    chartConfig={
      self :this,
      legend: {
                position: 'bottom'
            },
             tooltips: {
                enabled: true,
                mode: 'single',
                callbacks: {
                   label (tooltipItem, data) {
                    var allData = data.datasets[tooltipItem.datasetIndex].data;
                    var tooltipLabel = data.labels[tooltipItem.index];
                    var tooltipData = allData[tooltipItem.index];
                    
                    return tooltipLabel+":"+this._options.self.currencyFilter.transform( tooltipData,"USD",true,"1.0-0").substr(3);
                }
                }
            }        
    }; 

    constructor(
                private dashboardService : DashboardService,
                private localStorageServcie : LocalStorageService,
                private dateFormatPipeFilter:DateformatPipe,
                ) {
                     this.tenantInfo = localStorageServcie.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
                     this.finyear = localStorageServcie.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);   
                     
                     this.currentDate = this.dateFormatPipeFilter.transform(new Date(), AppConstant.API_CONFIG.ANG_DATE.apiFormat);
                     this.currencyFilter = new CurrencyPipe("en-in");
                     console.log(this.currencyFilter.transform(1235,"USD",false));
                     this.self = this;
                  }
    
    ngOnInit() {

        this.initBarChartConfig(); 

        // Begin     
        let commonParam:any = {};
        commonParam.tenantid = this.tenantInfo.tenantid;
        commonParam.finyear = this.finyear.finyear;
        
        // Load Required datasets
        this.getInvoiceSummary(commonParam);
        this.getBillSummary(commonParam);
        this.getTopCustomerBySales(commonParam);
        this.getTaxSummary(commonParam);
        this.getProfitandLoss(commonParam);
        this.getCurrentDateInvoiceCount(commonParam);
        this.getCurrentDateBillCount(commonParam);
        this.getCurrentDatePaymentCount(commonParam);
        this.getCurrentDateReceiptCount(commonParam);
        // End
  
        
        this.cities = [];
        this.cities.push({label:'Select City', value:null});
        this.cities.push({label:'New York', value:{id:1, name: 'New York', code: 'NY'}});
        this.cities.push({label:'Rome', value:{id:2, name: 'Rome', code: 'RM'}});
        this.cities.push({label:'London', value:{id:3, name: 'London', code: 'LDN'}});
        this.cities.push({label:'Istanbul', value:{id:4, name: 'Istanbul', code: 'IST'}});
        this.cities.push({label:'Paris', value:{id:5, name: 'Paris', code: 'PRS'}});    
        
        this.chartData = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    label: 'First Dataset',
                    data: [65, 59, 80, 81, 56, 55, 40],
                    fill: false,
                    borderColor: '#FFC107'
                },
                {
                    label: 'Second Dataset',
                    data: [28, 48, 40, 19, 86, 27, 90],
                    fill: false,
                    borderColor: '#03A9F4'
                }
            ]
        };

        this.items = [
            {label: 'Save', icon: 'fa fa-check'},
            {label: 'Update', icon: 'fa fa-refresh'},
            {label: 'Delete', icon: 'fa fa-trash'}
        ];
    }

    initBarChartConfig(){
        var self = this;
        this.barChartConfig.scales={                    
            yAxes:[{
                    ticks: {                                                                  
                        //max: parseInt(maxamount.toFixed(2)) - 2,
                        callback(value, index, values){                                        
                            return self.currencyFilter.transform(value,"USD",true,"1.0-0").substr(3);
                        }
                    }
                    }]       
        }
    }

    getInvoiceSummary(queryParam){
        let label_1= "Total Open Invoice";
        let label_2= "Total Due Invoice";
        this.dashboardService.getInvoiceSummary(queryParam)
        .then((response:any)=>{
            if(response.status)
            {

                /**
                 * 
                 * Chart Data Format
                 * 
                 * {
                        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                        datasets: [
                            {
                                label: 'My First dataset',
                                backgroundColor: '#42A5F5',
                                borderColor: '#1E88E5',
                                data: [65, 59, 80, 81, 56, 55, 40]
                            },
                            {
                                label: 'My Second dataset',
                                backgroundColor: '#9CCC65',
                                borderColor: '#7CB342',
                                data: [28, 48, 40, 19, 86, 27, 90]
                            }
                      ]
                  }  
                  
                */
                if(!response.data || !response.data.length)
                {
                    this.invoiceSummaryData = this.dashboardService.getDefaultBarChartData(label_1,label_2);
                    this.transactionNoRecordDisp.invoice = true;
                    return false;
                }
                
                let invoiceChartData:any = {};
                let labels=[];
                let datasets=[
                            {
                                label:label_1,
                                backgroundColor: '#50a72e',
                                borderColor: '#50a72e',
                                data: []
                            },
                            {
                                label:label_2,
                                backgroundColor: '#e9453b',
                                borderColor: '#e9453b',
                                data: []
                            }

                ];
                let isNonZeroExists = false;
                let minamount = response.data[0].invamount;
                let maxamount = response.data[0].invamount;
                 _.forEach(response.data,(value:any)=>{

                     if(value.invamount || value.balamount)
                     {
                         isNonZeroExists = true;
                     }

                     if (value.invamount > maxamount) {
                        maxamount = value.invamount;
                    }
                    else if(value.invamount < minamount) {
                        minamount = value.invamount;
                    }

                      datasets[0].data.push(value.invamount);
                      datasets[1].data.push(value.balamount);
                      labels.push(value.label);
                 });

                 if(!isNonZeroExists)
                 {
                     this.invoiceSummaryData = this.dashboardService.getDefaultBarChartData(label_1,label_2);
                     this.transactionNoRecordDisp.invoice = true;
                     return;
                 }
                 
                 invoiceChartData.labels=labels;
                 invoiceChartData.datasets=datasets;

                 // Prepare Chart Config Object
                 var self = this;
                //  this.barChartConfig.scales={                    
                //         yAxes:[{
                //                ticks: {                                                                  
                //                     //max: parseInt(maxamount.toFixed(2)) - 2,
                //                     callback(value, index, values){                                        
                //                         return self.currencyFilter.transform(value,"USD",true).substr(3);
                //                     }
                //                 }
                //                 }]       
                //  }
                 this.invoiceSummaryData = invoiceChartData;
            } 
            else
            {
                this.invoiceSummaryData = this.dashboardService.getDefaultBarChartData(label_1,label_2);
                this.transactionNoRecordDisp.invoice = true;
            }
            
        })
    }
    getBillSummary(queryParam){
        let label_1="Total Open Bills";
        let label_2="Total Due Bills";
        this.dashboardService.getBillSummary(queryParam)
        .then((response:any)=>{
            if(response.data)
            {
               if(!response.data || !response.data.length)
                {
                    this.billSummaryData = this.dashboardService.getDefaultBarChartData(label_1,label_2);
                    this.transactionNoRecordDisp.bill = true;
                    return false;
                }
                
                let billChartData:any = {};
                let labels=[];
                let datasets=[
                            {
                                label:label_1,
                                backgroundColor: '#50a72e',
                                borderColor: '#50a72e',
                                data: []
                            },
                            {
                                label:label_2,
                                backgroundColor: '#e9453b',
                                borderColor: '#e9453b',
                                data: []
                            }

                ];
                let isNonZeroExists = false;
                 _.forEach(response.data,(value:any)=>{

                        if(value.billamount || value.balamount)
                        {
                            isNonZeroExists = true;
                        }

                      datasets[0].data.push(value.billamount);
                      datasets[1].data.push(value.balamount);
                      labels.push(value.label);
                 });

                 if(!isNonZeroExists)
                 {
                     this.billSummaryData = this.dashboardService.getDefaultBarChartData(label_1,label_2);
                    this.transactionNoRecordDisp.bill = true;                     
                     return;
                 }  

                 billChartData.labels=labels;
                 billChartData.datasets=datasets;
                                 
                 this.billSummaryData = billChartData;
            }
            else
            {
                this.billSummaryData = this.dashboardService.getDefaultBarChartData(label_1,label_2);
                this.transactionNoRecordDisp.bill = true;                
            }

            
        })
    }
    getTopCustomerBySales(queryParam){
        let copyParam = {... queryParam};
        copyParam.count = 4;

        let defaultLabel = ['Customer1','Customer2','Customer3'];

        this.dashboardService.getTopCustomerBySales(copyParam)
        .then((response:any)=>{
            /**
               * {
                        labels: ['A','B','C'],
                        datasets: [
                            {
                                data: [300, 50, 100],
                                backgroundColor: [
                                    "#FF6384",
                                    "#36A2EB",
                                    "#FFCE56"
                                ],
                                hoverBackgroundColor: [
                                    "#FF6384",
                                    "#36A2EB",
                                    "#FFCE56"
                                ]
                            }]    
                        };
                }
             */
            if(response.status)
            {
                if(!response.data || !response.data.length)
                {
                  this.topSalesData = this.dashboardService.getDefaultPieChartData(...defaultLabel);
                  this.transactionNoRecordDisp.topsales = true;                  
                  return;
                }

                let topSalesChartData:any = {};
                let labels=[];
                let datasets=[
                            {       
                                data: [],                         
                                backgroundColor: [
                                    "#FF6384",
                                    "#36A2EB",
                                    "#FFCE56"
                                ],
                                hoverBackgroundColor: [
                                    "#FF6384",
                                    "#36A2EB",
                                    "#FFCE56"
                                ]                               
                            }                            

                ];
                let isNonZeroExists = false;
                 _.forEach(response.data,(value:any)=>{
                     if(value.amount)
                     {
                         isNonZeroExists = true;
                     }
                      datasets[0].data.push(value.amount);
                      labels.push(value.label);
                 });

                 if(!isNonZeroExists)
                 {
                     this.topSalesData = this.dashboardService.getDefaultPieChartData(...defaultLabel);
                     this.transactionNoRecordDisp.topsales = true; 
                     return;
                 } 

                 topSalesChartData.labels=labels;
                 topSalesChartData.datasets=datasets;
                                 
                 this.topSalesData = topSalesChartData;
            }
            else{
                 this.topSalesData = this.dashboardService.getDefaultPieChartData(...defaultLabel);
                 this.transactionNoRecordDisp.topsales = true; 
            } 
           
        })
    }
    getTaxSummary(queryParam){
        this.dashboardService.getTaxSummary(queryParam)
        .then((response:any)=>{
            if(response.data)
            {
               if(!response.data || !response.data.length)
                {
                    this.taxSummaryData = this.dashboardService.getDefaultPieChartData();
                    return false;
                }
                
                /**
                 * 
                 *  let taxSummaryChartData:any = {};
                    let labels=[];
                    let datasets=[
                                {
                                    label:"Taxes",
                                    backgroundColor: '#9CCC65',
                                    borderColor: '#7CB342',
                                    data: []
                                }                            

                    ];
                    _.forEach(response.data,(value:any,ind)=>{
                          datasets[0].data.push(value.amount);
                          labels.push(value.label);
                    });
                    taxSummaryChartData.labels=labels;
                    taxSummaryChartData.datasets=datasets;
                 */
               
                  let taxSummaryChartData:any = {};
                  let labels=[];
                  let datasets=[
                              {       
                                  data: [],                         
                                  backgroundColor: [
                                      "#FF6384",
                                      "#36A2EB",
                                      "#FFCE56"
                                  ],
                                  hoverBackgroundColor: [
                                      "#FF6384",
                                      "#36A2EB",
                                      "#FFCE56"
                                  ]                               
                              }                            

                  ];
                 let isNonZeroExists = false;
                 _.forEach(response.data,(value:any,ind)=>{       
                      if(value.amount)
                     {
                         isNonZeroExists = true;
                     }            
                      datasets[0].data.push(value.amount);                 
                      labels.push(value.label);
                    
                 });
                  if(!isNonZeroExists)
                 {
                     this.taxSummaryData = this.dashboardService.getDefaultPieChartData();
                     return;
                 }

                 taxSummaryChartData.labels=labels;
                 taxSummaryChartData.datasets=datasets;
                                 
                 this.taxSummaryData = taxSummaryChartData;
            }
            else
            {
                this.taxSummaryData = this.dashboardService.getDefaultPieChartData();
            }            
        })
    }
    getProfitandLoss(queryParam){
        this.dashboardService.getCreditDebitSummary(queryParam)
        .then((response:any)=>{
            if(response.status)
            {
                if(!response.data || !response.data.length)
                {
                    this.profitLossSummaryData = this.dashboardService.getDefaultPieChartData(...["NetIncome","Expenses","P&L"]);
                    this.transactionNoRecordDisp.profitLoss= true; 
                    return false;
                }

                 let profileLossSummaryChartData:any = {};
                  let labels=[];
                  let datasets=[
                              {       
                                  data: [],                         
                                  backgroundColor: [
                                      "#FF6384",
                                      "#36A2EB",
                                      "#FFCE56"
                                  ],
                                  hoverBackgroundColor: [
                                      "#FF6384",
                                      "#36A2EB",
                                      "#FFCE56"
                                  ]                               
                              }                            

                  ];
                  let isNonZeroExists = false;
                 _.forEach(response.data,(value:any,ind)=>{
                    if(value.amount)
                    {
                      isNonZeroExists = true;
                    }
                    datasets[0].data.push(value.amount);                      
                    labels.push(value.label); 
                 });
                 if(!isNonZeroExists)
                 {
                   this.profitLossSummaryData = this.dashboardService.getDefaultPieChartData(...["NetIncome","Expenses","P&L"]);
                   this.transactionNoRecordDisp.profitLoss= true; 
                   return;
                 }

                 profileLossSummaryChartData.labels=labels;
                 profileLossSummaryChartData.datasets=datasets;
                                 
                 this.profitLossSummaryData = profileLossSummaryChartData;

            }
            else{
              this.profitLossSummaryData = this.dashboardService.getDefaultPieChartData(...["NetIncome","Expenses","P&L"]);
              this.transactionNoRecordDisp.profitLoss= true; 
           }
        })
    }

    getCurrentDateInvoiceCount(commonParam){
       let queryParam = {... commonParam}; 
       queryParam.invoicedt = this.currentDate;
       this.dashboardService.getInvoiceCount(queryParam)
        .then((response:any)=>{
            if(response.status)
                this.transactionCount.invoice = response.data;
        });
    }
    getCurrentDateBillCount(commonParam){
       let queryParam = {... commonParam}; 
       queryParam.billdt = this.currentDate;
       this.dashboardService.getBillCount(queryParam)
        .then((response:any)=>{
            if(response.status)
                this.transactionCount.bill = response.data;
        });
    }
    getCurrentDatePaymentCount(commonParam){
       let queryParam = {... commonParam}; 
       queryParam.pymtrectdt = this.currentDate;
       queryParam.feature = "Payment";
       this.dashboardService.getReceiptorPaymentCount(queryParam)
        .then((response:any)=>{
            if(response.status)
                this.transactionCount.payment = response.data;
        });
    }
    getCurrentDateReceiptCount(commonParam){
        let queryParam = {... commonParam}; 
       queryParam.pymtrectdt = this.currentDate;
       queryParam.feature = "Receipt";
       this.dashboardService.getReceiptorPaymentCount(queryParam)
        .then((response:any)=>{
            if(response.status)
                this.transactionCount.receipt = response.data;
        });
    }

}
