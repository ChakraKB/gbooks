import { Component, OnInit, Inject, forwardRef } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../app.constant';
import { AppTopBar } from '../app.topbar.component';
import { LocalStorageService } from '../shared/local-storage.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  userData: any;
  sessionrefkey: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private httpClient: HttpClient,
    public localStorage: LocalStorageService,
  ) { }

  ngOnInit() {

    this.route.paramMap
      .subscribe((params: ParamMap) => {
        this.sessionrefkey = params.get("sessionrefkey");
        let queryParam = { "sessionKey": this.sessionrefkey };
        this.checkSession(queryParam).then((response: any) => {
          if (response.status) {
            this.userData = response.data;
            this.initSession();
          }
          else {
            localStorage.clear();
            window.location.href = AppConstant.ACCOUNT.ACC_URL + "signin.html";
          }
        });
      });
  }

  checkSession(queryParam) {
    let url = AppConstant.ACCOUNT.BASE_URL + AppConstant.ACCOUNT.API.SESSION_INFO + queryParam.sessionKey;
    return this.httpClient.get(url).toPromise().
      catch(e => {
        console.log("error happend", e);
      });
  }

  initSession() {
    this.localStorage.addItem(AppConstant.API_CONFIG.LOCALSTORAGE.STR_AUTH, "true");
    this.localStorage.addItem(AppConstant.API_CONFIG.LOCALSTORAGE.STR_AUTHSUCCESS, "true");
    this.userData.tenantname = this.userData.fullname ? this.userData.fullname : 'GNTS';
    this.localStorage.addItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER, this.userData);
    var finyear = AppConstant.API_CONFIG.FINYEARS[2];

    this.localStorage.addItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR, finyear);
    // if (this.userData.rolename != 'SuperAdmin')
    // {
    //     localStorage.setItem(prefix+AppConstant.API_CONFIG.LOCALSTORAGE.HEADS, JSON.stringify(this.sessionData.heads));
    // }
    var token = { 'token': this.sessionrefkey }
    //localStorage.setItem(prefix+AppConstant.API_CONFIG.LOCALSTORAGE.USERROLE, JSON.stringify(this.sessionData.roles));
    var resources = AppConstant.API_CONFIG.FINYEARS[2];

    this.localStorage.addItem(AppConstant.API_CONFIG.LOCALSTORAGE.RESOURCE, resources);
    this.localStorage.addItem(AppConstant.API_CONFIG.LOCALSTORAGE.TOKEN, token);


    // Redirect to Dashboard Page
    this.router.navigate(['']);


  }

}
