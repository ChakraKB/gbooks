import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewInvoiceComponent } from '../sales/view-invoice/view-invoice.component';
import { ViewBillComponent } from '../purchase/view-bill/view-bill.component';
import { ReceiptViewComponent } from '../receipts/receipt-view/receipt-view.component'
import { ViewPaymentComponent } from '../payment/view-payment/view-payment.component'
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
    ViewInvoiceComponent,
    ViewBillComponent,
    ReceiptViewComponent,
    ViewPaymentComponent
  ],
  exports:
  [
    ViewInvoiceComponent,
    ViewBillComponent,
    ReceiptViewComponent,
    ViewPaymentComponent
  ]
})
export class SharedModuleModule { }
