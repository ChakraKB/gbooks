import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../shared/common-http.service';
import { AppConstant } from '../../app.constant'

@Injectable()
export class ReportService {
  appendpoint: string;
  trailbalance_url: string;
  ledger_url: string;
  filterledger_url: string;
  balancesheet_url: string;
  profitandloss_url: string;
  invoicereportlist: string;
  billreportlist: string;
  payrecreportlist :any;
  constructor(private httpService: CommonHttpService) {
    this.appendpoint = AppConstant.API_ENDPOINT;
    this.trailbalance_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.REPORTS.TB;
    this.ledger_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.BOOKOFACCOUNTS.LIST;
    this.filterledger_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.REPORTS.LEDGER;
    this.balancesheet_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.REPORTS.BALANCESHEET;
    this.profitandloss_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.REPORTS.PL;
    this.invoicereportlist = this.appendpoint + AppConstant.API_CONFIG.API_URL.REPORTS.INVOICEREG;
    this.billreportlist = this.appendpoint + AppConstant.API_CONFIG.API_URL.REPORTS.BILLREG;
    this.payrecreportlist = this.appendpoint + AppConstant.API_CONFIG.API_URL.REPORTS.PAYRECLIST;
  }
  public getTBreport(data: any): Promise<any> {
    return this.httpService.globalPostService(this.trailbalance_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public getLedgerlist(data: any): Promise<any> {
    return this.httpService.globalPostService(this.ledger_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public getFilteredLedgerlist(data: any): Promise<any> {
    return this.httpService.globalPostService(this.filterledger_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public getBalanceSheetreport(data: any): Promise<any> {
    return this.httpService.globalPostService(this.balancesheet_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public getProfitandLossreport(data: any): Promise<any> {
    return this.httpService.globalPostService(this.profitandloss_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public GetInvoiceReportlist(data: any): Promise<any> {
    return this.httpService.globalPostService(this.invoicereportlist, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public GetBillReportlist(data: any): Promise<any> {
    return this.httpService.globalPostService(this.billreportlist, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public GetPayRecReportlist(data: any): Promise<any> {
    return this.httpService.globalPostService(this.payrecreportlist, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
}
