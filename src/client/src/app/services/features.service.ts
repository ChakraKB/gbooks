import { Injectable } from '@angular/core';
import { CommonHttpService } from '../shared/common-http.service';
import { AppConstant } from '../app.constant';
import * as _ from "lodash";

@Injectable()
export class FeaturesService {
  appendpoint: string;
  acconfiglist_url: string;
  contactlist_url: string;
  invoicetypelist_url: string;
  codemaster_url: string;
  productlist_url: string;
  taxlist_url: string;
  bookofacclist_url: string;
  hsncodelist_url: string;
  accheads_list: string;
  allpaymode: string;
  allpayterm: string;
  logoutUrl : string;
  constructor(private httpService: CommonHttpService) {
    this.appendpoint = AppConstant.API_ENDPOINT;
    this.acconfiglist_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.TNT_ACCOUNTCONFIG.LIST;
    this.contactlist_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.CONTACT.LIST; //new api for contact list instead of contact FINDALL
    this.invoicetypelist_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.TNT_ACCOUNTCONFIG.LIST;
    this.codemaster_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.TNT_CODEMASTER.LIST;
    this.productlist_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.TNT_PRODUCTS.LIST;
    this.taxlist_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.TNT_TAXES.LIST;
    this.bookofacclist_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.ACC_BOOKOFACCOUNTS.LIST;
    this.accheads_list = this.appendpoint + AppConstant.API_CONFIG.API_URL.ACC_HEADS.LIST;
    this.allpaymode = this.appendpoint + AppConstant.API_CONFIG.API_URL.PAYTERMS.RECPAYMODE;
    this.allpayterm = this.appendpoint + AppConstant.API_CONFIG.API_URL.TNT_CODEMASTER.LIST;
    this.hsncodelist_url = this.appendpoint + AppConstant.API_CONFIG.API_URL.TNT_PRODUCTS.HSNCODE;
    this.logoutUrl = AppConstant.ACCOUNT.BASE_URL+ AppConstant.ACCOUNT.API.LOGOUT;
  }

  // params : type will be Customer,Vendor 
  public contactGetAll(data: any): Promise<any> {
    return this.httpService.globalPostService(this.contactlist_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }

  // params : Invoice,Bill,Journal,ProfomaInv
  public getacconfigList(data: any): Promise<any> {
    return this.httpService.globalPostService(this.acconfiglist_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }

  // params : Invoice,Bill,Journal,POS
  public getcodemasterList(data: any): Promise<any> {
    return this.httpService.globalPostService(this.codemaster_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }

  public ProductList(data: any): Promise<any> {
    return this.httpService.globalPostService(this.productlist_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public TaxList(data: any): Promise<any> {
    return this.httpService.globalPostService(this.taxlist_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  // get all book of accounts
  public AccHeadList(data: any): Promise<any> {
    return this.httpService.globalPostService(this.accheads_list, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  // get all book of accounts
  public BookofAccList(data: any): Promise<any> {
    return this.httpService.globalPostService(this.bookofacclist_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }

  public paytermGetAll(data: any): Promise<any> {
    return this.httpService.globalPostService(this.allpayterm, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public paymodeGetAll(data: any): Promise<any> {
    return this.httpService.globalPostService(this.allpaymode, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public hsnGetAll(data: any): Promise<any> {
    return this.httpService.globalPostService(this.hsncodelist_url, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
  public LogOut(data: any): Promise<any> {
    return this.httpService.globalGetServiceByUrl(this.logoutUrl, data)
      .then(data => {
        console.log(data);
        return data;
      });
  }
}
