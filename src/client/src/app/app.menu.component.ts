import { Component, Input, OnInit, EventEmitter, ViewChild, Inject, forwardRef } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import { AppComponent } from './app.component';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    @Input() reset: boolean;

    model: any[];

    constructor( @Inject(forwardRef(() => AppComponent)) public app: AppComponent) { }

    ngOnInit() {
        this.model = [
            { label: 'Get Started', icon: 'fa fa-fw fa-lightbulb-o', routerLink: ['/getstarted'] },
            { label: 'Dashboard', icon: 'fa fa-fw fa-home', routerLink: ['/'] },
            {
                label: 'Accounts', icon: 'fa fa-fw fa-book',
                items: [
                    { label: 'Chart of accounts', icon: 'fa fa-fw fa-columns', routerLink: ['/accounts/chartofaccounts'] },
                    { label: 'Journals', icon: 'fa fa-fw fa-code', routerLink: ['/accounts/journals/list']  },
                    { label: 'Credit Note', icon: 'fa fa-fw fa-table', routerLink: ['/accounts/creditnote'] },
                    { label: 'Debit Note', icon: 'fa fa-fw fa-list-alt', routerLink: ['/accounts/debitnote'] },
                ]
            },
            {
                label: 'Bank', icon: 'fa fa-fw fa-university',
                items: [
                    { label: 'Bank Withdraw', icon: 'fa fa-fw fa-columns', routerLink: ['/banks/withdraw'] },
                    { label: 'Bank Deposit', icon: 'fa fa-fw fa-code', routerLink: ['/banks/deposit'] },
                    { label: 'Bank Transfer', icon: 'fa fa-fw fa-code', routerLink: ['/banks/transfer'] },
                    { label: 'BRS', icon: 'fa fa-fw fa-code', routerLink: ['/banks/brslist'] },
                ]
            },
            { label: 'Products', icon: 'fa fa-fw fa-shopping-cart', routerLink: ['/products'] },
            {
                label: 'Sales', icon: 'fa fa-fw fa-money',
                items: [
                    { label: 'Invoice', icon: 'fa fa-fw fa-columns', routerLink: ['/sales/list'] },
                    // { label: 'pro-forma Invoice', icon: 'fa fa-fw fa-columns', routerLink: ['/sales/proformainvoice'] },
                    { label: 'Customer Advance', icon: 'fa fa-fw fa-code', routerLink: ['/sales/customeradvance'] },
                  ]
            },
            { label: 'Receipts', icon: 'fa fa-fw fa-shopping-cart', routerLink: ['/receipts/list'] },
            {
                label: 'Purchase', icon: 'fa fa-fw fa-file-text',
                items: [
                    { label: 'Bills', icon: 'fa fa-fw fa-columns', routerLink: ['/purchase/list'] },
                    { label: 'Vendor Advance', icon: 'fa fa-fw fa-code', routerLink: ['/purchase/vendoradvance'] },
                ]
            },
            { label: 'Payments', icon: 'fa fa-fw fa-credit-card', routerLink: ['/payment/list'] },
            {
                label: 'GST Filing', icon: 'fa fa-fw fa-files-o',
                items: [
                    { label: 'Tax Summary', icon: 'fa fa-fw fa-columns', routerLink: ['/sales/list'] },
                ]
            },
            { label: 'Reports', icon: 'fa fa-fw fa-file', routerLink: ['/reports/list'] },
        ];
    }

    changeTheme(theme) {
        let themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
        themeLink.href = 'assets/theme/theme-' + theme + '.css';
    }

    changeLayout(layout) {
        this.app.layout = layout;
        let layoutLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('layout-css');
        layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';
    }
}

@Component({
    selector: '[app-submenu]',
    template: `
        <ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
            <li [ngClass]="{'active-menuitem': isActive(i)}" [class]="child.badgeStyleClass">
                <a [href]="child.url||'#'" (click)="itemClick($event,child,i)" *ngIf="!child.routerLink" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
                    (mouseenter)="hover=true" (mouseleave)="hover=false">
                    <i [ngClass]="child.icon"></i>
                    <span>{{child.label}}</span>
                    <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                    <i class="fa fa-fw fa-angle-down" *ngIf="child.items"></i>
                </a>

                <a (click)="itemClick($event,child,i)" *ngIf="child.routerLink"
                    [routerLink]="child.routerLink" routerLinkActive="active-menuitem-routerlink" [routerLinkActiveOptions]="{exact: true}" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
                    (mouseenter)="hover=true" (mouseleave)="hover=false">
                    <i [ngClass]="child.icon"></i>
                    <span>{{child.label}}</span>
                    <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                    <i class="fa fa-fw fa-angle-down" *ngIf="child.items"></i>
                </a>
                <div class="layout-menu-tooltip">
                    <div class="layout-menu-tooltip-arrow"></div>
                    <div class="layout-menu-tooltip-text">{{child.label}}</div>
                </div>
                <ul app-submenu [item]="child" *ngIf="child.items" [visible]="isActive(i)" [reset]="reset"
                    [@children]="app.slimMenu&&root ? isActive(i) ? 'visible' : 'hidden' : isActive(i) ? 'visibleAnimated' : 'hiddenAnimated'"></ul>
            </li>
        </ng-template>
    `,
    animations: [
        trigger('children', [
            state('hiddenAnimated', style({
                height: '0px'
            })),
            state('visibleAnimated', style({
                height: '*'
            })),
            state('visible', style({
                height: '*'
            })),
            state('hidden', style({
                height: '0px'
            })),
            transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppSubMenu {

    @Input() item: MenuItem;

    @Input() root: boolean;

    @Input() visible: boolean;

    _reset: boolean;

    activeIndex: number;

    hover: boolean;

    constructor( @Inject(forwardRef(() => AppComponent)) public app: AppComponent, public router: Router, public location: Location) { }

    itemClick(event: Event, item: MenuItem, index: number) {
        //avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        //activate current item and deactivate active sibling if any
        if (item.routerLink || item.items) {
            this.activeIndex = (this.activeIndex === index) ? null : index;
        }

        //execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }

        //prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            event.preventDefault();
        }

        //hide menu
        if (!item.items) {
            if (this.app.overlayMenu || this.app.isMobile()) {
                this.app.overlayMenuActive = false;
                this.app.mobileMenuActive = false;
            }

            if (!this.root && this.app.slimMenu) {
                this.app.resetSlim = true;
            }
        }
    }

    isActive(index: number): boolean {
        return this.activeIndex === index;
    }

    unsubscribe(item: any) {
        if (item.eventEmitter) {
            item.eventEmitter.unsubscribe();
        }

        if (item.items) {
            for (let childItem of item.items) {
                this.unsubscribe(childItem);
            }
        }
    }

    @Input() get reset(): boolean {
        return this._reset;
    }

    set reset(val: boolean) {
        this._reset = val;

        if (this._reset && this.app.slimMenu) {
            this.activeIndex = null;
        }
    }
}