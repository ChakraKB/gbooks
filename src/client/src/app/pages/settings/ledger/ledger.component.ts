import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { LedgerService } from '../services/ledger.service';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { AppConstant } from '../../../app.constant';
import { OverlayPanel } from 'primeng/primeng';
import { LocalStorageService } from '../../../../app/shared/local-storage.service';
import { DialogModule } from 'primeng/primeng';

@Component({
  selector: 'app-ledger',
  templateUrl: './ledger.component.html',
  styleUrls: ['./ledger.component.scss']
})
export class LedgerComponent implements OnInit {
  @Output() updatecategory = new EventEmitter();
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  display: boolean = false;
  ledgerlist: any[] = [];
  ledgerdetails: any = {};
  ledgerdtls: any[];
  editovrlay: any;
  addovrlay: any;
  show: boolean = false;
  adddialog_display:boolean=false
  list:boolean=true;
  constructor(private ledgerservice: LedgerService, private localStorageService: LocalStorageService) { }
  ngOnInit() {
    this.loadledger();
  }
  loadledger() {
    var ledgerdetails: any = this.localStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.ledgerservice.getAll({ tenantid: ledgerdetails.tenantid })
      .then(res => {
        this.ledgerlist = res.data;
      });
  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }
  loadledgerlist() {
    this.display = false;
    // ovrlay.hide();
    this.loadledger();
  }
  addledger(event, overlaypanel: DialogModule) {
    this.addovrlay = overlaypanel
    // overlaypanel.toggle(event);
    this.display = true;
    this.ledgerdtls = [];
  }
  update(event, ledgerdtls, overlaypanel: DialogModule) {
    this.ledgerdtls = ledgerdtls
    this.display = true;
    this.editovrlay = overlaypanel;
    // overlaypanel.toggle(event);
  }
}