import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LocalStorageService } from '../../../../../shared/local-storage.service';
import { AppConstant } from '../../../../../../app/app.constant';
import { LedgerService } from '../../../services/ledger.service'
import { DropdownModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import * as _ from "lodash";
import { MasterService } from '../../../../../services/master.service';
import { CommonService } from '../../../services/common.service';
import { Message } from 'primeng/primeng';
import { MessagesService } from '../../../../../shared/messages.service';
@Component({
  selector: 'app-addledger',
  templateUrl: './addledger.component.html',
  styleUrls: ['./addledger.component.scss']
})
export class AddledgerComponent implements OnInit {
  @Input() editovrlay: any;
  @Input() addovrlay: any;
  @Input() ledgerdtls: any;
  @Output() loadledgerlist: EventEmitter<any> = new EventEmitter();
  groups: any[];
  validation = "true";
  validationmsg = "";
  selectedgroup: any[];
  msgs: Message[] = [];
  ledgerform: FormGroup;
  firstname: FormControl;
  ledgerdetails: any = {};
  // openingbalance: FormControl;
  accheadname: FormControl;
  status: FormControl;
  userdetails: any;
  buttonText = "Save";
  changestatus: any[] = [];
  Status: any = [];
  constructor(public localstorageservice: LocalStorageService, private messageservice: MessagesService, private commonservice: CommonService, private ledgerservice: LedgerService, private fb: FormBuilder, private masterservice: MasterService) {

    this.ledgerform = fb.group({
      'firstname': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
      'accheadname': [null, Validators.required],
      'status': [null]
    })
    this.userdetails = localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.Status = AppConstant.API_CONFIG.status;
    for (var i = 0; i < this.Status.length; i++) {
      this.changestatus.push({
        label: this.Status[i].stat, value: this.Status[i].stat
      });
    }
  }

  ngOnInit() {
    this.buttonText = "Save";
    this.ledgergrp();

  }
  ngOnChanges() {
    this.vieweditledger();
  }
  vieweditledger() {
    this.ClearForm();
    if (!_.isEmpty(this.ledgerdtls)) {
      this.buttonText = "Update";
      let selectedgroup = _.find(this.groups, { value: { subaccheadid: this.ledgerdtls.accheadid } });
      this.ledgerform = new FormGroup({
        'firstname': new FormControl(this.ledgerdtls.subaccheadname,Validators.required),
        // 'openingbalance': new FormControl(this.ledgerdtls.openingbalance),
        'accheadname': new FormControl(selectedgroup.value,Validators.required),
        'status': new FormControl(this.ledgerdtls.status,Validators.required),

      })
    }
    else {
      this.ClearForm();
    }
  }
  ledgergrp() {
    this.groups = [];
    var rolelocaldetails: any = this.localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    var self = this;
    this.commonservice.FindAllGroups({"tenantid" : [0,this.userdetails.tenantid]})
      .then((res) => {
        var group = res.data;
        self.groups = self.masterservice.formatDataforDropdown("subaccheadname", group, "Select Group");

      });
  }
  loadlist(ovrlay) {
    this.loadledgerlist.next(ovrlay)
  }
  ClearForm() {
    this.buttonText = "Save";
    this.ledgerform = new FormGroup({
      'firstname': new FormControl(null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])),
      'accheadname': new FormControl(null, Validators.required)
      // 'openingbalance': new FormControl()
    });
  }

  formObj: any = {

    firstname: {
      required: "Please Enter Ledger Name",
      minlength: "Please enter atleast 3 characters",
      maxlength: "Please enter name between 50 characters"
    },
    accheadname: {
      required: "Please Select Group"
    },
    status: {
      required: "status Required"
    }
  }

  save(data) {

    if (this.ledgerform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.ledgerform, this.formObj);
      this.messageservice.showMessage({ severity: 'error', summary: 'Error Message', detail: errorMessage });
      return false;
    }
    if (!_.isEmpty(this.ledgerdtls)) {
      var formdata =
        {
          'subaccheadid': this.ledgerdtls.subaccheadid,
          'accheadid': data.accheadname.subaccheadid,
          'accheadname': data.accheadname.subaccheadname,
          'subaccheadname': data.firstname,
          'lastupdatedby': this.userdetails.tenantname,
          'lastupdateddt': new Date(),
          'tenantid': this.userdetails.tenantid,
          'tenantname': this.userdetails.tenantname,
          'type': "Ledger",
          'status': data.status
        }
      this.ledgerservice.update(formdata)
        .then(res => {
          if (res.status == true) {
            this.msgs = [];
            this.messageservice.showSuccessMessage({ severity: 'success', summary: 'Ledger Updated Successfully ', detail: '' });
            this.ClearForm();
            this.loadlist(this.editovrlay);
          } else {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
            this.ClearForm();
          }
        });
    }
    else {
      var form =
        {
          'accheadid': data.accheadname.subaccheadid,
          'accheadname': data.accheadname.subaccheadname,
          'crdr': data.accheadname.crdr,
          // 'openingbalance': data.openingbalance,
          'subaccheadname': data.firstname,
          'createdby': this.userdetails.tenantname,
          'createddt': new Date(),
          // 'lastupdatedby': this.userdetails.tenantname,
          // 'lastupdateddt': new Date(),
          'tenantid': this.userdetails.tenantid,
          'tenantname': this.userdetails.tenantname,
          'type': "Ledger",
          // 'Create': "Y",
          // 'gstno': "GSTNO",
          'status': "Active"
        }
      this.ledgerservice.create(form)
        .then(res => {
          if (res.status == true) {
            this.loadlist(this.addovrlay);
            this.msgs = [];
            this.messageservice.showSuccessMessage({ severity: 'success', summary: 'Ledger Created Successfully ', detail: '' });
            // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: res.message });
            this.ClearForm();

          } else {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
            this.ClearForm();
          }
        });
    }
  }

}
