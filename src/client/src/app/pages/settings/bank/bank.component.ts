import { Component, OnInit, ViewChild, EventEmitter, ElementRef } from '@angular/core';
import { BankService } from '../services/bank.service';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { AppConstant } from '../../../app.constant';
import { LocalStorageService } from '../../../../app/shared/local-storage.service';
import { AddbankComponent } from '../bank/addbank.component';
import { OverlayPanel, LazyLoadEvent } from 'primeng/primeng';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  banklist: any[] = [];
  allbanks: any;
  localStorageDetails: any = {};
  addbankDetails: boolean = false;
  editbankDetails: boolean = false;
  allproducts: any;
  editbank: any;
  addbank: boolean = false;
  addnew: boolean = false;
  list: boolean = true;
  show: boolean = false;
  name: string;
  totalRecords: number = 0;
  perPage: number = 10;
  page: number = 0;
  @ViewChild('wrapper') wrapper: ElementRef;
  constructor(private bankservice: BankService, private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.getAll();
  }
  getAll() {
    this.localStorageDetails = this.localStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.bankservice.getAll({ tenantid: this.localStorageDetails.tenantid })
      .then(res => {
        if (res.status) {
          this.allbanks = res.data;
          //this.totalRecords = 
        }
        else {
          console.log("no bank found");
        }
      });
  }
  loadBankLazy(event: LazyLoadEvent) {
    this.page = (event.first / this.perPage)
    this.bankservice.getAll({ tenantid: this.localStorageDetails.tenantid, limit: 10, offset: this.page * 10 })
      .then(res => {
        if (res.status) {
          this.allbanks = res.data;
          //this.totalRecords = res.count;
        }
        else {
          console.log("no bank found");
        }
      });
  }

  notifyNewBank(event) {
    this.getAll();
    this.addbankDetails = false;
    this.editbankDetails = false;
    this.addnew = false;
    this.editbank = false;
    this.list = true;
  }
  addBank() {
    this.addbankDetails = true;
    this.editbankDetails = false;
    this.addnew = true;
    this.editbank = false;
    this.list = false;
    this.banklist = [];
    // this.wrapper.nativeElement.click()
  }

  handleClose(e) {
    this.addbankDetails = false;
    this.editbankDetails = false;
    this.list = true;
  }

  update(banklist) {
    this.list = false;
    this.addbankDetails = false;
    this.editbankDetails = true;
    this.addnew = false;
    this.editbank = true;
    this.banklist = banklist;
  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }
}
