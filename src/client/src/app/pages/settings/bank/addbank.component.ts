import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as _ from "lodash";
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { BankService } from '../services/bank.service';
import { LocalStorageService } from '../../../../app/shared/local-storage.service';
import { AppConstant } from '../../../app.constant';
import { MasterService } from '../../../services/master.service';
import { Message } from 'primeng/primeng';
import { CommonService } from '../services/common.service';
import { UtilsService } from '../../../services/utils.service';
import { MessagesService } from '../../../shared/messages.service';


@Component({
  selector: 'app-addbank',
  templateUrl: './addbank.component.html',
  styleUrls: ['./addbank.component.scss']
})
export class AddbankComponent implements OnInit {
  localstorageDetails: any;
  bankDetails: any = {};
  selectedbank: any;
  selectedbankDetails: any;
  bankList: any = [];
  countryList: any = [];
  stateList: any = [];
  cityList: any = [];
  buttonText = "Save";
  buttonclick = false;
  validation = "true";
  validationmsg = "";
  msgs: Message[] = [];
  bankForm: FormGroup;
  bankname: FormControl;
  accountno: FormControl;
  ifsccode: FormControl;
  swiftcode: FormControl;
  branchname: FormControl;
  branchcode: FormControl;
  countryname: FormControl;
  statename: FormControl;
  cityname: FormControl;
  address: FormControl;
  zipcode: FormControl;
  allbanks: any[];
  formdata = {};
  Indate: any;
  selectstate: any[];
  selectcountry: any[];
  stateValue: any;
  changestatus: any[] = [];
  status: any = [];
  @Input() banklist: any;
  @Output() notifyNewBank: EventEmitter<any> = new EventEmitter();
  constructor(private formbuilder: FormBuilder, private bankservice: BankService, private localstorageservice: LocalStorageService,
    private masterservice: MasterService, private commonservice: CommonService, private UtilsService: UtilsService, private messageService: MessagesService) {
    this.localstorageDetails = this.localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.bankForm = formbuilder.group({
      'bankname': [null, Validators.required],
      'accountno': [null, Validators.required],
      'ifsccode': [null, Validators.required],
      'swiftcode': [null],
      'branchname': [null],
      'branchcode': [null],
      'countryname': [null],
      'statename': [null],
      'cityname': [null],
      'zipcode': [null],
      'address': [null],
      'status': [null]
    })
    this.status = AppConstant.API_CONFIG.status;
    for (var i = 0; i < this.status.length; i++) {
      this.changestatus.push({
        label: this.status[i].stat, value: this.status[i].stat
      });
    }
  }
  callParent(message) {
    this.notifyNewBank.next(message);
  }
  numberOnly(event) {
    this.UtilsService.allowNumberOnly(event);
  }
  ngOnInit() {
    // this.createFormControls();
    // this.createForm();
    this.buttonText = "Save";
    this.findAllBanks();
    this.loadcountries();
    // this.loadstates();
    // this.loadcities();
    var dateS = [];
    var today = new Date().toISOString()
    this.Indate = formatDate(today);
    function formatDate(date) {
      if (date) {
        dateS = date.split("-");
        var d = new Date();
        d.setMonth(dateS[1]);
        d.setFullYear(dateS[0]);
        d.setDate(parseInt(dateS[2]));
        var
          month = '' + (d.getMonth()),
          day = '' + d.getDate(),
          year = d.getFullYear();
        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
          day = '0' + day;
        return [year, month, day].join('-');
      }
    }
    setTimeout(() => {
      this.update()
    }, 1000);
  }
  childEvent(data, bankDetails) {
  }
  loadcountries() {
    this.countryList = [];
    var self = this;
    this.commonservice.FindAllCountry({})
      .then((res) => {
        if (res.status) {
          var country = res.data;
          self.countryList = self.masterservice.formatDataforDropdown("countryname", country, "Select Country");

          // this.countryList.push({label:'Select Country', value:null});
          // for(var i=0;i<res.data.length;i++){
          //   this.countryList.push({ label: res.data[i].countryname, value:{ 
          //     countryname:res.data[i].countryname,
          //     countryid:res.data[i].countryid
          //   }
          //   });
          // }
        }
      });
  }
  loadstates(selectcountry): Promise<any> {
    if(!_.isEmpty(selectcountry))
    {
    this.stateList = [];
    var self = this;
    return this.commonservice.FindAllState({ countryname: selectcountry })
      .then((res) => {
        if (res.status) {
          var state = res.data;
          self.stateList = self.masterservice.formatDataforDropdown("statename", state, "Select State");
          // console.log("state list",res);
          // this.stateList.push({label:'Select State', value:null});
          // for(var i=0;i<res.data.length;i++){
          //   this.stateList.push({ label: res.data[i].statename, value:{ 
          //     statename:res.data[i].statename,
          //     stateid:res.data[i].stateid
          //   }
          //   });
          // }
          // console.log("state list values",this.stateList);
        }
      });
    }
  }
  loadcities(selectstate): Promise<any> {
    this.cityList = [];
    var self = this;
    return this.commonservice.FindAllCity({ stateid: selectstate })
      .then((res) => {
        if (res.status) {
          var city = res.data;
          self.cityList = self.masterservice.formatDataforDropdown("cityname", city, "Select City");

          // console.log("city list",res);
          // this.cityList.push({label:'Select city', value:null});
          // for(var i=0;i<res.data.length;i++){
          //   this.cityList.push({ label: res.data[i].cityname, value:{ 
          //     cityname:res.data[i].cityname,
          //     cityid:res.data[i].cityid
          //   }
          //   });
          // }
          // console.log("city list values",this.cityList);
        }

      });
  }
  findAllBanks(): Promise<any> {
    var bankList = [];
    var self = this;
    return this.bankservice.getAllBanks({ accheadslist: ["BANK BALANCES"] })
      .then((res) => {
        if (res.status) {
          // var bank = res.data;
          // self.bankList = self.masterservice.formatDataforDropdown("subaccheadname", bank, "Select Bank");
          this.bankList = [];
          this.bankList.push({ label: 'Select Bank', value: null });
          for (var i = 0; i < res.data.length; i++) {
            this.bankList.push({
              label: res.data[i].subaccheadname, value: {
                bankname: res.data[i].subaccheadname,
                bankcode: res.data[i].subaccheadid
              }
            });
          }
        }
      });
  }
  clearform() {
    this.bankForm.reset();
  }

  oncountryChange(selectcountry) {
    this.selectcountry = _.isEmpty(selectcountry) ? null : selectcountry.countryname;
    this.loadstates(this.selectcountry);
  }
  onstateChange(statechange) {
    this.selectstate = _.isEmpty(statechange) ? null : statechange.stateid;
    this.loadcities(this.selectstate);
  }
  update() {
    if (this.banklist.bankid) {
      this.buttonText = "Update";
      let filterValue = _.find(this.countryList, { value: { countryid: this.banklist.countryid } });
      // let bankValue = _.find(this.bankList,{value:{bankname:this.banklist.bankname}});
      let countryValue = _.find(this.countryList, { value: { countryname: this.banklist.countryname } });
      // let stateValue = _.find(this.stateList,{value:{stateid:this.banklist.stateid}});  
      this.bankForm = new FormGroup({
        'bankname': new FormControl(null),
        'accountno': new FormControl(this.banklist.accountno),
        'ifsccode': new FormControl(this.banklist.ifsccode),
        'swiftcode': new FormControl(this.banklist.swiftcode),
        'branchname': new FormControl(this.banklist.branchname),
        'branchcode': new FormControl(this.banklist.branchcode),
        'countryname': new FormControl(_.isEmpty(countryValue) ? null : countryValue.value),
        'statename': new FormControl(null),
        'cityname': new FormControl(null),
        'zipcode': new FormControl(this.banklist.zipcode),
        'address': new FormControl(this.banklist.address),
        'status': new FormControl(this.banklist.status)
      });
      this.findAllBanks().then(() => {
        let bankValue = _.find(this.bankList, { value: { bankname: this.banklist.bankname, bankcode: this.banklist.bankcode } });
        this.bankForm.controls["bankname"].setValue(bankValue.value);
      })
      this.loadstates(countryValue.value.countryname).then(() => {
        let stateValue = _.find(this.stateList, { value: { stateid: this.banklist.stateid, statename: this.banklist.statename } });
        this.stateValue = stateValue;
        this.bankForm.controls["statename"].setValue(stateValue.value);
      })
      this.loadcities(this.stateValue).then(() => {
        let cityValue = _.find(this.cityList, { value: { cityid: this.banklist.cityid, cityname: this.banklist.cityname } });
        this.bankForm.controls["cityname"].setValue(cityValue.value);
      })
      //  this.banklist.bankid = this.banklist.bankid;
    }
  }

  formObj: any = {
    bankname: {
      required: "Please Select Bank Name",
    },
    accountno: {
      required: "Please Enter Account Number",
    },
    ifsccode: {
      required: "Please Enter IFSC Code"
    }
  }

  formsubmit(data) {

    if (this.bankForm.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.bankForm, this.formObj);
      this.messageService.showMessage({ severity: 'error', summary: 'Error Message', detail: errorMessage });
      return false;
    }
    if (this.banklist.bankid) {
      this.formdata = {
        'bankid': this.banklist.bankid,
        'bankname': data.bankname.bankname,
        'bankcode': data.bankname.bankcode,
        'countryname': _.isEmpty(data.countryname) ? null : data.countryname.countryname,
        'countryid': _.isEmpty(data.countryname) ? null : data.countryname.countryid,
        'statename': _.isEmpty(data.statename) ? null : data.statename.statename,
        'stateid': _.isEmpty(data.statename) ? null : data.statename.stateid,
        'cityname': _.isEmpty(data.cityname) ? null : data.cityname.cityname,
        'cityid': _.isEmpty(data.cityname) ? null : data.cityname.cityid,
        'accountno': data.accountno,
        'ifsccode': data.ifsccode,
        'swiftcode': _.isEmpty(data.swiftcode) ? null : data.swiftcode,
        'branchname': _.isEmpty(data.branchname) ? null : data.branchname,
        'branchcode': _.isEmpty(data.branchcode) ? null : data.branchcode,
        'tenantid': this.localstorageDetails.tenantid,
        'tenantname': this.localstorageDetails.tenantname,
        'zipcode': _.isEmpty(data.zipcode) ? null : data.zipcode,
        'address': _.isEmpty(data.address) ? null : data.address,
        'status': data.status,
        'lastupdatedby': this.localstorageDetails.loginname,
        'lastupdateddt': this.Indate
      }
      this.bankservice.updateBankDetails(this.formdata)
        .then((res) => {

          if (res.status == true) {
            this.msgs = [];
            this.messageService.showSuccessMessage({ severity: 'success', summary: 'Bank Updated Successfully ', detail: '' });
            // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: res.message });
            this.clearform();
            this.callParent(res.message);
          }
          else if (res.status == false) {
            this.msgs = [];
            this.messageService.showMessage({ severity: 'error', summary: 'Error ', detail: res.message });
            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });

          }
        });
    }
    else {
      this.formdata = {
        'bankname': data.bankname.bankname,
        'bankcode': data.bankname.bankcode,
        'countryname': _.isEmpty(data.countryname) ? null : data.countryname.countryname,
        'countryid': _.isEmpty(data.countryname) ? null : data.countryname.countryid,
        'statename': _.isEmpty(data.statename) ? null : data.statename.statename,
        'stateid': _.isEmpty(data.statename) ? null : data.statename.stateid,
        'cityname': _.isEmpty(data.cityname) ? null : data.cityname.cityname,
        'cityid': _.isEmpty(data.cityname) ? null : data.cityname.cityid,
        'accountno': data.accountno,
        'ifsccode': data.ifsccode,
        'swiftcode': _.isEmpty(data.swiftcode) ? null : data.swiftcode,
        'branchname': _.isEmpty(data.branchname) ? null : data.branchname,
        'branchcode': _.isEmpty(data.branchcode) ? null : data.branchcode,
        'tenantid': this.localstorageDetails.tenantid,
        'tenantname': this.localstorageDetails.tenantname,
        'zipcode': _.isEmpty(data.zipcode) ? null : data.zipcode,
        'address': _.isEmpty(data.address) ? null : data.address,
        'status': "Active",
        'createdby': this.localstorageDetails.loginname,
        'createddt': this.Indate
      }
      this.bankservice.saveBankDetails(this.formdata)
        .then((res) => {
          if (res.status == true) {
            this.msgs = [];
            this.messageService.showSuccessMessage({ severity: 'success', summary: 'Bank Created Successfully ', detail: '' });
            // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: res.message });
            this.clearform();
            this.callParent(res.message);
          }
          else if (res.status == false) {
            this.msgs = [];
            this.messageService.showMessage({ severity: 'error', summary: 'Error ', detail: res.message });
            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
            this.clearform();
          }
        });
    }



  }


}