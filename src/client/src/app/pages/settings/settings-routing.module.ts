import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SettingsComponent } from './settings.component';
import{ PartiesComponent }from'./parties/parties.component';
import{ LedgerComponent }from './ledger/ledger.component';
import{ BankComponent } from './bank/bank.component';
import{ BrandComponent } from './brand/brand.component';
import{ CategoryComponent } from './category/category.component'

const routes: Routes = [
  
    {
      path: 'settingslist',
      component: SettingsComponent,
      data: {
        title: 'Settings'
      }
    },
    { path: 'parties', component: PartiesComponent},
    { path: 'ledger', component: LedgerComponent},
    { path: 'bank', component: BankComponent},
    { path: 'brand', component: BrandComponent},
    { path: 'category', component: CategoryComponent},

    // children: [
     

    // ]
   //  component: CompanyComponent,
   
   
  
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    // SettingsComponent,
    // //RolesComponent,
    // OrganizationSettingsComponent,
    // ProfileSettingsComponent
  ]
  
})
export class SettingsRoutingModule { }