import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { AppConstant } from '../../../app.constant';
import { LocalStorageService } from '../../../../app/shared/local-storage.service';
import { AddcategoryComponent } from './addcategory/addcategory/addcategory.component';
import { OverlayPanel } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  display: boolean = false;
  adddialog_display = false;
  show: boolean = false;
  catglist: any[] = [];
  catgdetails: any = {};
  editovrlay: any;
  addovrlay: any;
  msgs:any;
  constructor(private categoryservice: CategoryService, private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.loadcategory();
  }
  loadcategory() {
    var catgdetails: any = this.localStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.categoryservice.getAll({ tenantid: catgdetails.tenantid })
      .then(res => {
        if (res.status) {
          this.catglist = res.data;
        }
        else {
          console.log("no category");
        }
      });
  }
  loadcategorylist() {
    this.display = false;
    this.adddialog_display = false;
    this.loadcategory();
  }
  addcategory(event, overlaypanel: DialogModule) {
    this.addovrlay = overlaypanel
    this.adddialog_display = true;
    // overlaypanel.toggle(event);
    this.catgdetails = [];
  }
  update(event, catgdetails, overlaypanel: DialogModule) {
    this.catgdetails = catgdetails
    this.display = true;
    this.editovrlay = overlaypanel;
    // overlaypanel.toggle(event);
  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }
}
