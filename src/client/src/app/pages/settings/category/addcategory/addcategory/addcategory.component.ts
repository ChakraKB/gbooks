import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LocalStorageService } from '../../../../../shared/local-storage.service';
import { AppConstant } from '../../../../../../app/app.constant';
import { CategoryService } from '../../../services/category.service'
import { CategoryComponent } from '../../category.component';
import * as _ from "lodash";
import { OverlayPanel, Message } from 'primeng/primeng';
import { MasterService } from '../../../../../services/master.service';
import { MessagesService } from '../../../../../shared/messages.service';

@Component({
  selector: 'app-addcategory',
  templateUrl: './addcategory.component.html',
  styleUrls: ['./addcategory.component.scss']
})
export class AddcategoryComponent implements OnInit {
  @Input() editovrlay: any;
  @Input() addovrlay: any;
  @Input() catgdetails: any;
  @Output() loadcategorylist: EventEmitter<any> = new EventEmitter();
  categoryform: FormGroup;
  categoryname: FormControl;
  userdetails: any;
  validation = "true";
  validationmsg = "";
  msgs: Message[] = [];
  buttonText = "Save";
  changestatus: any[] = [];
  status: any = [];
  private child: CategoryComponent;

  constructor(private fb: FormBuilder, private messageService: MessagesService, public localstorageservice: LocalStorageService, private categoryservice: CategoryService, private masterservice: MasterService) {
    this.userdetails = localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.categoryform = fb.group({
      'categoryname': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
      'status': [null, Validators.required]
    })
    this.status = AppConstant.API_CONFIG.status;
    for (var i = 0; i < this.status.length; i++) {
      this.changestatus.push({
        label: this.status[i].stat, value: this.status[i].stat
      });
    }
  }

  ngOnInit() {
    this.msgs = [];
    this.CreateFormControls();
    this.CreateForm();
  }
  ngOnChanges() {
    this.vieweditcategory();
  }
  loadlist(ovrlay) {
    this.loadcategorylist.next(ovrlay)
  }
  vieweditcategory() {
    this.ClearForm();
    if (!_.isEmpty(this.catgdetails)) {
      this.categoryform = new FormGroup({
        'categoryname': new FormControl(this.catgdetails.categoryname,Validators.required),
        'status': new FormControl(this.catgdetails.status,Validators.required),
      })
    }
    else {
      this.ClearForm();
    }
  }
  ClearForm() {
    this.msgs = [];
    this.categoryform = new FormGroup({
      'categoryname': new FormControl(null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)]))
    });
  }
  CreateFormControls() {
    this.categoryname = new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)]))
  }
  CreateForm() {
    this.categoryform = new FormGroup({
      categoryname: this.categoryname
    });
  }

  formObj: any = {
    categoryname: {
      required: "Please enter Category Name",
    },
    status: {
      required: "status Required"
    }
  }
  save(data) {

    if (this.categoryform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.categoryform, this.formObj);
      this.messageService.showMessage({ severity: 'error', summary: 'Error Message', detail: errorMessage });
      return false;
    }

    if (typeof this.catgdetails.categoryid == "number") {
      var formdata =
        {
          'categoryid': this.catgdetails.categoryid,
          'categoryname': data.categoryname,
          'lastupdatedby': this.userdetails.tenantname,
          'lastupdateddt': new Date(),
          'tenantid': this.userdetails.tenantid,
          'tenantname': this.userdetails.tenantname,
          'status': data.status
        }
      this.categoryservice.update(formdata)
        .then(res => {
          if (res.status == true) {
            this.msgs = [];
            // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: res.message });
            this.messageService.showSuccessMessage({ severity: 'success', summary: 'Category Updated Successfully ', detail: '' });
            this.ClearForm();
            this.loadlist(this.editovrlay);
          } else {
            this.msgs = [];
            this.messageService.showMessage({ severity: 'error', summary: 'Error ', detail: res.message });
            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
          }
        });
    }
    else {
      var saveformdata =
        {
          'categoryname': data.categoryname,
          'createdby': this.userdetails.tenantname,
          'createddt': new Date(),
          'lastupdatedby': this.userdetails.tenantname,
          'lastupdateddt': new Date(),
          'tenantid': this.userdetails.tenantid,
          'tenantname': this.userdetails.tenantname,
          'status': "Active"
        }

      this.categoryservice.create(saveformdata)
        .then(res => {
          if (res.status == true) {
            this.loadlist(this.addovrlay);
            this.msgs = [];
            this.messageService.showSuccessMessage({ severity: 'success', summary: 'Category Created Successfully ', detail: '' });
            // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: res.message });
            this.ClearForm();
          } else {
            this.msgs = [];
            this.messageService.showMessage({ severity: 'error', summary: 'Error ', detail: res.message });
            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
          }
        });
    }
  }

}
