import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PartiesService } from './../../services/parties.service';
import { CommonService } from './../../services/common.service';
import { LocalStorageService } from '../../../../shared/local-storage.service';
import { Message } from 'primeng/primeng';
import { AppConstant } from '../../../../../app/app.constant';
import { MasterService } from '../../../../services/master.service';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { OverlayPanel } from 'primeng/primeng';
import { UtilsService } from '../../../../services/utils.service';
import * as _ from "lodash";
import { CheckboxModule } from 'primeng/primeng';
import { MessagesService } from '../../../../shared/messages.service';
import { DateformatPipe } from '../../../../pipes/dateformat.pipe';

@Component({
  selector: 'app-addparties',
  templateUrl: './addparties.component.html',
  styleUrls: ['./addparties.component.scss']
})
export class AddpartiesComponent implements OnInit {
  country: SelectItem[];
  validation = "true";
  validationmsg = "";
  formdata = {};
  msgs: Message[] = [];
  par: FormGroup;
  Indate: any;
  userdetails: any;
  allcountry: any[];
  countrys: any[];
  billcities: any[];
  shipcities: any[];
  billstates: any[];
  shipstates: any[];
  cities: any[];
  states: any[];
  groups: any[];
  industries: any[];
  filteredcountry: any[];
  selectbillstate: any[];
  selectshipstate: any[];
  selectedcountrydetails: any;
  selectedcitydetails: any;
  selectedstatedetails: any;
  selectedshipcountrydetails: any;
  selectedshipcitydetails: any;
  selectedshipstatedetails: any;
  selectedindustry: any;
  selectedgroup: any;
  selectedcitybdetails: any;
  selectedstatebdetails: any;
  selectedcontacttype: any;
  selectedstatesdetails: any;
  checkstate: any;
  checkstates: any;
  changestatus: any[] = [];
  status: any = [];
  buttonText = "Save";
  @Input() partiesdetails: any;
  @Output() notifyNewProduct: EventEmitter<any> = new EventEmitter();

  constructor(private fb: FormBuilder, private partiesservice: PartiesService, private LocalStorageService: LocalStorageService,
    private commonservice: CommonService, private masterservice: MasterService, private messageService: MessagesService, private UtilsService: UtilsService,
  private dateFormatPipeFilter: DateformatPipe) {
    this.par = fb.group({
      'companyname': [null, Validators.required],
      'accheadname': [null, Validators.required],
      'firstname': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
      'mobileno': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(16)])],
      'emailid': [null],
      'billaddress': [null],
      'billcityid': [null],
      'billcityname': [null],
      'billstatename': [null],
      'billcountryname': [null],
      'billzipcode': [null],
      'shipaddress': [null],
      'shipcityid': [null],
      'shipcityname': [null],
      'shipstatename': [null],
      'shipcountryname': [null],
      'shipzipcode': [null],
      'industryname': [null],
      'industryid': [null],
      'gstno': [null],
      'tinno': [null],
      'cntctdeprtmnt': [null],
      'cntctdesignation': [null],
      'socialids': [null],
      'status': [null],
      'agreed': [false]
    })
    this.userdetails = LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.status = AppConstant.API_CONFIG.status;
    for (var i = 0; i < this.status.length; i++) {
      this.changestatus.push({
        label: this.status[i].stat, value: this.status[i].stat
      });
    }
  }
  numberOnly(event) {
    this.UtilsService.allowNumberOnly(event);
  }

  ngOnInit() {
    this.buttonText = "Save";
    this.groups = [];
    var self = this;
    this.commonservice.FindAllGroups({ subaccheadslist: ['SUNDRY CREDITORS', 'SUNDRY DEBTORS'] })
      .then((res) => {
        if (res.status) {
          var group = res.data;
          self.groups = self.masterservice.formatDataforDropdown("subaccheadname", group, "Select Group");
        }
      });
    this.loadindustry();
    this.loadcountry();
    setTimeout(() => {
      this.viewproduct()
    }, 1000);
  }
  checkbox() {

    if (this.par["controls"].agreed.value) {

      this.par.controls.shipaddress.setValue(this.par.getRawValue().billaddress);
      this.par.controls.shipzipcode.setValue(this.par.getRawValue().billzipcode);
      this.par.controls.shipcountryname.setValue(this.par.getRawValue().billcountryname);
      this.checkstate = this.par.getRawValue().billstatename;
      this.shipstates = this.billstates;
      this.par.controls.shipstatename.setValue(this.checkstate);
      this.checkstates = this.par.getRawValue().billcityname;
      this.shipcities = this.billcities;
      this.par.controls.shipcityname.setValue(this.checkstates);


    } else {
      console.log("value false");
    }
  }
  loadcountry() {
    this.countrys = [];
    var self = this;
    this.commonservice.FindAllCountry({})
      .then((res) => {
        if (res.status) {
          var counties = res.data;
          self.countrys = self.masterservice.formatDataforDropdown("countryname", counties, "Select Country");
        }
      });
  }
  loadstate(selectcountry, mode): Promise<any> {
    if(!_.isEmpty(selectcountry))
    {
      return this.commonservice.FindAllState({ countryname: selectcountry.value.countryname })
      .then((res) => {
        if (res.status) {
          if (mode == "B") {
            this.billstates = [];
            this.billstates = this.masterservice.formatDataforDropdown("statename", res.data, "Select State");
          }
          else if (mode == "S") {
            this.shipstates = [];
            this.shipstates = this.masterservice.formatDataforDropdown("statename", res.data, "Select State");
          }
        }
      });
    }

  }
  loadindustry() {
    this.industries = [];
    var self = this;
    this.commonservice.FindAllIndustryType({})
      .then((res) => {
        if (res.status) {
          var industry = res.data;
          self.industries = self.masterservice.formatDataforDropdown("industryname", industry, "Select Type");
        }
      });
  }

  loadcities(selectstate, mode): Promise<any> {
    return this.commonservice.FindAllCity({ stateid: selectstate.value.stateid })
      .then((res) => {
        if (res.status) {
          if (mode == "B") {
            this.billcities = [];
            this.billcities = this.masterservice.formatDataforDropdown("cityname", res.data, "Select City");
          }
          else if (mode == "S") {
            this.shipcities = [];
            this.shipcities = this.masterservice.formatDataforDropdown("cityname", res.data, "Select City");
          }
        }
      });

  }
  clearform() {
    this.par.reset();
    this.buttonText = "Save";
  }

  callParent(message) {
    this.notifyNewProduct.next(message);
  }
  viewproduct() {

    if (!_.isEmpty(this.partiesdetails)) {
      this.buttonText = "Update";
      this.partiesdetails.contactid = this.partiesdetails.contactid;
      var self = this;
      this.selectedgroup = _.find(self.groups, { value: { subaccheadid: self.partiesdetails.accheadid } });
      this.selectedcountrydetails = _.find(self.countrys, { value: { countryname: self.partiesdetails.billcountryname } });
      // this.selectedstatedetails = _.find(self.states, { value: { stateid: self.partiesdetails.billstateid } });
      this.selectedshipcountrydetails = _.find(self.countrys, { value: { countryname: self.partiesdetails.shipcountryname } });
      // this.selectedshipstatedetails = _.find(self.states, { value: { stateid: self.partiesdetails.shipstateid } });
      this.selectedindustry = _.find(self.industries, { value: { industryid: self.partiesdetails.industryid } });
      this.selectedcontacttype = this.partiesdetails.contactype;
      this.par = new FormGroup({
        'firstname': new FormControl(this.partiesdetails.firstname,Validators.required),
        'companyname': new FormControl(this.partiesdetails.companyname,Validators.required),
        'accheadname': new FormControl(this.selectedgroup.value,Validators.required),
        'emailid': new FormControl(this.partiesdetails.emailid),
        'mobileno': new FormControl(this.partiesdetails.mobileno,Validators.required),
        'billaddress': new FormControl(this.partiesdetails.billaddress),
        'billcityname': new FormControl(null),
        'billstatename': new FormControl(null),
        'billcountryname': new FormControl(_.isEmpty(this.selectedcountrydetails) ? null : this.selectedcountrydetails.value),
        'billzipcode': new FormControl(this.partiesdetails.billzipcode),
        'shipaddress': new FormControl(this.partiesdetails.shipaddress),
        'shipcityname': new FormControl(null),
        'shipstatename': new FormControl(null),
        'shipcountryname': new FormControl(_.isEmpty(this.selectedshipcountrydetails) ? null : this.selectedshipcountrydetails.value),
        'shipzipcode': new FormControl(this.partiesdetails.shipzipcode),
        'cntctdeprtmnt': new FormControl(this.partiesdetails.cntctdeprtmnt),
        'cntctdesignation': new FormControl(this.partiesdetails.cntctdesignation),
        'socialids': new FormControl(this.partiesdetails.socialids),
        'industryname': new FormControl(_.isEmpty(this.selectedindustry) ? null : this.selectedindustry.value),
        'gstno': new FormControl(this.partiesdetails.gstno),
        'tinno': new FormControl(this.partiesdetails.tinno),
        'status': new FormControl(this.partiesdetails.status),
      })
      this.loadstate(this.selectedcountrydetails, "B").then(() => {
        let selectedstatebdetails = _.find(this.billstates, { value: { stateid: this.partiesdetails.billstateid } });
        if (!_.isEmpty(selectedstatebdetails)) {
          this.selectedstatebdetails = selectedstatebdetails;
          this.par.controls["billstatename"].setValue(selectedstatebdetails.value);
          this.loadcities(this.selectedstatebdetails, "B").then(() => {
            let selectedcitybdetails = _.find(this.billcities, { value: { cityid: this.partiesdetails.billcityid } });
            if (!_.isEmpty(selectedcitybdetails)) {
              this.par.controls["billcityname"].setValue(selectedcitybdetails.value);
            }
          })
        }
      })
      this.loadstate(this.selectedshipcountrydetails, "S").then(() => {
        let selectedstatesdetails = _.find(this.shipstates, { value: { stateid: this.partiesdetails.shipstateid } });
        this.selectedstatesdetails = selectedstatesdetails;
        if (!_.isEmpty(selectedstatesdetails)) {

          this.par.controls["shipstatename"].setValue(selectedstatesdetails.value);
          this.loadcities(this.selectedstatesdetails, "S").then(() => {
            let selectedcitysdetails = _.find(this.shipcities, { value: { cityid: this.partiesdetails.shipcityid } });
            if (!_.isEmpty(selectedcitysdetails)) {
              this.par.controls["shipcityname"].setValue(selectedcitysdetails.value);
            }
          });
        }

      })

    }
  }

  formObj: any = {
    firstname: {
      required: "Please enter name",
      minlength: "Please enter name atleast 3 characters",
      maxlength: "Please enter name between 50 characters"
    },
    accheadname: {
      required: "Please select group"
    },
    companyname: {
      required: "Please enter company name"
    },
    mobileno: {
      required: "Please enter phone number",
      minlength: "Please enter phone number atleast 10 characters",
      maxlength: "Please enter phone number between 16 characters"
    },
    status: {
      required: "status Required"
    }
  }

  save(data) {
    // var docDate = this.dateFormatPipeFilter.transform(data.Docmentdata, 'y-MM-dd');
    var crdata = this.dateFormatPipeFilter.transform(new Date(), 'y-MM-dd');

    if (this.par.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.par, this.formObj);
      this.messageService.showMessage({ severity: 'error', summary: 'Error Message', detail: errorMessage });
      return false;
    }
    if (this.partiesdetails) {
      this.formdata = {
        'contactid': this.partiesdetails.contactid,
        'tenantid': this.userdetails.tenantid,
        'tenantname': this.userdetails.tenantname,
        'accheadid': data.accheadname.subaccheadid,
        'accheadname': data.accheadname.subaccheadname,
        'firstname': data.firstname,
        'lastname': "",
        'companyname': data.companyname,
        'emailid': data.emailid,
        'mobileno': data.mobileno,
        'billaddress': _.isEmpty(data.billaddress) ? null : data.billaddress,
        'billcityid': _.isEmpty(data.billcityname) ? null : data.billcityname.cityid,
        'billcityname': _.isEmpty(data.billcityname) ? null : data.billcityname.citynam,
        'billstateid': _.isEmpty(data.billstatename) ? null : data.billstatename.stateid,
        'billstatename': _.isEmpty(data.billstatename) ? null : data.billstatename.statename,
        'billcountryid': _.isEmpty(data.billcountryname) ? null : data.billcountryname.countryid,
        'billcountryname': _.isEmpty(data.billcountryname) ? null : data.billcountryname.countryname,
        'billzipcode': _.isEmpty(data.billzipcode) ? null : data.billzipcode,
        'shipaddress': _.isEmpty(data.shipaddress) ? null : data.shipaddress,
        'shipcityid': _.isEmpty(data.shipcityname) ? null : data.shipcityname.cityid,
        'shipcityname': _.isEmpty(data.shipcityname) ? null : data.shipcityname.cityname,
        'shipstateid': _.isEmpty(data.shipstatename) ? null : data.shipstatename.stateid,
        'shipstatename': _.isEmpty(data.shipstatename) ? null : data.shipstatename.statename,
        'shipcountryid': _.isEmpty(data.shipcountryname) ? null : data.shipcountryname.countryid,
        'shipcountryname': _.isEmpty(data.shipcountryname) ? null : data.shipcountryname.countryname,
        'shipzipcode': _.isEmpty(data.shipzipcode) ? null : data.shipzipcode,
        'cntctdeprtmnt': _.isEmpty(data.cntctdeprtmnt) ? null : data.cntctdeprtmnt,
        'cntctdesignation': _.isEmpty(data.cntctdesignation) ? null : data.cntctdesignation,
        'socialids': _.isEmpty(data.socialids) ? null : data.socialids,
        'TYPE': "Party",
        'industryid': _.isEmpty(data.industryname) ? null : data.industryname.industryid,
        'industryname': _.isEmpty(data.industryname) ? null : data.industryname.industryname,
        'status': data.status,
        'lastupdatedby': this.userdetails.loginname,
        'lastupdateddt': crdata,
        'gstno': _.isEmpty(data.gstno) ? null : data.gstno,
        'tinno': _.isEmpty(data.tinno) ? null : data.tinno,
        'ccyid': _.isEmpty(data.ccyid) ? null : data.ccyid,
        'ccyname': _.isEmpty(data.ccyname) ? null : data.ccyname,
        'paymenttermid': _.isEmpty(data.paymenttermid) ? null : data.paymenttermid,
        'paymentterms': _.isEmpty(data.paymentterms) ? null : data.paymentterms,
        "contactype": this.selectedcontacttype,
      }
      this.partiesservice.update(this.formdata)
        .then((res) => {
          if (res.status == true) {
            this.msgs = [];
            this.messageService.showSuccessMessage({ severity: 'success', summary: 'Parties Updated Successfully ', detail: '' });
            // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: res.message });
            this.callParent(res.message);
            this.clearform();
          }
          else if (res.status == false) {
            this.clearform();
            this.msgs = [];
            this.messageService.showMessage({ severity: 'error', summary: 'Error ', detail: res.message });
            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
          }
        });
    }
    else {
      this.formdata = {
        'create': "Y",
        'tenantid': this.userdetails.tenantid,
        'tenantname': this.userdetails.tenantname,
        'accheadid': data.accheadname.subaccheadid,
        'accheadname': data.accheadname.subaccheadname,
        'firstname': data.firstname,
        'lastname': "",
        'companyname': data.companyname,
        'emailid': data.emailid,
        'mobileno': data.mobileno,
        'billaddress': _.isEmpty(data.billaddress) ? null : data.billaddress,
        'billcityid': _.isEmpty(data.billcityname) ? null : data.billcityname.cityid,
        'billcityname': _.isEmpty(data.billcityname) ? null : data.billcityname.citynam,
        'billstateid': _.isEmpty(data.billstatename) ? null : data.billstatename.stateid,
        'billstatename': _.isEmpty(data.billstatename) ? null : data.billstatename.statename,
        'billcountryid': _.isEmpty(data.billcountryname) ? null : data.billcountryname.countryid,
        'billcountryname': _.isEmpty(data.billcountryname) ? null : data.billcountryname.countryname,
        'billzipcode': _.isEmpty(data.billzipcode) ? null : data.billzipcode,
        'shipaddress': _.isEmpty(data.shipaddress) ? null : data.shipaddress,
        'shipcityid': _.isEmpty(data.shipcityname) ? null : data.shipcityname.cityid,
        'shipcityname': _.isEmpty(data.shipcityname) ? null : data.shipcityname.cityname,
        'shipstateid': _.isEmpty(data.shipstatename) ? null : data.shipstatename.stateid,
        'shipstatename': _.isEmpty(data.shipstatename) ? null : data.shipstatename.statename,
        'shipcountryid': _.isEmpty(data.shipcountryname) ? null : data.shipcountryname.countryid,
        'shipcountryname': _.isEmpty(data.shipcountryname) ? null : data.shipcountryname.countryname,
        'shipzipcode': _.isEmpty(data.shipzipcode) ? null : data.shipzipcode,
        'cntctdeprtmnt': _.isEmpty(data.cntctdeprtmnt) ? null : data.cntctdeprtmnt,
        'cntctdesignation': _.isEmpty(data.cntctdesignation) ? null : data.cntctdesignation,
        'socialids': _.isEmpty(data.socialids) ? null : data.socialids,
        'TYPE': "Party",
        'industryid': _.isEmpty(data.industryname) ? null : data.industryname.industryid,
        'industryname': _.isEmpty(data.industryname) ? null : data.industryname.industryname,
        'status': "Active",
        'createdby': this.userdetails.loginname,
        'createddt': crdata,
        'gstno': _.isEmpty(data.gstno) ? null : data.gstno,
        'tinno': _.isEmpty(data.tinno) ? null : data.tinno,
        'ccyid': _.isEmpty(data.ccyid) ? null : data.ccyid,
        'ccyname': _.isEmpty(data.ccyname) ? null : data.ccyname,
        'paymenttermid': _.isEmpty(data.paymenttermid) ? null : data.paymenttermid,
        'paymentterms': _.isEmpty(data.paymentterms) ? null : data.paymentterms,
        "contactype": this.selectedcontacttype,
      }
      this.partiesservice.create(this.formdata)
        .then((res) => {
          if (res.status == true) {
            this.msgs = [];
            this.messageService.showSuccessMessage({ severity: 'success', summary: 'Parties Created Successfully ', detail: '' });
            // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: res.message });
            this.callParent(res.message);
            this.clearform();
          }
          else if (res.status == false) {
            // this.clearform();
            this.msgs = [];
            this.messageService.showMessage({ severity: 'error', summary: 'Error ', detail: res.message });
            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
          }
        });
    }

  }
  setContactType(event) {
    if (event.value.subaccheadname === "SUNDRY DEBTORS") {
      this.selectedcontacttype = "Customer";
    }
    else if (event.value.subaccheadname === "SUNDRY CREDITORS") {
      this.selectedcontacttype = "Vendor";
    }
    // this.setContactType = 
  }
}
