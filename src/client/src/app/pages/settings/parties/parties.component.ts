import { Component, OnInit, Renderer, ElementRef, Input, Output, ViewChild } from '@angular/core';
import { PartiesService } from '../services/parties.service';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { AppConstant } from '../../../app.constant';
import { LocalStorageService } from '../../../../app/shared/local-storage.service';
import { AddpartiesComponent } from '../parties/addparties/addparties.component'
@Component({
  selector: 'app-parties',
  templateUrl: './parties.component.html',
  styleUrls: ['./parties.component.scss']
})
export class PartiesComponent implements OnInit {
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  partieslist: any[] = [];
  partiesdetails: any = {};
  editpar: any;
  addparties: boolean = false;
  editparties: boolean = false;
  addnew: boolean = false;
  list: boolean = true;
  show: boolean = false;
  name: string;
  datafromat:string;
  constructor(private partiesservice: PartiesService, private localStorageService: LocalStorageService) {
    this.datafromat = AppConstant.API_CONFIG.DATE.displayFormat;
   }
  @ViewChild('wrapper') wrapper: ElementRef;
  ngOnInit() {
    this.getallparties();

  }
  getallparties() {
    var partiesdetails: any = this.localStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.partiesservice.getAll({ tenantid: partiesdetails.tenantid })
      .then(res => {
        if (res.status) {
          this.partieslist = res.data;
          console.log(" this.partieslist", this.partieslist)
        }
        else {
          console.log("no parties");
        }
      });
  }

  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }
  notifyNewProduct(event) {
    this.getallparties();
    this.addparties = false;
    this.editparties = false;
    this.addnew = false;
    this.editpar = false;
    this.list = true;
  }
  Addproducts(partiesView) {
    this.addparties = true;
    this.editparties = false;
    this.addnew = true;
    this.editpar = false;
    this.list = false;
    this.partiesdetails = null;
    partiesView.activeIndex=1;
    // this.wrapper.nativeElement.click()
  }

  handleClose(e) {
    this.addparties = false;
    this.editparties = false;
    this.list = true;
  }
  viewproduct(partiesdetails,partiesView) {
    this.list = false;
    this.addparties = false;
    this.editparties = true;
    this.addnew = false;
    this.editpar = true;
    this.partiesdetails = partiesdetails;
    partiesView.activeIndex=1;
  }
}
