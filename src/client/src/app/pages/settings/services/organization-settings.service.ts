import { Injectable } from '@angular/core';
import { AppConstant } from '../../../app.constant';
import { CommonHttpService } from "../../../shared/common-http.service";

@Injectable()
export class OrganizationSettingsService {

 constructor(private httpService: CommonHttpService) { }
  mainUrl = AppConstant.API_ENDPOINT;
 public FindAll(data: any):Promise<any> {
   return this.httpService.globalGetServiceByUrl(this.mainUrl + AppConstant.API_CONFIG.API_URL.TNT.FINDBYID,data)
      .then(data => {
        return data;
      });
  }
  public UpdateOrgSetting(data: any):Promise<any> {
   return this.httpService.globalPostService(this.mainUrl + AppConstant.API_CONFIG.API_URL.TNT.TENANTUPDATE,data)
      .then(data => {
        return data;
      });
  }
}
