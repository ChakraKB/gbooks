import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { BrandService } from '../services/brand.service';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { AppConstant } from '../../../app.constant';
import { LocalStorageService } from '../../../../app/shared/local-storage.service';
import { OverlayPanel } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit {
  public currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  public display_dtime = AppConstant.API_CONFIG.ANG_DATE.displaydtime;
  private date_dformat = AppConstant.API_CONFIG.ANG_DATE.displayFormat;
  private date_apiformat = AppConstant.API_CONFIG.ANG_DATE.apiFormat;

  @Output() updatebrand = new EventEmitter();
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  display: boolean = false;
  brandlist: any[] = [];
  branddetails: any = {};
  branddtls: any[];
  editovrlay: any;
  addovrlay: any;
  show: boolean = false;
  constructor(private brandservice: BrandService, private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.loadbrand();
  }
  loadbrand() {
    var branddetails: any = this.localStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.brandservice.getAll({ tenantid: branddetails.tenantid })
      .then(res => {
        if (res.status) {
          this.brandlist = res.data;
        }
        else {
          console.log("no brand");
        }
      });
  }
  loadbrandlist() {
    this.display = false;
    this.loadbrand();
  }
  addbrand(event, overlaypanel: DialogModule) {
    this.addovrlay = overlaypanel;
    // overlaypanel.toggle(event);
    this.display = true;
    // this.branddtls = null;
    this.branddtls = [];
  }

  update(event, branddtls, overlaypanel: DialogModule) {
    this.branddtls = branddtls
    this.display = true;
    this.editovrlay = overlaypanel;
    // overlaypanel.toggle(event);
  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }
}
