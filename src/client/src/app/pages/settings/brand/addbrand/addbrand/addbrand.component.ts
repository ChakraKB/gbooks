import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LocalStorageService } from '../../../../../shared/local-storage.service';
import { AppConstant } from '../../../../../../app/app.constant';
import { BrandService } from '../../../services/brand.service';
import { BrandComponent } from '../../brand.component';
import * as _ from "lodash";
import { Message } from 'primeng/primeng';
import { MasterService } from '../../../../../services/master.service';
import { MessagesService } from '../../../../../shared/messages.service';
import { CommonService } from '../../../services/common.service';
import { SelectItem, MenuItem } from 'primeng/primeng';
@Component({
  selector: 'app-addbrand',
  templateUrl: './addbrand.component.html',
  styleUrls: ['./addbrand.component.scss']
})
export class AddbrandComponent implements OnInit {
  @Input() editovrlay: any;
  @Input() addovrlay: any;
  @Input() branddtls: any;
  @Output() loadbrandlist: EventEmitter<any> = new EventEmitter();
  brandform: FormGroup;
  brandname: FormControl;
  branddetails: any = {};
  userdetails: any;
  validation = "true";
  validationmsg = "";
  msgs: Message[] = [];
  buttonText = "Save";
  changestatus: any[] = [];
  status: any = [];
  constructor(private commonservice: CommonService, private messageService: MessagesService, private fb: FormBuilder, private localstorageservice: LocalStorageService, private brandservice: BrandService, private masterservice: MasterService) {
    this.userdetails = localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.brandform = fb.group({
      'brandname': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15)])],
      'status': [null, Validators.required]
    })
    this.status = AppConstant.API_CONFIG.status;
    for (var i = 0; i < this.status.length; i++) {
      this.changestatus.push({
        label: this.status[i].stat, value: this.status[i].stat
      });
    }
  }
  ngOnInit() {
    // this.CreateFormControls();
    // this.CreateForm();
    // this.status();
    this.msgs = [];
  }
  ngOnChanges() {
    this.vieweditbrand();
  }
  loadlist(ovrlay) {
    this.loadbrandlist.next(ovrlay)
  }
  // status(){
  //   this.commonservice.status({}).then((res) => {
  // console.log("status res",res)
  //   });


  // }
  // CreateFormControls() {
  //   this.brandname = new FormControl('', Validators.required)
  // }
  // CreateForm() {
  //   this.brandform = new FormGroup({
  //     brandname: this.brandname
  //   });
  // }
  vieweditbrand() {
    this.ClearForm();
    if (!_.isEmpty(this.branddtls)) {
      this.brandform = new FormGroup({
        'brandname': new FormControl(this.branddtls.brandname,Validators.required),
        'status': new FormControl(this.branddtls.status,Validators.required)
      })
    }
    else {
      this.ClearForm();
    }
  }
  ClearForm() {
    this.brandform = new FormGroup({
      'brandname': new FormControl(null,Validators.required)
    });

    // if(!this.branddtls)
    //     this.brandform.addControl('status',new FormControl());

    this.msgs = [];
  }
  formObj: any = {
    brandname: {
      required: "Please enter Brand Name",
    },
    status: {
      required: "status Required"
    }
  }
  save(data) {
    if (this.brandform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.brandform, this.formObj);
      this.messageService.showMessage({ severity: 'error', summary: 'Error Message', detail: errorMessage });
      return false;
    }

    if (!_.isEmpty(this.branddtls)) {
      var formdata =
        {
          'brandid': this.branddtls.brandid,
          'brandname': data.brandname,
          'lastupdatedby': this.userdetails.tenantname,
          'lastupdateddt': new Date(),
          'tenantid': this.userdetails.tenantid,
          'tenantname': this.userdetails.tenantname,
          'status': data.status,
        }
      this.brandservice.update(formdata).then(res => {
        if (res.status == true) {
          this.ClearForm();
          this.messageService.showSuccessMessage({ severity: 'success', summary: 'Brand Updated Successfully ', detail: '' });
          this.loadlist(this.editovrlay);
        } else {
          this.messageService.showMessage({ severity: 'error', summary: 'Error ', detail: res.message });
        }
      });
    }
    else {

      var saveformdata =
        {
          'brandname': data.brandname,
          'createdby': this.userdetails.tenantname,
          'createddt': new Date(),
          'tenantid': this.userdetails.tenantid,
          'tenantname': this.userdetails.tenantname,
          'lastupdatedby': this.userdetails.tenantname,
          'lastupdateddt': new Date(),
          // 'status': 'Active'
        }

      this.brandservice.create(saveformdata)
        .then(res => {
          if (res.status == true) {
            this.loadlist(this.addovrlay);
            this.msgs = [];
            this.messageService.showSuccessMessage({ severity: 'success', summary: 'Brand Created Successfully ', detail: '' });
            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: res.message });
            this.ClearForm();
          } else {
            this.msgs = [];
            this.messageService.showMessage({ severity: 'error', summary: 'Error ', detail: res.message });
            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
          }
        });
    }
  }
}
