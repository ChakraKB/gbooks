import { Component, OnInit } from '@angular/core';
import { AccountsService } from'./../service/accounts.service';
import { AppConstant } from './../../app.constant';
import {LocalStorageService} from '../../shared/local-storage.service';
import { UtilsService } from '../../services/utils.service';
import * as _ from "lodash";

@Component({
  selector: 'app-debit-notelist',
  templateUrl: './debit-notelist.component.html',
  styleUrls: ['./debit-notelist.component.scss']
})
export class DebitNotelistComponent implements OnInit {
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  userdetails: any;
  alldebit: any;
  addproduct: boolean = false;
  addnew:boolean=false;
  list : boolean = true;
  finyear:any;
  dataFormat:string;
  currency_Symbol:string;
  activeTab_edit:boolean=false;
  showEdit:boolean=false;
  debitidlist:any;
  selectedviewdebit:Array<any>=[];
  activetabindex:number=0;
  show:boolean=false
  constructor(private AccountsService: AccountsService, private LocalStorageService: LocalStorageService,private UtilsService:UtilsService ) {
    this.userdetails = LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.dataFormat=AppConstant.API_CONFIG.DATE.displayFormat;
    this.currency_Symbol=AppConstant.API_CONFIG.CURRENCY_FORMAT
  }
  ngOnInit() {
  this.crdrgetall();
}
showhidefilter() {
  if (this.show == true) {
    this.show = false
  }
  else {
    this.show = true;
  }
}
crdrgetall(){
  const data={
    "offset": 0,
    "limit": 10,
    "startdate": "",
    "enddate": "",
    "startCreatedDt":"",
    "endCreatedDt":"",
    "gtAmount":"",
    "ltAmount":"",
    "amountKey":"",
    "query": {
    "tenantid":this.userdetails.tenantid,
    "type":"DRNOTE"
  }
}
var callback=this;
this.AccountsService.FindAllAccounts(data).then(function(res){
  callback.alldebit=res.data;
  
})
}
notifyNewDebitNote(even){
  this.crdrgetall();
  this.addproduct = false;
  this.addnew=false;
  this.list = true;
  this. activeTab_edit=false
  this.showEdit=false;
}
Addproducts(debitview) {
  this.addproduct = true;
  this.addnew=true;
  this.list = false;
  this. activeTab_edit=false
  this.showEdit=false;
  this.selectedviewdebit=[];
  debitview.activeIndex=1
  this.UtilsService.active_addAndeditTab(debitview,"Add Debit Note");
}
handleClose(event,closetab) {
this.addproduct = false;
this.showEdit=false;
this.list = true;
this.UtilsService.deactivate_multitab(this.selectedviewdebit,event,closetab,"transno");
}
editDeposit(debitidlist,editDeposit){
  this.addproduct = false;
  this.addnew=false;
  this.list = false;
 this. activeTab_edit=true;
 this.showEdit=true;
 this.selectedviewdebit=[];
 this.debitidlist=debitidlist;
 editDeposit.activeIndex=1
// this.UtilsService.active_addAndeditTab(editDeposit,"Edit Debit Note")
}
addTabviewdebit(item,addTabviewdebit){
  this.addproduct = false;
  this.addnew=false;
  this.list = false;
 this. activeTab_edit=false;
 this.showEdit=false;
  this.UtilsService.activate_multitab(this.selectedviewdebit,item,addTabviewdebit,"transno");
}
}

