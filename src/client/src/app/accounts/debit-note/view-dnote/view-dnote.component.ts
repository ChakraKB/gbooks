import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { LocalStorageService } from '../../../shared/local-storage.service';
import * as _ from "lodash";
import { MasterService } from '../../../services/master.service';
import { AccountsService } from '../../service/accounts.service';
import { PurchasesService } from '../../../services/purchases/purchases.service';
import {AppConstant}from '../../../app.constant'
@Component({
  selector: 'app-view-dnote',
  templateUrl: './view-dnote.component.html',
  styleUrls: ['./view-dnote.component.scss']
})
export class ViewDnoteComponent implements OnInit {
  @Input() crdrid: any;
  transno:any;
  billList: Array<any> = [];
  debitData:any=[];
  userstoragedata: any = {};
  finyear: any = {};
  resources: any = {};
  reqdata: any = {};
  reqinvoicedata: any = {};
  billData:any;
  billlineitems:any = [];
  credit_feature: string = "Debit Note";
  dataFormat:string;
  currency_Symbol:string;
  constructor(private masterservice: MasterService, private storageservice: LocalStorageService,
    private route: ActivatedRoute, private accountservice: AccountsService, private billservice: PurchasesService) {
    this.route.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.crdrid = params.crdrid;
      }
    });
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.resources = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.RESOURCE);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.dataFormat=AppConstant.API_CONFIG.DATE.displayFormat;
    this.currency_Symbol=AppConstant.API_CONFIG.CURRENCY_FORMAT;
  }

  ngOnInit() {
      this.reqdata = {
      "crdrid": this.crdrid
    };
    if (this.crdrid && this.crdrid != undefined) {
      this.loaddebitDetails(this.reqdata);
    }
  }
  loaddebitDetails(reqdata){
  this.debitData = [];
    var reqdata: any = {
      "crdrid": this.crdrid,
      "feature": "DRNOTE"
    };
    this.accountservice.FindByIDAccounts(reqdata)
      .then((res) => {
        if (res.status) {
             this.debitData = res.data[0];
             this.transno =   this.debitData.transno;
            this.loadbilldetails().then(res => {
            this.billlineitems = _.find(this.billList, { billno: this.debitData.crdrrefno });      
               })
}
      });
     
  }
   loadbilldetails(): Promise<any> {
    this.billData = [];
    var reqinvoicedata: any = {
      "tenantid": this.userstoragedata.tenantid,
      "finyear": this.finyear.finyear,
      "feature": "bill",
      "status": this.userstoragedata.status
    };
    return this.billservice.getBillList(reqinvoicedata).then((res) => {
      this.billList = res.data;
    });
  }

}
