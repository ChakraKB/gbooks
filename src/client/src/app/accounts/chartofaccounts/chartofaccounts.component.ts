import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import * as _ from "lodash";
import { FeaturesService } from '../../services/features.service';
import { MasterService } from '../../services/master.service';
import { TreeTableModule, TreeNode } from 'primeng/primeng';
import { Router, NavigationExtras } from '@angular/router';
import { MessagesService } from '../../shared/messages.service';
import { UtilsService } from '../../services/utils.service';
@Component({
  selector: 'app-chartofaccounts',
  templateUrl: './chartofaccounts.component.html',
  styleUrls: ['./chartofaccounts.component.scss']
})
export class ChartofaccountsComponent implements OnInit {
  selectedAccount: TreeNode;
  userstoragedata: any;
  finyear: any;
  allheads: any;
  allbookaccs: any;
  Assetsvalues: TreeNode[];
  Liabilitisvalues:TreeNode[];
  Expensesvalues:TreeNode[];
  Incomevalues:TreeNode[];
  constructor(private masterservice: MasterService, private storageservice: LocalStorageService,
    private router: Router, private messageservice: MessagesService, private utilservice: UtilsService, private featureservice: FeaturesService, ) {
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
  }
  ngOnInit() {
    this.getHeadsList();
  }
  getHeadsList() {
    var self = this;
    var data = {
    };
    self.allheads = [];
    this.featureservice.AccHeadList(data)
      .then(function (res) {
        if (res.status) {
          self.allheads = res.data;
          self.loadBookofAcc();
        }
      });
  }
  loadBookofAcc() {
    var self = this;
    var data = {
      tenantid: [this.userstoragedata.tenantid, 0],
    };
    self.allbookaccs = [];
    this.masterservice.BookGetAll(data)
      .then(function (res) {
        self.allbookaccs = res.data;
        var assetheads = _.filter(self.allheads,function(h:any){
          return (h.ALIE == "A")
        });
        var treedata: any = self.utilservice.chartofaccounts_treeformater(assetheads, self.allbookaccs);
        self.Assetsvalues = treedata.data;
        console.log("assets tree",self.Assetsvalues);
        var liabilitiesheads = _.filter(self.allheads,function(h:any){
          return (h.ALIE == "L")
        });
        var treelbitdata: any = self.utilservice.chartofaccounts_treeformater(liabilitiesheads, self.allbookaccs);
        self.Liabilitisvalues = treelbitdata.data;
        console.log("liabs tree",self.Liabilitisvalues);
        var expensesheads = _.filter(self.allheads,function(h:any){
          return (h.ALIE == "E")
        });
        var treedataexpenses: any = self.utilservice.chartofaccounts_treeformater(expensesheads, self.allbookaccs);
        self.Expensesvalues = treedataexpenses.data;
        console.log("expenses tree",self.Expensesvalues);
        var incomeheads = _.filter(self.allheads,function(h:any){
          return (h.ALIE == "I")
        });
        var treedataincome: any = self.utilservice.chartofaccounts_treeformater(incomeheads, self.allbookaccs);
        self.Incomevalues = treedataincome.data;
        console.log("income tree",self.Incomevalues);
      });
     
  }
  viewNode(node: TreeNode) {
    // this.msgs = [];
    // this.msgs.push({severity: 'info', summary: 'Node Selected', detail: node.data.name});
  }
  collapseNode(node: TreeNode) {
    // this.msgs = [];
    // this.msgs.push({severity: 'info', summary: 'Node Selected', detail: node.data.name});
  }
  expandNode(node: TreeNode) {
    // node.parent.children = node.parent.children.filter( n => n.data !== node.data);
    // this.msgs = [];
    // this.msgs.push({severity: 'info', summary: 'Node Deleted', detail: node.data.name});
  }
  expandNodeAll(event) {
  }

}
