import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { LocalStorageService } from '../../../shared/local-storage.service';
import * as _ from "lodash";
import { MasterService } from '../../../services/master.service';
import { AccountsService } from '../../service/accounts.service';
import { JournalsService } from '../../service/journals.service'
import { AppConstant } from '../../../app.constant'
import { Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-journals',
  templateUrl: './view-journals.component.html',
  styleUrls: ['./view-journals.component.scss']
})
export class ViewJournalsComponent implements OnInit {
  @Input() journalid: any;
  @Input() journalno: any;
  journaldatalist: any;
  userstoragedata: any = {};
  resources: any = {};
  reqdata: any = {};
  finyear: any = {};
  journalData: any;
  journaldatas: Array<any> = [];
  journaltaxs: Array<any> = [];
  journal_feature: string = "Journal";
  journalList: any;
  dataFormat: string;
  currency_Symbol: string;
  journalTotal: any;
  flag: string;
  constructor(private masterservice: MasterService, private storageservice: LocalStorageService,private location: Location,
    private route: ActivatedRoute, private journalservice: JournalsService, private router: Router ) {
    this.route.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.journalid = params.journalid;
        this.journalno = params.journalno;
        this.flag = params.journal;
      }
    });
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.resources = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.RESOURCE);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.dataFormat = AppConstant.API_CONFIG.DATE.displayFormat;
    this.currency_Symbol = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  }

  ngOnInit() {
    this.reqdata = {
      "journalid": this.journalid
    };
    if (this.journalid && this.journalid != undefined) {
      this.loadjournalDetails();
    }
  }

  redirecttoprev() {
    if (this.flag == "fromledger") {
      this.router.navigate(['/reports/trailbalance']);
    }
    else if (this.flag == "journal") {
      this.router.navigate(['/accounts/journals/list']);
    }
    this.location.back();
  }
  loadjournalDetails() {
    this.journalData = [];
    this.journalservice.getJournalDetails(this.journalid)
      .then((res) => {

        if (res.status) {
          this.journalData = res.data[0];
          this.journaldatas = res.data[0].ledgers;
          this.journalTotal = this.journalData.journaltotal;
        }
      });

  }

}
