import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray, ValidatorFn, AbstractControl } from '@angular/forms';
import { JournalsService } from '../../service/journals.service';
import { FeaturesService } from '../../../services/features.service';
import { MasterService } from '../../../services/master.service';
import { LocalStorageService } from '../../../shared/local-storage.service';
import * as moment from 'moment';
import * as _ from "lodash";
import { AppConstant } from '../../../app.constant';


@Component({
  selector: 'app-list-journal',
  templateUrl: './list-journal.component.html',
  styleUrls: ['./list-journal.component.scss']
})
export class ListJournalComponent implements OnInit {
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  tenantInfo: any;
  finyear: any;
  apiDateFormat: string;
  journalfeat: any;
  journalType: any;
  alljournals: any[] = [];
  dispDateFormat: string;
  show : boolean = false;

  constructor(private fb: FormBuilder,
    private journalService: JournalsService,
    private featureService: FeaturesService,
    private masterService: MasterService,
    private localStorageServcie: LocalStorageService) {
    this.tenantInfo = localStorageServcie.getItem("user");
    this.finyear = localStorageServcie.getItem("finyear");
    this.apiDateFormat = AppConstant.API_CONFIG.DATE.apiFormat;
    this.dispDateFormat = AppConstant.API_CONFIG.DATE.displayFormat;

  }

  ngOnInit() {
    this.getAllJournals();
    this.getAllCodeMasterFeatures();
  }

  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }

  getAllJournals(type?) {
    let queryParams: any = {};
    queryParams.tenantid = this.tenantInfo.tenantid;
    queryParams.finyear = this.finyear.finyear;
    //  queryParams.limit = 10;
    queryParams.type = type ? type : "General";

    this.journalService.getAllJournals(queryParams).then(response => {
      if (response.status) {
        this.alljournals = response.data;
      }
      else
      {
        this.alljournals = [];
      }
    })
  }
  getAllCodeMasterFeatures() {
    let params: any = {};
    params.type = "JOURNAL";

    this.featureService.getcodemasterList(params).then(res => {
      if (res.status) {
        this.journalfeat = this.masterService.formatDataforDropdown("name", res.data);
        this.journalType = this.journalfeat[0].value;
      }
    });
  };

  filterJournals() {
    this.getAllJournals(this.journalType.name);
  }
}
