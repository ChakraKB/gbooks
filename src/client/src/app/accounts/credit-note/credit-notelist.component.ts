import { Component, OnInit } from '@angular/core';
import { AccountsService } from './../service/accounts.service';
import { AppConstant } from './../../app.constant';
import { LocalStorageService } from '../../shared/local-storage.service';
import { UtilsService } from '../../services/utils.service'
import * as _ from "lodash";

@Component({
  selector: 'app-credit-notelist',
  templateUrl: './credit-notelist.component.html',
  styleUrls: ['./credit-notelist.component.scss']
})
export class CreditNotelistComponent implements OnInit {
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  userdetails: any
  allcredit: any[] = [];
  addproduct: boolean = false;
  activeTab_addCredit: boolean = false;
  list: boolean = true;
  finyear: any;
  dataFormat: string;
  currency_Symbol: string;
  activeTab_editcredit: boolean = false;
  showEdit: boolean = false;
  creditls: any;
  indextabview: number = 0;
  activeTab_viewcredit: boolean = false;
  selectedcredit: Array<any> = [];
  view: boolean = false;
  show:boolean=false;
  constructor(private AccountsService: AccountsService, private LocalStorageService: LocalStorageService,
    private UtilsService: UtilsService) {
    this.userdetails = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.dataFormat = AppConstant.API_CONFIG.DATE.displayFormat;
    this.currency_Symbol = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  }


  ngOnInit() {
    this.crdrgetall();
  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }

  crdrgetall() {
    const data = {
      "offset": 0,
      "limit": 10,
      "startdate": "",
      "enddate": "",
      "startCreatedDt":"",
      "endCreatedDt":"",
      "gtAmount":"",
      "ltAmount":"",
      "amountKey":"",
      "query": {
      "tenantid":this.userdetails.tenantid,
      "type":"CRNOTE"
    }
    
    }
    this.AccountsService.FindAllAccounts(data).then((res) => {
      this.allcredit = res.data;

    })
  }
  notifyNewCreditNote(even) {
    this.crdrgetall();
    this.addproduct = false;
    this.list = true;
    this.showEdit = false;
    this.activeTab_addCredit = false;
    this.activeTab_editcredit = false;
    this.activeTab_viewcredit = false;

  }
  Addproducts(addTab) {
    this.addproduct = true;
    this.list = false;
    this.showEdit = false;
    this.activeTab_addCredit = true;
    this.activeTab_editcredit = false;
    this.activeTab_viewcredit = false;
    this.selectedcredit=[];
    addTab.activeIndex=1;
  }
  handleClose(event, addTab,) {
    this.showEdit = false;
    this.addproduct = false;
    this.showEdit = false;
    this.list = true;
  this.UtilsService.deactivate_multitab(this.selectedcredit, event, addTab, "transno");
  }
  EditClickcredit(creditls, addTab) {
    this.creditls = creditls;
    this.addproduct = false;
    this.list = false;
    this.showEdit = true;
    this.activeTab_addCredit = false;
    this.activeTab_editcredit = true;
    this.activeTab_viewcredit = false;
    this.selectedcredit=[];
   addTab.activeIndex=1;
   
   
  }
  addTabviewcreditnote(item, addTab) {
  this.addproduct = false;
  this.activeTab_addCredit = false;
  this.list = false;
  this.activeTab_editcredit = false;
  this.showEdit = false;
  this.UtilsService.activate_multitab(this.selectedcredit, item, addTab, "transno");
 
  }
}
