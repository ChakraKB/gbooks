import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { DatePipe } from '@angular/common';
import { LocalStorageService } from '../../../shared/local-storage.service';
import * as _ from "lodash";
import { MasterService } from '../../../services/master.service';
import { AccountsService } from '../../service/accounts.service';
import { SalesService } from '../../../services/sales/sales.service';
import { AppConstant } from '../../../app.constant'
@Component({
  selector: 'app-view-cnote',
  templateUrl: './view-cnote.component.html',
  styleUrls: ['./view-cnote.component.scss']
})
export class ViewCnoteComponent implements OnInit {
  @Input() crdrid: any;
  invoiceList: Array<any> = [];
  creditDeta: any = {};
  transno: any[];
  invoiceDeta: any;
  userstoragedata: any = {};
  finyear: any = {};
  resources: any = {};
  reqdata: any = {};
  invoicelistdetails: any = {};
  reqinvoicedata: any = {};
  invoicelineitems: any = [];
  credit_feature: string = "Credit Note";
  dataFormat: string;
  currency_Symbol: string;
  constructor(private masterservice: MasterService, private storageservice: LocalStorageService,
    private accountservice: AccountsService, private salesservice: SalesService) {
    // this.route.params.subscribe(params => {
    //   if (!_.isEmpty(params)) {
    //     this.crdrid = params.crdrid;
    //     console.log("url params", this.crdrid);
    //   }
    // });
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.resources = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.RESOURCE);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.dataFormat = AppConstant.API_CONFIG.DATE.displayFormat;
    this.currency_Symbol = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  }

  ngOnInit() {
    //   this.reqdata = {
    //     "crdrid": this.crdrid
    //   };
    //   console.log("credit details", this.crdrid);
    //   if (this.crdrid && this.crdrid != undefined) {
    //     this.loadcreditDetails(this.reqdata);
    //   }
    this.loadcreditDetails();
  }
  loadcreditDetails() {
    this.creditDeta = [];
    var reqdata: any = {
      "crdrid": this.crdrid,
      "feature": "CRNOTE"
    };
    this.accountservice.FindByIDAccounts(reqdata)
      .then((res) => {
        if (res.status) {
          this.creditDeta = res.data[0];
          this.transno = this.creditDeta.transno;
          this.loadinvoicedetails().then(res => {
            this.invoicelineitems = _.find(this.invoiceList, { invoiceno: this.creditDeta.crdrrefno });
          })
        }
      });
  }
  loadinvoicedetails(): Promise<any> {
    this.invoiceDeta = [];
    var reqinvoicedata: any = {
      "tenantid": this.userstoragedata.tenantid,
      "finyear": this.finyear.finyear,
      "feature": "invoice",
      "status": this.userstoragedata.status
    };
    return this.salesservice.getInvoiceList(reqinvoicedata).then((res) => {
      this.invoiceList = res.data;
    });
  }
}
