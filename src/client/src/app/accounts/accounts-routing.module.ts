import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { JournalsComponent } from './journals/journals.component';
import { ChartofaccountsComponent } from './chartofaccounts/chartofaccounts.component';
import { CreditNoteComponent } from './credit-note/credit-note.component';
import { CreditNotelistComponent } from './credit-note/credit-notelist.component';
import {ListJournalComponent} from './journals/list-journal/list-journal.component';
import { DebitNoteComponent } from './debit-note/debit-note.component';
import { DebitNotelistComponent } from './debit-note/debit-notelist.component';
import {ViewCnoteComponent} from './credit-note/view-cnote/view-cnote.component';
import {ViewDnoteComponent}from './debit-note/view-dnote/view-dnote.component';
import {ViewJournalsComponent} from './journals/view-journals/view-journals.component';
const routes: Routes = [
  {
    path: 'chartofaccounts',
    component: ChartofaccountsComponent,
    data: {
      title: 'Chart of Accounts'
    }
  },
  {
    path: 'journals',
    data: {
      title: 'Journals'
    },
    children:[ 
    {	 
      path: '',	 
      component: JournalsComponent,	 
    },
    {
       path: 'list',
       component: ListJournalComponent,
	  },
    {
       path: ':journalid/:journalno',
       component: JournalsComponent,
	  } 
   ]
  },
  {
    path: 'creditnote',
    data: {
      title: 'Credit Note'
    },
 
     children:[
      {
        path:'',
        component: CreditNotelistComponent,
      },
      {
        path:'addnote',
        component: CreditNoteComponent,
      }
    ]
  },
  {
    path: 'debitnote',
    data: {
      title: 'Debit Note'
    },
      children:[
      {
        path:'',
        component: DebitNotelistComponent,
      },
      {
        path:'adddebitnote',
        component: DebitNoteComponent,
      }
    ]
  },
  //  {
  //   path: 'viewcreditnote/:crdrid',
  //   component: ViewCnoteComponent
  // },
  //  {
  //   path: 'viewdebitnote/:crdrid',
  //   component: ViewDnoteComponent
  // },
   {
    path: 'viewjournal/:journal/:journalid/:journalno',
    component: ViewJournalsComponent
  },
  
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class AccountsRoutingModule { }
