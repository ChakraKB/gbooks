import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReceiptsListComponent } from './receipts-list/receipts-list.component';
import { ReceipteditComponent } from './receiptedit/receiptedit.component';
import { ReceiptViewComponent } from './receipt-view/receipt-view.component';
import { ReceiptupdateComponent } from './receiptupdate/receiptupdate.component'
import { InvmatchingComponent } from './invmatching/invmatching.component'
const routes: Routes = [
  {
    path: 'list',
    component: ReceiptsListComponent,
    data: {
      title: 'Receipts List'
    }
  },
  {
    path: 'receiptedit',
    component: ReceipteditComponent,
    data: {
      title: 'Add/Edit Payment'
    }
  },
  {
    path: 'viewreceipt/:receipt/:pymtrectid',
    component: ReceiptViewComponent
  },
  {
    path: 'invmatching/:pymtrectid',
    component: InvmatchingComponent
  },
  {
    path: 'editreceipt/:pymtrectid',
    component: ReceiptupdateComponent,
    data: {
      title: 'Edit Receipt'
    }
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class ReceiptsRoutingModule { }
