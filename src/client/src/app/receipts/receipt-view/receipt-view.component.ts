import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import * as _ from "lodash";
import { Router, NavigationExtras } from '@angular/router';
import { ReceiptService } from '../receipt.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-receipt-view',
  templateUrl: './receipt-view.component.html',
  styleUrls: ['./receipt-view.component.scss']
})
export class ReceiptViewComponent implements OnInit {
  public currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  public date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  @Input() pymtrectid: any;
  receiptData: any = [];
  receiptDetails: any = [];
  userstoragedata: any = {};
  refno: any;
  mode: string;
  constructor(private storageservice: LocalStorageService, private route: ActivatedRoute,private location: Location,
    private receiptservice: ReceiptService, private router: Router) {
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.route.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.pymtrectid = params.pymtrectid;
        this.mode = params.receipt;
      }
    });
  }

  ngOnInit() {
    if (this.pymtrectid && this.pymtrectid != undefined) {
      this.loadReceipts();
    }
  }

  redirecttoprev() {
    if (this.mode == "fromledger") {
      this.router.navigate(['/reports/trailbalance']);
    }
    else if (this.mode == "receipt") {
      this.router.navigate(['/receipts/list']);
    }
    this.location.back();
  }

  loadReceipts() {
    this.receiptservice.ReceiptGetbyId({ paymentReceiptid: this.pymtrectid, feature: "Receipt" })
      .then((res) => {
        if (res.status) {
          this.receiptData = res.data[0];
          this.receiptDetails = this.receiptData.details[0];
        }
      });
  }

}
