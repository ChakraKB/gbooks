import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { LocalStorageService } from '../../../shared/local-storage.service';
import * as _ from "lodash";
import { MasterService } from '../../../services/master.service';
import {ReceiptService} from '../../../../app/receipts/receipt.service';
import{AppConstant}from '../../../app.constant'
@Component({
  selector: 'app-view-customer-advance',
  templateUrl: './view-customer-advance.component.html',
  styleUrls: ['./view-customer-advance.component.scss']
})
export class ViewCustomerAdvanceComponent implements OnInit {
  @Input() pymtrectid: any;
  cust_advanceData: any;
  cust_advancedatas: any = [];
  cust_advancetaxs: any = [];
  userstoragedata: any = {};
  resources: any = {};
  reqdata: any = {};
  cust_advance_feature:string = "Custormer Advance";
  dataFormat:string;
  currency_Symbol:string;

  constructor(private masterservice: MasterService, private storageservice: LocalStorageService,
    private route: ActivatedRoute, private receiptservice: ReceiptService) { 
      this.route.params.subscribe(params => {
        if (!_.isEmpty(params)) {
          this.pymtrectid = params.pymtrectid;
          console.log("url params", params);
        }
      });
      this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
      this.resources = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.RESOURCE);
      this.dataFormat=AppConstant.API_CONFIG.DATE.displayFormat;
      this.currency_Symbol=AppConstant.API_CONFIG.CURRENCY_FORMAT;
  }

  ngOnInit() {
    this.reqdata = {
      "pymtrectid": this.pymtrectid
    };
    console.log("customerdetails", this.pymtrectid);
    if (this.pymtrectid && this.pymtrectid != undefined) {
      this.loadcust_advanceDetails(this.reqdata);
    }
  }
  loadcust_advanceDetails(reqdata) {
    
        this.cust_advanceData = [];
        var reqdata: any = {
          "feature": "Receipt",
          "paymentReceiptid": this.pymtrectid
        };
        this.receiptservice.ReceiptGetbyId(reqdata)
          .then((res) => {
            if (res.status) {
              this.cust_advanceData = res.data[0];
              this.cust_advancedatas = res.data[0].details[0];
              // this.cust_advancetaxs = res.data[0].invoiceTaxes;
              console.log("Customer adv data: ", JSON.stringify(this.cust_advanceData));
              console.log( res.data[0].details[0])
              // if( res.data[0].feature == "proforma_invoice")
              // {
              // this.invoice_feature = "Pro-Forma Invoice";
              // }
            }
          });
      }

}
