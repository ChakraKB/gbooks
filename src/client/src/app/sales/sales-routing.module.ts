import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { AddEditInvoiceComponent } from './add-edit-invoice/add-edit-invoice.component';
import { CustomeradvanceListComponent } from '../sales/customer-advance/customeradvance-list/customeradvance-list.component';
import { proformaInvoicelistComponent } from './proforma-invoice/proforma-invoicelist.component';
import { proformaInvoiceAddeditComponent } from './proforma-invoice/addedit/proforma-invoice-addedit.component';
import { CustomeradvanceAddeditComponent } from './customer-advance/customeradvance-addedit/customeradvance-addedit.component';
import { EditInvoiceComponent } from './edit-invoice/edit-invoice.component';
import { EditComponent } from './proforma-invoice/edit/edit.component';
import { CustomerAdvanceUpdateComponent } from './customer-advance/customer-advance-update/customer-advance-update.component'
import { ViewCustomerAdvanceComponent } from './customer-advance/view-customer-advance/view-customer-advance.component'
import { ViewInvoiceComponent } from '../sales/view-invoice/view-invoice.component'

const routes: Routes = [
  {
    path: 'list',
    component: InvoiceListComponent,
    data: {
      title: 'Invocie List'
    }
  },
  {
    path: 'addedit',
    component: AddEditInvoiceComponent,
    data: {
      title: 'Add/Edit Invoice'
    }
  },
  {
    path: 'editinvoice/:invoiceid',
    component: EditInvoiceComponent,
    data: {
      title: 'Edit Invoice'
    }
  },

  {
    path: 'customeradvance',
    component: CustomeradvanceListComponent,
    data: {
      title: 'Customer Advance'
    }
  },
  {
    path: 'customeradvanceaddedit',
    component: CustomeradvanceAddeditComponent,
    data: {
      title: 'Customer Advance Add/Edit'
    }
  },
  {
    path: 'customeradvanceupdate/:pymtrectid',
    component: CustomerAdvanceUpdateComponent,
    data: {
      title: 'Customer Advance Add/Edit'
    }
  },
  {
    path: 'proformainvoice',
    component: proformaInvoicelistComponent,
    data: {
      title: 'proforma Invoice'
    }
  },
  {
    path: 'proformaadd',
    component: proformaInvoiceAddeditComponent,
    data: {
      title: 'proforma Invoice'
    }
  },
  {
    path: 'editproformainvoice/:invoiceid',
    component: EditComponent,
    data: {
      title: 'Edit Pro-Forma Invoice'
    }
  },
  {
    path: 'viewcustomeradvance/:pymtrectid',
    component: ViewCustomerAdvanceComponent
  },
  {
    path: 'viewinvoice/:invtype/:invoiceid',
    component: ViewInvoiceComponent
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
})
export class SalesRoutingModule { }
