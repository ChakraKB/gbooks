import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { InvoiceDataTable } from '../invoice-data-table.interface';
import { SalesService } from '../../services/sales/sales.service';
import { LocalStorageService } from '../../shared/local-storage.service';
import { DashboardService } from '../../services/dashboard.service';
import { AppConstant } from '../../app.constant';
import { ViewInvoiceComponent } from '../view-invoice/view-invoice.component';
import { ContextMenuModule, MenuItem } from 'primeng/primeng';
import { Router, NavigationExtras } from '@angular/router';
import * as _ from "lodash";
import { CurrencyPipe } from "@angular/common";

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {
  public currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  public date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  public paginator = AppConstant.API_CONFIG.PAGINATOR.LISTPAGES;
  // private invoicelistmenuitem: MenuItem[];
  public invoicelistmainmenu: MenuItem[];
  barData: any;
  lineData: any;
  invoicelist: any = [];
  proformainvoicelist: any = [];
  userstoragedata: any;
  finyear: any;
  selectedinvoice: any = [];
  activeTab: String = "-";
  invoicereqData: any = [];
  proformainvoicereqData: any = [];
  currencyFilter: CurrencyPipe;
  show : boolean = false;

  // Chart variable declartions
  invoiceSummaryData;
  invoiceCountData;
  transactionCount = {
    invoice: 0,
    bill: 0,
    payment: 0,
    receipt: 0,
  };
  transactionNoRecordDisp = {
    invoice: false,
    invoiceCount: false 
  }
  barChartConfig: any = {
    legend: {
      position: 'bottom'
    },
    scales: {},

  };
  // Chart variable declartions

  constructor(private salesService: SalesService, private storageservice: LocalStorageService,
    private dashboardService: DashboardService,
    private router: Router) {
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.currencyFilter = new CurrencyPipe("en-in");

    console.log("this.finyear", this.finyear)
  }

  ngOnInit() {

    this.initBarChartConfig();
    // Begin     
    let commonParam: any = {};
    commonParam.tenantid = this.userstoragedata.tenantid;
    commonParam.finyear = this.finyear.finyear;

    this.getInvoiceSummary(commonParam);
    this.getInvoiceCountSummary(commonParam);
    this.invoicelistmainmenu = [
      {
        label: 'Invoice', icon: 'fa-plus', command: (event) => {
          //event.originalEvent: Browser event
          //event.item: menuitem metadata
          this.router.navigate(['sales/addedit']);
        }
      },
      {
        label: 'Pro-Forma Invoice', icon: 'fa-plus', command: (event) => {
          //event.originalEvent: Browser event
          //event.item: menuitem metadata
          this.router.navigate(['sales/proformaadd']);
        }
      },
    ];

    var self = this;
        
    this.invoicereqData = {
      tenantid: this.userstoragedata.tenantid,
      finyear: this.finyear.finyear,
      feature: "invoice",
      status: "Active"
    };
    this.proformainvoicereqData = {
      tenantid: this.userstoragedata.tenantid,
      finyear: this.finyear.finyear,
      feature: "proforma_invoice",
      status: "Active"
    };
    this.getInvoiceList();
    this.getProformaInvoiceList();
    // this.salesService.getInvoiceList(data)
    //   .then(function (res) {

    //   });
  }

  initBarChartConfig() {
    var self = this;
    this.barChartConfig.scales = {
      yAxes: [{
        ticks: {
          //max: parseInt(maxamount.toFixed(2)) - 2,
          callback(value, index, values) {
            return self.currencyFilter.transform(value, "USD", true,"1.0-0").substr(3);
          }
        }
      }]
    },
     this.barChartConfig.tooltips= {
                enabled: true,
                mode: 'single',
                callbacks: {
                    label (tooltipItems, data) {
                        return self.currencyFilter.transform( tooltipItems.yLabel,"USD",true,"1.0-0").substr(3);
                    }
                }
            };
  }
  getInvoiceSummary(queryParam) {
    let label_1 = "Total Open Invoice";
    let label_2 = "Total Due Invoice";
    this.dashboardService.getInvoiceSummary(queryParam)
      .then((response: any) => {
        if (response.status) {
          if (!response.data || !response.data.length) {
            this.invoiceSummaryData = this.dashboardService.getDefaultBarChartData(label_1, label_2);
            this.transactionNoRecordDisp.invoice = true;
            return false;
          }
          let invoiceChartData: any = {};
          let labels = [];
          let datasets = [
                           {
                                label:label_1,
                                backgroundColor: '#50a72e',
                                borderColor: '#50a72e',
                                data: []
                            },
                            {
                                label:label_2,
                                backgroundColor: '#e9453b',
                                borderColor: '#e9453b',
                                data: []
                            }

          ];
          let isNonZeroExists = false;
          let minamount = response.data[0].invamount;
          let maxamount = response.data[0].invamount;
          _.forEach(response.data, (value: any) => {

            if (value.invamount || value.balamount) {
              isNonZeroExists = true;
            }

            if (value.invamount > maxamount) {
              maxamount = value.invamount;
            }
            else if (value.invamount < minamount) {
              minamount = value.invamount;
            }

            datasets[0].data.push(value.invamount);
            datasets[1].data.push(value.balamount);
            labels.push(value.label);
          });

          if (!isNonZeroExists) {
            this.invoiceSummaryData = this.dashboardService.getDefaultBarChartData(label_1, label_2);
            this.transactionNoRecordDisp.invoice = true;
            return;
          }

          invoiceChartData.labels = labels;
          invoiceChartData.datasets = datasets;

          // Prepare Chart Config Object
          var self = this;         
          this.invoiceSummaryData = invoiceChartData;
        }
        else {
          this.invoiceSummaryData = this.dashboardService.getDefaultBarChartData(label_1, label_2);
          this.transactionNoRecordDisp.invoice = true;
        }

      })
  }

  getInvoiceCountSummary(queryParam) {
    let label_1 = "Total Invoice";
    this.dashboardService.getInvoiceCountSummary(queryParam)
      .then((response: any) => {
        if (response.status) {
          if (!response.data || !response.data.length) {
            this.invoiceCountData = this.dashboardService.getDefaultBarChartData(label_1);
            this.transactionNoRecordDisp.invoiceCount = true;
            return false;
          }
          let invoiceChartData: any = {};
          let labels = [];
          let datasets = [
                           {
                                label:label_1,
                                backgroundColor: '#42A5F5',
                                borderColor: '#1E88E5',
                                data: []
                            }                           

          ];
          let isNonZeroExists = false;
          let minamount = response.data[0].count;
          let maxamount = response.data[0].count;
          _.forEach(response.data, (value: any) => {

            if (value.count) {
              isNonZeroExists = true;
            }

            if (value.count > maxamount) {
              maxamount = value.count;
            }
            else if (value.count < minamount) {
              minamount = value.count;
            }

            datasets[0].data.push(value.count);
            labels.push(value.label);
          });

          if (!isNonZeroExists) {
            this.invoiceCountData = this.dashboardService.getDefaultBarChartData(label_1);
            this.transactionNoRecordDisp.invoiceCount = true;
            return;
          }

          invoiceChartData.labels = labels;
          invoiceChartData.datasets = datasets;

          // Prepare Chart Config Object
          var self = this;         
          this.invoiceCountData = invoiceChartData;
        }
        else {
          this.invoiceCountData = this.dashboardService.getDefaultBarChartData(label_1);
          this.transactionNoRecordDisp.invoiceCount = true;
        }

      })
  }

  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }

  getInvoiceList() {
    this.salesService.getInvoiceList(this.invoicereqData)
      .then(res => {
        this.invoicelist = res.data;
        console.log(this.invoicelist);
      });
  }
  getProformaInvoiceList() {
    this.salesService.getInvoiceList(this.proformainvoicereqData)
      .then(res => {
        this.proformainvoicelist = res.data;
        console.log(this.proformainvoicelist);
      });
  }
  loadData(event) {
    //event.first = First row offset
    //event.rows = Number of rows per page
    console.log(event);
  }
  addTabViewinvoice(item) {
    this.activeTab = "-";
    if (this.selectedinvoice.length > 0) {
      this.selectedinvoice.unshift(item);
    }
    else {
      this.selectedinvoice.push(item);
    }
    console.log(this.selectedinvoice);
    this.activeTab = item.invoiceno;
  }
  handletabClose(e) {
    alert(e);
    e.close();

  }
  handleTabChange(e, menu) {
    console.log(e);
  }
  onContextSelect(event) {
    console.log(event);
  }
}
