import { Component, OnInit, EventEmitter, ElementRef, Input, Output } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import * as _ from "lodash";
import { MasterService } from '../../services/master.service';
import { SalesService } from '../../services/sales/sales.service';
import { Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.scss']
})
export class ViewInvoiceComponent implements OnInit {
  @Input() invoiceid: any;
  @Input() feature: any;
  invoiceDeta: any;
  invoicedatas: Array<any> = [];
  invoicetaxs: Array<any> = [];
  userstoragedata: any = {};
  resources: any = {};
  reqdata: any = {};
  invoice_feature: string = "Invoice";
  flag: string;
  public currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  public date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  constructor(private masterservice: MasterService, private storageservice: LocalStorageService,private location: Location,
    private route: ActivatedRoute, private salesservice: SalesService, private router: Router) {
    this.route.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.invoiceid = params.invoiceid;
        this.flag = params.invtype;
        console.log("url params", params);
      }
    });
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.resources = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.RESOURCE);

  }

  ngOnInit() {
    this.reqdata = {
      "invoiceid": this.invoiceid
    };
    console.log("invoicedetails", this.invoiceid);
    if (this.invoiceid && this.invoiceid != undefined) {
      this.loadInvoiceDetails(this.reqdata, this.flag);
    }
  }

  redirecttoprev() {
    if (this.flag == "fromledger") {
      this.router.navigate(['/reports/trailbalance']);
    }
    else if (this.flag == "invoice") {
      this.router.navigate(['/sales/list']);
    }
    else if (this.flag == "proforma") {
      this.router.navigate(['/sales/list']);
    }
    this.location.back();
  }

  loadInvoiceDetails(reqdata, flag) {
    this.invoiceDeta = [];
    var reqdata: any = {
      "invoiceid": this.invoiceid
    };
    if (!_.isEmpty(flag)) {
      if (flag == "proforma")
        reqdata.feature = "proforma_invoice";
      else if (flag == "invoice") {
        reqdata.feature = "invoice";
      }else{
        
      }
    } else {
      reqdata.feature = this.feature;
    }


    this.salesservice.getInvoiceById(reqdata)
      .then((res) => {
        if (res.status) {
          this.invoiceDeta = res.data[0];
          this.invoicedatas = res.data[0].invoiceDetails;
          this.invoicetaxs = res.data[0].invoiceTaxes;
          console.log("Invoice data: ", this.invoiceDeta);
          if (res.data[0].feature == "proforma_invoice") {
            this.invoice_feature = "Pro-Forma Invoice";
          }
        }
      });
  }


}
