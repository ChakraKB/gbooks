import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SalesRoutingModule } from './sales-routing.module';
import { SalesService } from '../services/sales/sales.service';
import { ProductallService } from '../products/productall.service';
import { MasterService } from '../services/master.service';
import { FeaturesService } from '../services/features.service';
import { UtilsService } from '../services/utils.service';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { AddEditInvoiceComponent } from './add-edit-invoice/add-edit-invoice.component';
import { CustomeradvanceListComponent } from './customer-advance/customeradvance-list/customeradvance-list.component';
import { CustomeradvanceAddeditComponent } from './customer-advance/customeradvance-addedit/customeradvance-addedit.component';
import { CustomerAdvanceUpdateComponent } from './customer-advance/customer-advance-update/customer-advance-update.component'
import { AutoCompleteModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ChartModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { DataTableModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { InputTextareaModule } from 'primeng/primeng';
import { MenuModule } from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';
import { SplitButtonModule } from 'primeng/primeng';
import { TabMenuModule } from 'primeng/primeng';
import { TabViewModule } from 'primeng/primeng';
import { TooltipModule } from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {RadioButtonModule} from 'primeng/primeng';
import { proformaInvoicelistComponent } from './proforma-invoice/proforma-invoicelist.component';
import { proformaInvoiceAddeditComponent } from './proforma-invoice/addedit/proforma-invoice-addedit.component';
import { EditInvoiceComponent } from './edit-invoice/edit-invoice.component';
import { AccountsService } from '../accounts/service/accounts.service';
import { ReceiptService } from '../receipts/receipt.service'
import { BlockUIModule } from 'primeng/primeng';
import { EditComponent } from './proforma-invoice/edit/edit.component';
import { ViewCustomerAdvanceComponent } from './customer-advance/view-customer-advance/view-customer-advance.component';
import {SharedModuleModule} from '../shared-module/shared-module.module'

@NgModule({
  imports: [
    SharedModuleModule,
    CommonModule,
    FormsModule,
    SalesRoutingModule,
    AutoCompleteModule,
    ButtonModule,
    CalendarModule,
    ChartModule,
    DataListModule,
    DataTableModule,
    DropdownModule,
    GrowlModule,
    InputTextModule,
    InputTextareaModule,
    MenuModule,
    MessagesModule,
    PaginatorModule,
    SplitButtonModule,
    TabMenuModule,
    TabViewModule,
    TooltipModule,
    PanelModule,
    RadioButtonModule,
    BlockUIModule
  ],
  declarations: [
    InvoiceListComponent,
    AddEditInvoiceComponent,
    proformaInvoicelistComponent,
    proformaInvoiceAddeditComponent,
    CustomeradvanceListComponent,
    ViewCustomerAdvanceComponent,
    CustomeradvanceAddeditComponent,
    EditInvoiceComponent,
    CustomerAdvanceUpdateComponent,
    EditComponent
  ],
  providers: [
    SalesService,
    FeaturesService,
    MasterService,
    ProductallService,
    AccountsService,
    ReceiptService,
    UtilsService
  ],
})
export class SalesModule {
  constructor() {

  }
}
