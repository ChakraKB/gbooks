import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import * as _ from "lodash";
import { PaymentService } from '../payment.service';
import { Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-payment',
  templateUrl: './view-payment.component.html',
  styleUrls: ['./view-payment.component.scss']
})
export class ViewPaymentComponent implements OnInit {
  public currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  public date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  @Input() pymtrectid: any;
  paymentData: any = [];
  paymentDetails: any = [];
  userstoragedata: any = {};
  refno: any;
  mode: string;
  constructor(private storageservice: LocalStorageService,private location: Location, private route: ActivatedRoute, private paymentservice: PaymentService,
    private router: Router,
  ) {
    this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.route.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.pymtrectid = params.pymtrectid;
        this.mode = params.payment;
      }
    });
  }

  ngOnInit() {
    if (this.pymtrectid && this.pymtrectid != undefined) {
      this.loadPayments();
    }

  }

  redirecttoprev() {
    if (this.mode == "fromledger") {
      this.router.navigate(['/reports/trailbalance']);
    }
    else if (this.mode == "payment") {
      this.router.navigate(['/payment/list']);
    }
    this.location.back();
  }
  loadPayments() {
    this.paymentservice.PaymentgetbyID({ paymentReceiptid: this.pymtrectid, feature: "payment" })
      .then((res) => {
        if (res.status) {
          this.paymentData = res.data[0];
          this.paymentDetails = this.paymentData.details[0];
        }
      });
  }


}
