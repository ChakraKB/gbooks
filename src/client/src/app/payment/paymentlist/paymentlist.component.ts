import { Component, OnInit } from '@angular/core';
import { SalesService } from '../../services/sales/sales.service';
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import { PaymentService } from '../payment.service';
import { CurrencyPipe } from "@angular/common";
import { DashboardService } from '../../services/dashboard.service';
import * as _ from "lodash";

@Component({
  selector: 'app-paymentlist',
  templateUrl: './paymentlist.component.html',
  styleUrls: ['./paymentlist.component.scss']
})
export class PaymentlistComponent implements OnInit {
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  private date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  public paginator = AppConstant.API_CONFIG.PAGINATOR.LISTPAGES;
  barData: any;
  lineData: any;
  invoicelist: any = [];
  userstoragedata: any;
  finyear: any;
  selectedinvoice: any = [];
  userdetails: any;
  matchedlist: any[];
  unmatchedlist: any[];
  heads: any;
  show : boolean=false;
  chartConfig={
    self :this,
    legend: {
              position: 'bottom'
          },
            tooltips: {
              enabled: true,
              mode: 'single',
              callbacks: {
                  label (tooltipItem, data) {
                  var allData = data.datasets[tooltipItem.datasetIndex].data;
                  var tooltipLabel = data.labels[tooltipItem.index];
                  var tooltipData = allData[tooltipItem.index];
                  
                  return tooltipLabel+":"+this._options.self.currencyFilter.transform( tooltipData,"USD",true,"1.0-0").substr(3);
              }
              }
          }        
  };

  currencyFilter: CurrencyPipe;

    // Chart variable declartions
  paymentSummaryData;
  paymentCountData; 
  transactionNoRecordDisp = {
    payment: false,
    paymentCount: false 
  }
  barChartConfig: any = {
    legend: {
      position: 'bottom'
    },
    scales: {},

  };
  // Chart variable declartions


  constructor(private localstorageservice: LocalStorageService, private paymentService: PaymentService,
  private dashboardService: DashboardService,) {
    this.userdetails = this.localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.heads = this.localstorageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.HEADS);
    this.currencyFilter = new CurrencyPipe("en-in");
  }

  ngOnInit() {
    this.getallpayments();

    this.initBarChartConfig();

    var self = this;
    
    // Begin     
    let commonParam: any = {};
    commonParam.tenantid = this.userdetails.tenantid;
    commonParam.finyear = this.finyear.finyear;

    this.getPaymentSummary(commonParam);
    this.getPaymentCountSummary(commonParam);


  }

  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }

  getallpayments() {
    var data = {
      "offset": 0,
      "limit": null,
      "query": {
        'tenantid': this.userdetails.tenantid,
        'feature': "Payment",
        'finyear': this.finyear.finyear
      }
    }
    this.paymentService.PaymentGetall(data).then((res) => {
      this.matchedlist = res.data;
      this.unmatchedlist = res.data;
      this.unmatchedlist = _.filter(this.unmatchedlist, function (res) {
        if (res.pymtrecttype == "ADVANCE" && res.balamount != 0) {
          return res;
        }
      });
      this.matchedlist = _.filter(this.matchedlist, function (res) {
        return res.pymtrecttype != "ADVANCE"
      });
    })
  }

  getPaymentSummary(queryParam) {
    let label_1 = "Total Advances";
    this.dashboardService.getPaymentSummary(queryParam)
      .then((response: any) => {
        if (response.status) {
          if (!response.data || !response.data.length) {
            this.paymentSummaryData = this.dashboardService.getDefaultBarChartData(label_1);
            this.transactionNoRecordDisp.payment = true;
            return false;
          }
          let invoiceChartData: any = {};
          let labels = [];
          let datasets = [
                           {
                                label:label_1,
                                backgroundColor: '#50a72e',
                                borderColor: '#50a72e',
                                data: []
                            }                            

          ];
          let isNonZeroExists = false;
          _.forEach(response.data, (value: any) => {

            if (value.amount) {
              isNonZeroExists = true;
            }
            datasets[0].data.push(value.amount);
            labels.push(value.label);
          });

          if (!isNonZeroExists) {
            this.paymentSummaryData = this.dashboardService.getDefaultBarChartData(label_1);
            this.transactionNoRecordDisp.payment = true;
            return;
          }

          invoiceChartData.labels = labels;
          invoiceChartData.datasets = datasets;

          // Prepare Chart Config Object
          var self = this;         
          this.paymentSummaryData = invoiceChartData;
        }
        else {
          this.paymentSummaryData = this.dashboardService.getDefaultBarChartData(label_1);
          this.transactionNoRecordDisp.payment = true;
        }

      })
  }

    getPaymentCountSummary(queryParam){
        let copyParam = {... queryParam};
        copyParam.limit = 4;

        let defaultLabel = ['Customer1','Customer2','Customer3'];

        this.dashboardService.getTopReceipts(copyParam)
        .then((response:any)=>{            
            if(response.status)
            {
                if(!response.data || !response.data.length)
                {
                  this.paymentCountData = this.dashboardService.getDefaultPieChartData(...defaultLabel);
                  this.transactionNoRecordDisp.paymentCount = true;                  
                  return;
                }

                let topSalesChartData:any = {};
                let labels=[];
                let datasets=[
                            {       
                                data: [],                         
                                backgroundColor: [
                                    "#FF6384",
                                    "#36A2EB",
                                    "#FFCE56"
                                ],
                                hoverBackgroundColor: [
                                    "#FF6384",
                                    "#36A2EB",
                                    "#FFCE56"
                                ]                               
                            }                            

                ];
                let isNonZeroExists = false;
                 _.forEach(response.data,(value:any)=>{
                     if(value.amount)
                     {
                         isNonZeroExists = true;
                     }
                      datasets[0].data.push(value.amount);
                      labels.push(value.label);
                 });

                 if(!isNonZeroExists)
                 {
                     this.paymentCountData = this.dashboardService.getDefaultPieChartData(...defaultLabel);
                     this.transactionNoRecordDisp.paymentCount = true; 
                     return;
                 } 

                 topSalesChartData.labels=labels;
                 topSalesChartData.datasets=datasets;
                                 
                 this.paymentCountData = topSalesChartData;
            }
            else{
                 this.paymentCountData = this.dashboardService.getDefaultPieChartData(...defaultLabel);
                 this.transactionNoRecordDisp.paymentCount = true; 
            } 
           
        })
    }

  initBarChartConfig() {
    var self = this;
    this.barChartConfig.scales = {
      yAxes: [{
        ticks: {
          //max: parseInt(maxamount.toFixed(2)) - 2,
          callback(value, index, values) {
            return self.currencyFilter.transform(value, "USD", true,"1.0-0").substr(3);
          }
        }
      }]
    },
     this.barChartConfig.tooltips= {
                enabled: true,
                mode: 'single',
                callbacks: {
                    label (tooltipItems, data) {
                        return self.currencyFilter.transform( tooltipItems.yLabel,"USD",true,"1.0-0").substr(3);
                    }
                }
            };
  }




}
