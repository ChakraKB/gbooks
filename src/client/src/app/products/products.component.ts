import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FeaturesService } from './../services/features.service';
import { ProductallService } from './productall.service';
import { LocalStorageService } from '../shared/local-storage.service';
import { AppConstant } from '../app.constant';
import { AutoCompleteModule } from 'primeng/primeng';
import { UtilsService } from '../services/utils.service';
import { MessagesService } from '../../app/shared/messages.service';
import { MasterService } from '../../app/services/master.service'
import * as _ from "lodash";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnChanges {
  public amtlength = AppConstant.API_CONFIG.AMOUNTFORMAT.MAXLENGTH;
  uom: SelectItem[];
  brand: SelectItem[];
  category: SelectItem[];
  selecteduomdetails: any;
  selectedbrandetails: any;
  selectedcatdetails: any;
  selectedbrandname: any[];
  selectedcatname: any[];
  selectedUOMdesc: any;
  selectedhsncode: any;
  selecteduom: any[];
  validation = "true";
  validationmsg = "";
  formdata = {};
  userdetails: any;
  alluoms: any[];
  allbrands: any[];
  allcategories: any[];
  allhsncode: any[];
  hsncode: any[];
  msgs: Message[] = [];
  filteredbrand: any[];
  filtereduoms: any[];
  filteredcat: any[];
  Indate: any;


  @Input() prodtls: any;
  @Output() notifyNewProduct: EventEmitter<any> = new EventEmitter();



  pro: FormGroup;
  productname: FormControl;
  productmrp: FormControl;
  post: any;
  changestatus: any[] = [];
  status: any = [];
  constructor(private masterservice: MasterService, private messageservice: MessagesService,
     private fb: FormBuilder, private ProductallService: ProductallService,
      private LocalStorageService: LocalStorageService, private featureservice: FeaturesService, private UtilsService: UtilsService) {
    this.pro = fb.group({
      'productname': [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(15)])],
      'productmrp': [null, Validators.required],
      'productcode': [null, Validators.required],
      'selecteduom': [null, Validators.required],      
      'hsncode': [null, Validators.required],
      'selectedbrand': [null, Validators.required],
      'selectedcategory': [null, Validators.required],
      'status': [null],
    })
    this.userdetails = LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.status = AppConstant.API_CONFIG.status;
    for (var i = 0; i < this.status.length; i++) {
      this.changestatus.push({
        label: this.status[i].stat, value: this.status[i].stat
      });
    }
  }
  callParent(message) {
    this.notifyNewProduct.next(message);
  }
  numberOnly(event) {
    this.UtilsService.allowNumberOnly(event);
  }
  ngOnInit() {
    this.ProductallService.getAll({})
      .then((res) => {
        this.alluoms = res.data;
      });

    this.ProductallService.brandGetAll({ tenantid: this.userdetails.tenantid, status: "Active", })
      .then((res) => {
        this.allbrands = res.data;
      });

    this.ProductallService.categoryGetAll({ tenantid: this.userdetails.tenantid, status: "Active", })
      .then((res) => {
        this.allcategories = res.data;
      });
    this.featureservice.hsnGetAll({})
      .then((res) => {
        var allhsncode = res.data;
        this.hsncode = this.masterservice.formatDataforDropdown("hsncode", allhsncode, "Select HSN Code");
      });
    setTimeout(() => {
      this.viewproduct()
    }, 1000);



    var dateS = [];
    var today = new Date().toISOString()
    this.Indate = formatDate(today);
    function formatDate(date) {
      if (date) {
        dateS = date.split("-");
        var d = new Date();
        d.setMonth(dateS[1]);
        d.setFullYear(dateS[0]);
        d.setDate(parseInt(dateS[2]));
        var
          month = '' + (d.getMonth()),
          day = '' + d.getDate(),
          year = d.getFullYear();
        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
          day = '0' + day;
        return [year, month, day].join('-');
      }
    }
  }
  ngOnChanges() {
    setTimeout(() => {
      this.viewproduct()
    }, 1000);
  }
  handleDropdownClick(event, label) {
    if (label == "uom") {
      this.filtereduoms = [];
      setTimeout(() => {
        this.filtereduoms = this.alluoms;
      }, 100)
    }
    else if (label == "brand") {
      this.filteredbrand = [];
      setTimeout(() => {
        this.filteredbrand = this.allbrands;
      }, 100)
    }
    else if (label == "cat") {
      this.filteredcat = [];
      setTimeout(() => {
        this.filteredcat = this.allcategories;
      }, 100)
    }

  }

  searchuom(event) {
    this.filtereduoms = [];
    this.filtereduoms = _.filter(this.alluoms, function (c) {
      var uomname = c.uomdesc;
      return (uomname.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
    });
  }

  searchbrands(event) {
    this.filteredbrand = [];
    this.filteredbrand = _.filter(this.allbrands, function (c) {
      var brandname = c.brandname;
      return (brandname.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
    });
  }

  searchcategory(event) {
    this.filteredcat = [];
    this.filteredcat = _.filter(this.allcategories, function (c) {
      var catname = c.categoryname;
      return (catname.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
    });
  }

  selectuom(item) {
    // this.selectedUOMdesc = item.uomdesc;
    this.selecteduomdetails = item;
  }
  selectbrand(item) {
    // this.selectedbrandname = item.brandname;
    this.selectedbrandetails = item;
  }
  selectcategory(item) {
    // this.selectedcatname = item.categoryname;
    this.selectedcatdetails = item;
  }
  clearform() {
    this.pro.reset();
  }

  viewproduct() {
    if (!_.isEmpty(this.prodtls)) {
      var self = this;
      this.selecteduomdetails = _.find(this.alluoms, function (o) { return o.uomid == self.prodtls.uomid; });
      this.selectedbrandetails = _.find(this.allbrands, function (o) { return o.brandid == self.prodtls.brandid; });
      this.selectedcatdetails = _.find(this.allcategories, function (o) { return o.categoryid == self.prodtls.categoryid; });
      this.selectedhsncode = _.find(this.hsncode, { value: { hsncode: this.prodtls.hsncode } })
      this.pro = new FormGroup({
        'productname': new FormControl(this.prodtls.prodname,Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15)])),
        'productmrp': new FormControl(this.prodtls.mrp),
        'productcode': new FormControl(this.prodtls.productcode),
        'selecteduom': new FormControl(this.selecteduomdetails),        
        'hsncode': new FormControl(this.selectedhsncode.value),
        'selectedbrand': new FormControl(this.selectedbrandetails),
        'selectedcategory': new FormControl(this.selectedcatdetails),
        'status': new FormControl(this.prodtls.status,Validators.required),
      })
    }
  }

  setSelectedHsn(item) {
    this.selectedhsncode = item.value;
  }
   
     
  formObj:any = {
     
       productname:{
         required :"Product Name Required",
         minlength : "Product Name atleast 1 charactors",
         maxlength : "Product Name not more than 30 charactors"
        },
        productmrp:{
         required :"Product productmrp Required"        
        },
         productcode:{
         required :"productcode Required"        
        },
         hsncode:{
         required :"hsncode Required"        
        },
        selecteduom:{
         required :"selecteduom Required"        
        },
        selectedbrand:{
         required :"selectedbrand Required"        
        },
         selectedcategory:{
         required :"selectedcategory Required"        
        },
        status:{
         required :"status Required"        
        }
  }
  createpro(data) {

    if(this.pro.status == "INVALID")
    {
      var errorMessage = this.masterservice.getFormErrorMessage(this.pro,this.formObj);
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: errorMessage });
      return false;
    }
     
    // this.validation = "true";
    // this.validationmsg = "";
    // if (!data.selectedcategory) {
    //   this.validation = "false";
    //   this.validationmsg = "Please Select Category"
    // }
    // if (!data.selectedbrand) {
    //   this.validation = "false";
    //   this.validationmsg = "Please Select Brand"
    // }
    // if (!data.selecteduom) {
    //   this.validation = "false";
    //   this.validationmsg = "Please Select UOM"
    // }
    // if (!data.productcode) {
    //   this.validation = "false";
    //   this.validationmsg = "Please enter Product Code"
    // }
    // if (!data.productmrp) {
    //   this.validation = "false";
    //   this.validationmsg = "Please enter MRP"
    // }
    // if (!data.productname) {
    //   this.validation = "false";
    //   this.validationmsg = "Please enter Product Name"
    // }



      if (this.prodtls) {
        this.formdata = {
          'prodid': this.prodtls.prodid,
          'tenantid': this.userdetails.tenantid,
          'tenantname': this.userdetails.tenantname,
          'prodname': data.productname,
          'mrp': data.productmrp,
          'productcode': data.productcode,
          'hsncode': data.hsncode.hsncode,
          'hsndesc' : data.hsncode.description,
          "uomid": this.selecteduomdetails.uomid,
          'uomdesc': this.selecteduomdetails.uomdesc,
          'brandid': this.selectedbrandetails.brandid,
          'brandname': this.selectedbrandetails.brandname,
          'categoryid': this.selectedcatdetails.categoryid,
          'categoryname': this.selectedcatdetails.categoryname,
          'status': data.status,
          'createdby': this.userdetails.loginname,
          'createddt': this.Indate,
          'lastupdatedby': this.userdetails.loginname,
          'lastupdateddt': this.Indate,
        }
        this.ProductallService.ProductUpdate(this.formdata)
          .then((res) => {
            if (res.status == true) {
              this.msgs = [];
              this.messageservice.showSuccessMessage({ severity: 'success', summary: 'Product Updated Successfully ', detail: '' });
              this.callParent(res.message);
              this.clearform();
            }
            else if (res.status == false) {
              this.msgs = [];
              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
            }
          });
      }
      else {
        this.formdata = {
          'tenantid': this.userdetails.tenantid,
          'tenantname': this.userdetails.tenantname,
          'prodname': data.productname,
          'mrp': data.productmrp,
          'productcode': data.productcode,
          'hsncode': data.hsncode.hsncode,
          'hsndesc' : data.hsncode.description,
          "uomid": this.selecteduomdetails.uomid,
          'uomdesc': this.selecteduomdetails.uomdesc,
          'brandid': this.selectedbrandetails.brandid,
          'brandname': this.selectedbrandetails.brandname,
          'categoryid': this.selectedcatdetails.categoryid,
          'categoryname': this.selectedcatdetails.categoryname,
          'status': "Active",
          'createdby': this.userdetails.loginname,
          'createddt': this.Indate,
          'lastupdateddt': this.Indate,
        }
        this.ProductallService.ProductCreate(this.formdata)
          .then((res) => {
            if (res.status == true) {
              this.msgs = [];
              this.messageservice.showSuccessMessage({ severity: 'success', summary: 'Product Created Successfully ', detail: '' });
              this.callParent(res.message);
              this.clearform();
            }
            else if (res.status == false) {
              this.msgs = [];
              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: res.message });
            }
          });
      }
    // else {
    //   this.msgs = [];
    //   this.msgs.push({ severity: 'error', summary: 'Error Message', detail: this.validationmsg });
    //   return false;
    // }



  }

}
