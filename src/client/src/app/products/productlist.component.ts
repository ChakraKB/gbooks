import { Component, OnInit, Renderer, ElementRef, Input, Output, ViewChild } from '@angular/core';
import { ProductallService } from './productall.service';
import { LocalStorageService } from '../shared/local-storage.service';
import { AppConstant } from '../app.constant';
import { UtilsService } from '../services/utils.service';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductlistComponent implements OnInit {
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  private date_dformat = AppConstant.API_CONFIG.DATE.displayFormat;
  public paginator = AppConstant.API_CONFIG.PAGINATOR.REPORTPAGES;
  userdetails: any
  allproducts: any;
  editpro: any;
  addproduct: boolean = false;
  editproduct: boolean = false;
  addnew: boolean = false;
  list: boolean = true;
  prodtls: any[];
  name: string;
  show: boolean = false;
  selectedpro:any[]=[];
  product:any
  constructor(private ProductallService: ProductallService, private LocalStorageService: LocalStorageService,
  private UtilsService:UtilsService) {
    this.userdetails = LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
  }

  @ViewChild('wrapper') wrapper: ElementRef;

  ngOnInit() {
    this.getAllProducts();
  }

  getAllProducts() {
    this.ProductallService.ProductGetAll({ tenantid: this.userdetails.tenantid })
      .then((res) => {
        this.allproducts = res.data;
      });
  }
  notifyNewProduct(event) {
    this.getAllProducts();
    this.addproduct = false;
    this.editproduct = false;
    this.addnew = false;
    this.editpro = false;
    this.list = true;
  }
  Addproducts(protabview) {
    this.addproduct = true;
    this.editproduct = false;
    this.addnew = true;
    this.editpro = false;
    this.list = false;
    this.prodtls = null;
    this.selectedpro=[];
    protabview.activeIndex = 1;
    // this.wrapper.nativeElement.click()
  }

  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }


  handleClose(event,protabview) {
    this.addproduct = false;
    this.editproduct = false;
    this.list = true;
    this.UtilsService.deactivate_multitab(this.selectedpro,event,protabview,"prodid");

  }

  Editproduct(prodtls, protabview) {
    this.list = false;
    this.addproduct = false;
    this.editproduct = true;
    this.addnew = false;
    this.editpro = true;
    this.prodtls = prodtls;
    this.selectedpro=[];
    protabview.activeIndex = 1;
  }
  viewproduct(items,protabview){
    this.list = false;
    this.addproduct = false;
    this.editproduct = false;
    this.addnew = false;
    this.editpro = false;
  this.product=items;
  this.UtilsService.activate_multitab(this.selectedpro, items, protabview,"prodid");
  }
}
