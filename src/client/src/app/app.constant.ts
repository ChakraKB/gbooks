export const AppConstant = Object.freeze({

  API_ENDPOINT: "http://27.250.1.58:5001/efinance/",
  ACCOUNT: {
    ACC_URL: "http://localhost:5500/",
    BASE_URL: "http://27.250.1.58:5000/ibacapi/",
    APP_SETTING: "dashboard/#/",
    API: {
      LOGOUT: 'logout/',
      SESSION_INFO: 'session/'
    }
  },
  API_CONFIG: {
    APP_CONTENT: {
      APP_NAME: "infoG",
      APP_DESC: "infoG Admin Dashboard",
    },
    LOCALSTORAGE: {
      STR_PREFIX: "infob-",
      STR_AUTH: "AuthendicationSuccess",
      STR_AUTHSUCCESS: "isAuthenticated",
      USER: "user",
      FINYEAR: "finyear",
      HEADS: "heads",
      USERROLE: "userroles",
      RESOURCE: "resources",
      TOKEN: "token"
    },
    FINYEARS: [
      { "finyear": "APR 2015 - MAR 2016", "prevfinyear": "APR 2014 - MAR 2015", "YearStartsFrom": "01-04-2015", "YearEndsOn": "31-03-2016" },
      { "finyear": "APR 2016 - MAR 2017", "prevfinyear": "APR 2015 - MAR 2016", "YearStartsFrom": "01-04-2016", "YearEndsOn": "31-03-2017" },
      { "finyear": "APR 2017 - MAR 2018", "prevfinyear": "APR 2016 - MAR 2017", "YearStartsFrom": "01-04-2017", "YearEndsOn": "31-03-2018" },
      { "finyear": "APR 2019 - MAR 2020", "prevfinyear": "APR 2017 - MAR 2018", "YearStartsFrom": "01-04-2018", "YearEndsOn": "31-03-2019" },
      { "finyear": "APR 2020 - MAR 2021", "prevfinyear": "APR 2019 - MAR 2020", "YearStartsFrom": "01-04-2019", "YearEndsOn": "31-03-2020" }],
    EMPTYMESSAGE:
    {
      MESSAGE: "No Records Found"
    },
    M_BASE_URL: 'views/',
    M_TENANTS_URL: 'views/tenents/',
    HEADER_CONTENT_TYPE: {
      FORM_URL_ENCODE: "application/x-www-form-urlencoded;charset=utf-8;",
      APPLICATION_JSON: "application/json",
    },
    PAYMENT: {
      //                  INSTAMOJOURL: "https://www.instamojo.com/gntstech/infog-payment/",  // dev
      INSTAMOJOURL: "https://www.instamojo.com/gntstech/infog-trail/"  // qc
    },
    JASPER_REPORTSERVER: "http://lowcost-env.qm8mcxiehm.ap-south-1.elasticbeanstalk.com/",
    RUPEES: {
      symbol: '₹. ',
      symbol2: '₹'
    },
    PAGINATOR:
    {
      LISTPAGES: 5,
      REPORTPAGES: 10
    },
    AMOUNTFORMAT:
    {
      MAXLENGTH: '12'
    },
    CURRENCY_FORMAT: "INR",
    DATE: {
      format1: "dd-MM-yyyy",
      apiFormat: "YYYY-MM-DD",  // A valid moment js data format. Refer https://momentjs.com/docs/#/parsing/string-format/
      displayFormat: "dd-MMM-yyyy"
    },
    ANG_DATE: {
      displaydtime: "dd-MM-y HH:mm",
      displayFormat: "dd-MM-y", // 01-31-2019 y-MM-dd
      apiFormat: "y-MM-dd",
    },
    status: [
      { stat: 'Active' },
      { stat: 'Inactive' }
    ],
    Features: [
      { name: 'Receipt' },
      { name: 'Payment' }
    ],
    Partygroup: [
      { name: 'All' },
      { name: 'Sundry Creditors' },
      { name: 'Sundry Debtors' }
    ],
    LEDGEREXCP: ['Cash In Hand', 'Purchases', 'Sales'],
    EXCLEDGER: ['Cash In Hand', 'Purchases', 'Sales', 'Bank'],
    LIABILITIES_HEAD: ['Capital', 'Current Liabilities And Provisions'],
    ASSETS_HEAD: ['Fixed Assets', 'Current Assets'],
    PROFITLOSS_HEADS: ['Expenditures (Indirect)', 'Income (Direct)',
      'Expenditures (Direct)', 'Income (Indirect)'],
    LEDGERTAXINSUBGROUP: ['Cash In Hand', 'GST Output 12%', 'GST Output 18%',
      'GST Output 28%', 'GST Output 5%'],
    BALANCESHEET_HEADS: ['Capital', 'Current Liabilities And Provisions',
      'Fixed Assets', 'Current Assets'],
    API_URL: {
      TNT_ACCOUNTCONFIG: {
        LIST: "config/list",
      },
      TNT_CODEMASTER: {
        LIST: "codeMaster/list ",
      },
      CONTACT: {
        CREATE: "contact/create",
        LIST: "contact/list",
        UPDATE: "contact/update",
        FINDBYID: ""  //new api for contact list instead of contact FINDALL
      },
      TNT_PRODUCTS: {
        CREATE: "products/create",
        UPDATE: "products/update",
        LIST: "products/list",
        FINDBYID: "products/findByID",
        DELETE: "products/delete",
        PROSEARCH: "products/search",
        HSNCODE: "products/hsn/list"
      },
      TNT_BRANDS: {
        CREATE: "brands/create",
        UPDATE: "brands/update",
        FINDALL: "brands/list",
        FINDBYID: "brands/findByID",
        DELETE: "brands/delete",
      },
      TNT_CATEGORY: {
        CREATE: "category/create",
        UPDATE: "category/update",
        FINDALL: "category/list",
        FINDBYID: "category/findByID",
        DELETE: "category/delete",
      },
      TNT_TAXES: {
        LIST: "taxmaster/list"
      },
      TNT_UOM: {
        CREATE: "uom/create",
        UPDATE: "uom/upsert",
        FINDALL: "uom/list",
        FINDBYID: "uom/findByID",
        DELETE: "uom/delete",
      },
      ACC_BOOKOFACCOUNTS: {
        LIST: "bookOfAccount/list",
        CREATE: "bookOfAccount/create",
        UPDATE: "bookOfAccount/update",
        FINDALL: "booksofaccounts/findAll",
      },
      TAX: {
        CREATE: "tax/create",
        UPDATE: "tax/update",
        FINDALL: "tax/findAll",
        FINDBYID: "tax/findByID",
        DELETE: "tax/delete",
      },
      ACC_HEADS: {
        LIST: "heads/list"
      },
      TNT_LEDGERSUMMARY: {
        NAMES: "reports/ledger/list"
      },
      TNT_BANK: {
        LISTALL: "journal/list"
      },
      TNT_JOURNAL: {
        FINDBYID: "journal/",
        CREATE: "journal/create",
        LISTALL: "journal/list",
        UPDATE: "journal/update",
        CONFIG: "reports/config/list",
        DETAILS: "journaldetails/findAll",
        FEATURE: "codemaster/findall",
      },
      TNT_INVOICE: {
        INVOICE: "invoice/",
        CREATE: "invoice/create",
        UPDATE: "invoice/update",
        LIST: "invoice/list",
        GETBYID: "invoice/getbyid",
        // CREATE: "invoices/generate",
        // UPDATE: "invoices/upsert",
        DELETE: "invoices/delete",
        FINDALL: "invoice/findAll",
        DETAILS: "invoicedetail/findAll",
        INVOICEPAYMENT: "payments/create",
        SEQ: "generator/seqenceno",
        ACCCONFIG: "accconfig/findAll"

      },
      TNT_LICENCE: {
        // CREATE: "tenantlicen/create",
        CREATE: "invoice/create",
        UPDATE: "tenantlicen/upsert",
        DELETE: "tenantlicen/delete",
        FINDALL: "tenantlicen/findAll",
      },
      TNT_BILL: {
        CREATE: "bill/create",
        LIST: "bill/list",
        BILL: "bill/",
        GETBYID: "bill/getbyid",
        UPDATE: "bill/update",
        FINDALL: "bills/findAll",
        DETAILS: "billsdetails/findAll",
        PAYMENTRECEIPTLIST: "receipts/findAll",
        PAYMENTRECEIPTDETAILS: "receiptsdetails/findAll",
        PAYMENTDETAILS: "receiptsdetails/findByID",
        BILLRECEIPT: "receipts/create",
      },
      ADV_RECEIPT: {
        CREATE: "receipts/advreccreate",
        INVDETAILS: "receipts/advdetails",
        ADVALLOCATE: "receipts/paymentmatch",
        ADVLIST: "receipts/advfindAll",
        GETADVINVDETAILS: "invoicedetail/advdetails",
        ADVRECUPDATE: "receipts/advupdate",
        RECEIPTUPDATE: "receipts/update"
      },
      TNT_ACCOUNT: {
        CREATE: "cdnotes/create",
        FINDALL: "cdnotes/list",
        FINDBYID: "cdnotes/getById",
        UPDATE: "cdnotes/update"
      },
      TNT_PAYMENT_RECEIPT: {
        CREATE: "paymentreceipt/create",
        LIST: "paymentreceipt/list",
        UPDATE: "paymentreceipt/update",
        FINDBYID: "paymentreceipt/getbyid"
      },
      PAYTERMS: {
        RECPAYTERM: "codemaster/findall",
        RECPAYMODE: "bookofaccount/paymentmode/list"
      },
      TAXES: {
        CREATE: "tenanttax/create",
        UPDATE: "tenanttax/upsert",
        FINDBYID: "tenanttax/findByID",
        FINDALL: "tenanttax/findAll",
        DELETE: "tenanttax/delete",
        GSTFINDALL: "taxmaster/findall"
      },
      ROLES: {
        CREATE: "role/create",
        UPDATE: "role/update",
        FINDBYID: "roles/findByID",
        FINDALL: "role/list",
        DELETE: "roles/delete"
      },
      TNTUSER: {
        CREATE: "users/create",
        UPDATE: "users/update",
        DELETE: "users/delete",
        FINDALL: "users/list"
      },
      TNT: {
        FINDBYID: "tenant/",
        TENANTUPDATE: "tenant/update"
      },
      BRAND: {
        CREATE: "brands/create",
        FINDALL: "brands/list",
        UPDATE: "brands/update"
      },
      PROD_GROUP: {
        CREATE: "category/create",
        FINDALL: "category/list",
        UPDATE: "category/update",
        FINDBYID: ""
      },
      PROFILE: {
        CHANGEPWD: "users/change/password"
      },
      BANK: {
        FINDALL: "bank/list",
        CREATE: "bank/create",
        UPDATE: "bank/update"
      },
      BRS: {
        LIST: "brs/list",
        CREATE: "brs/create",
        UPDATE: "brs/update",
        MATCHLIST: "brs/listForMatch"
      },
      COUNTRY: {
        FINDALL: "country/list"
      },
      STATE: {
        FINDALL: "state/list"
      },
      CITY: {
        FINDBYID: "city/list"

      },
      INDUSTRY_TYPE: {
        FINDALL: "industry/list"
      },
      DASHBOARD: {
        TOPCUSTOMERBYSALES: {
          GETTOPCUSTOMERBYSELES: "dashboard/top/sale/customers"
        },
        CREDITSDEBITS: {
          GETCREDITSDEBITS: "dashboard/creditsdebits/summery"
        },
        TAXSUMMARYREPORT: {
          GETTAXSUMMARYREPORT: "dashboard/tax/summery"
        },
        INVOICEREPORT: {
          GET_LIST: "dashboard/invoice/dues",
          GET_MONTH_COUNT: "dashboard/month/invoice/count"
        },
        BILLREPORT: {
          GET_LIST: "dashboard/bill/dues",
          GET_MONTH_COUNT: "dashboard/month/bill/count"
        },
        CUSTOMER_ADVANCE: {
          GET_LIST: "dashboard/month/customer/advance",
          GET_MONTH_COUNT: ""
        },
        VENDOR_ADVANCE: {
          GET_LIST: "dashboard/month/vendor/advance",
          GET_MONTH_COUNT: ""
        },
        RECEIPTS: {
          GET_LIST: "dashboard/month/receipt",
          TOP_RECEIPT: "dashboard/top/receipt/contact"
        },
        PAYMENTS: {
          GET_LIST: "dashboard/month/payment",
          TOP_PAYMENT: "dashboard/top/payment/contact"
        },
        INVOICECOUNT: "dashboard/invoice/count",
        BILLCOUNT: "dashboard/bill/count",
        PAYMENTRECEIPTCOUNT: "dashboard/paymentreceipt/count"
      },
      REPORTS: {
        TB: "reports/trailbalance",
        PL: "reports/profitloss",
        LEDGER: "reports/ledger",
        BALANCESHEET: "reports/balancesheet",
        INVOICEREG: "reports/invoice/list",
        BILLREG: "reports/bills/list ",
        PAYRECLIST: "reports/paymentreceipt/list"
      },
      BOOKOFACCOUNTS: {
        LIST: "bookofaccount/list",
        CREATE: "bookofaccount/create",
        UPDATE: "bookofaccount/update"
      },
    },
    SITECONFIG: {
      SUNDRY_C: {
        "accheadid": "9eb16ae2cda75e7f336a6e6220161227",
        "accheadname": "Sundry Creditors"
      },
      SUNDRY_D: {
        "accheadid": "620985adccf07463ca18b80320161227",
        "accheadname": "Sundry Debtors",
      }
    },
    CHART_SERVICE: {
      TOPCUSTOMERBYSELES: {
        GETTOPCUSTOMERBYSELES: "reports/getTopCustomersBySales"
      },
      CREDITSDEBITS: {
        GETCREDITSDEBITS: "reports/getTotalCreditsandDebits"
      },
      TAXSUMMARYREPORT: {
        GETTAXSUMMARYREPORT: "reports/getTaxSummaryReport "
      },
      INVOICELIST: {
        GETINVOICELIST: "reports/getInvoiceDuesReport"
      },
      BILLLIST: {
        GETBILLLIST: "reports/getOpenDueBillsReport"
      }
    },
    CHAT_CONFIG: {
      LIMIT: 6,
      LIST_PAGE_LIMIT: 10
    }
  }
});
