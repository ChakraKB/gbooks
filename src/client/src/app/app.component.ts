import { Component, AfterViewInit, ElementRef, Renderer, ViewChild, OnInit } from '@angular/core';
import { LocalStorageService } from './shared/local-storage.service';
import { MessagesService } from './shared/messages.service';
import { AppConstant } from './app.constant'
import { FeaturesService } from './services/features.service';
import { Message } from 'primeng/primeng';
import * as $ from "jquery";

enum MenuMode {
    STATIC,
    OVERLAY,
    SLIM
};

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnInit {

    userLoggedIn = false;

    menu: MenuMode = MenuMode.STATIC;

    layout: string = 'default';

    darkMenu: boolean;

    documentClickListener: Function;

    staticMenuInactive: boolean;

    overlayMenuActive: boolean;

    mobileMenuActive: boolean;

    menuClick: boolean;

    menuButtonClick: boolean;

    topbarMenuButtonClick: boolean;

    AppsMenuButtonClick: boolean;

    topbarMenuClick: boolean;

    AppsMenuClick: boolean;

    topbarMenuActive: boolean;

    AppsMenuActive: boolean;

    activeTopbarItem: Element;

    resetSlim: boolean;

    messages: Message[] = [];

    successMessage: Message[] = [];

    clearSuccessMsg: any;

    constructor(
        public renderer: Renderer,
        public localStorage: LocalStorageService,
        public messageService: MessagesService,
        private featureservice: FeaturesService) {

    }

    ngOnInit() {
        this.subscribeMessages();
        this.subscribeSuccessMessage();
    };



    ngAfterViewInit() {
        this.documentClickListener = this.renderer.listenGlobal('body', 'click', (event) => {
            if (!this.menuClick && !this.menuButtonClick) {
                this.mobileMenuActive = false;
                this.overlayMenuActive = false;
                this.resetSlim = true;
            }

            if (!this.topbarMenuClick && !this.topbarMenuButtonClick) {
                this.topbarMenuActive = false;
            }

            this.menuClick = false;
            this.menuButtonClick = false;
            this.topbarMenuClick = false;
            this.topbarMenuButtonClick = false;
        });
    }


    subscribeMessages() {
        this.messageService.getMessages().subscribe((message) => {
            if (!message) {
                // clear alerts when an empty alert is received
                this.messages = [];
                return;
            }
            this.messages.push(message);
        });
    }

    subscribeSuccessMessage() {
        this.messageService.getSuccessMessage().subscribe((message) => {
            this.successMessage = [];
            this.successMessage.push(message);
            if (this.clearSuccessMsg) {
                clearTimeout(this.clearSuccessMsg);
            }

            this.clearSuccessMsg = setTimeout(() => {
                this.successMessage = [];
            }, 2500)
        });
    }

    onMenuButtonClick(event: Event) {
        this.menuButtonClick = true;

        if (this.isMobile()) {
            this.mobileMenuActive = !this.mobileMenuActive;
        }
        else {
            if (this.staticMenu)
                this.staticMenuInactive = !this.staticMenuInactive;
            else if (this.overlayMenu)
                this.overlayMenuActive = !this.overlayMenuActive;
        }

        event.preventDefault();
    }

    onTopbarMenuButtonClick(event: Event) {
        this.topbarMenuButtonClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;
        event.preventDefault();
    }
    onAppsMenuButtonClick(event: Event) {
        this.AppsMenuButtonClick = true;
        this.AppsMenuActive = !this.AppsMenuActive;
        event.preventDefault();
    }

    onTopbarItemClick(event: Event, item: Element) {
        if (this.activeTopbarItem === item)
            this.activeTopbarItem = null;
        else
            this.activeTopbarItem = item;

        event.preventDefault();
    }

    onTopbarMenuClick(event: Event) {
        this.topbarMenuClick = true;
    }

    onAppsMenuClick(event: Event) {
        this.AppsMenuClick = true;
    }

    onMenuClick(event: Event) {
        this.menuClick = true;
        this.resetSlim = false;
    }

    get slimMenu(): boolean {
        return this.menu === MenuMode.SLIM;
    }

    get overlayMenu(): boolean {
        return this.menu === MenuMode.OVERLAY;
    }

    get staticMenu(): boolean {
        return this.menu === MenuMode.STATIC;
    }

    changeToSlimMenu() {
        this.menu = MenuMode.SLIM;
    }

    changeToOverlayMenu() {
        this.menu = MenuMode.OVERLAY;
    }

    changeToStaticMenu() {
        this.menu = MenuMode.STATIC;
    }

    isMobile() {
        return window.innerWidth < 640;
    }

    logout() {
        let token: any = this.localStorage.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.TOKEN);
        setTimeout(() => {
            this.featureservice.LogOut(token.token).then((res) => {
                if (res.status) {
                    this.localStorage.clearAllItem();
                    window.location.href = AppConstant.ACCOUNT.ACC_URL + "signin.html";
                }
            })
        });

    }
}