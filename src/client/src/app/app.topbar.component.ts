import { Component, Inject, forwardRef } from '@angular/core';
import { AppComponent } from './app.component';
import { LocalStorageService } from './shared/local-storage.service';
import { AppConstant } from './app.constant';
@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBar {
    public userstoragedata: any = {};
    tenantfullname: any = "";
    AppcenterList = [];
    imagesrcpath = "assets/layout/images/appcenter/";
    constructor( @Inject(forwardRef(() => AppComponent)) public app: AppComponent,
        private storageservice: LocalStorageService, ) {
        this.AppcenterList = [
            { title: "eFinance", description: "", status: true, modulepath: "EFINANCE", image_enabled: this.imagesrcpath + "e-finance.png" },
            { title: "eCatalogue", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "e-catalogue.png" },
            { title: "Customer Connect", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "customer-connect.png" },
            { title: "People Connect", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "people-connect.png" },
            { title: "eFile", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "e-file.png" },
            { title: "eKart", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "e-kart.png" },
            { title: "eStore", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "e-store.png" },
            { title: "iProcure", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "i-procure.png" },
            { title: "iRetail", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "i-retail.png" },
            { title: "mPos", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "m-pos.png" },
            { title: "Settings", description: "", status: true, modulepath: "SETTINGS", image_enabled: this.imagesrcpath + "settings.png" },
            { title: "App-Center", description: "", status: true, modulepath: "APPCENTER", image_enabled: this.imagesrcpath + "app-center.png" },
            // { title: "mPos", description: "", status: false, modulepath: "", image_enabled: this.imagesrcpath + "app-icon-10.png", image_disabled: this.imagesrcpath + "m-pos.png" },
        ];
        this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
        this.tenantfullname = this.userstoragedata ? this.userstoragedata.fullname : null;
        //this.tenantfullname = this.userstoragedata.fullname;
        this.subscribeUserSessionData();
    }

    navigateToApp(app: any) {
        var url = "";
        if (app.status) {
            if (app.modulepath == "APPCENTER") {
                url = AppConstant.ACCOUNT.ACC_URL;
            }
            else if (app.modulepath == "SETTINGS") {
                url = AppConstant.ACCOUNT.ACC_URL + "#/appcenter";
            }
            window.location.href = url;
        }
    }
    setLogin() {
        this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
        this.tenantfullname = this.userstoragedata ? this.userstoragedata.fullname : null;
    }

    subscribeUserSessionData() {
        this.storageservice.getUserSessionData().subscribe((userData) => {
            this.userstoragedata = this.storageservice.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
            this.tenantfullname = this.userstoragedata ? this.userstoragedata.fullname : null;
        });
    }
}