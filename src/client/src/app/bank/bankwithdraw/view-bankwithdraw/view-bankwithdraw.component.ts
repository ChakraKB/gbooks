import { Component, OnInit,Input } from '@angular/core';
import {JournalsService } from '../../../accounts/service/journals.service';
import{AppConstant}from '../../../app.constant'

@Component({
  selector: 'app-view-bankwithdraw',
  templateUrl: './view-bankwithdraw.component.html',
  styleUrls: ['./view-bankwithdraw.component.scss']
})
export class ViewBankwithdrawComponent implements OnInit {

  withdrawheader:any;
  withdrawdetails:any
  @Input() journalid: number;
  datafromat:string;
  
  constructor( private JournalsService:JournalsService, ) { 
    this.datafromat= AppConstant.API_CONFIG.DATE.displayFormat; 
  }

  ngOnInit() {
   this. getjournalView();
  }
  getjournalView(){
  this.JournalsService.getJournalDetails(this.journalid).then((res)=>{
    this.withdrawheader=res.data[0];
    this.withdrawdetails=res.data[0].ledgers

  })
  }
}
