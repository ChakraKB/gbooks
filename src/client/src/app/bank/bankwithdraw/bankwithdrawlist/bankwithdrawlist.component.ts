import { Component, OnInit, Renderer, ElementRef, Input, Output} from '@angular/core';
import { BanksService } from './../../service/banks.service';
import { LocalStorageService } from '../../../shared/local-storage.service';
import { AppConstant } from '../../../app.constant';
import {JournalsService } from '../../../accounts/service/journals.service';
import { UtilsService } from '../../../services/utils.service';
import * as _ from "lodash";
@Component({
  selector: 'app-bankwithdrawlist',
  templateUrl: './bankwithdrawlist.component.html',
  styleUrls: ['./bankwithdrawlist.component.scss']
})
export class BankwithdrawlistComponent implements OnInit {
  data: any;
  userdetails: any
  allbankwithdraw: any;
  adddeposit: boolean = false;
  addnew: boolean = false;
  list: boolean = true;
  editdeposit: boolean = false;
  editpro: any;
  withdrawls: Array<any> = []
  selecteddeps:Array<any>=[];
  activeTab:String="-";
  viewdeposit:boolean=false;
  finyear:any;
  datafromat:string;
  activetabIndex:number=0; 
  currency_Symbol:string;
  show:boolean=false;
  

  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  constructor(private BanksService: BanksService,
     private LocalStorageService: LocalStorageService,
    private JournalsService:JournalsService,
    private UtilsService: UtilsService) {
    this.userdetails = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.datafromat= AppConstant.API_CONFIG.DATE.displayFormat;
    this.currency_Symbol=AppConstant.API_CONFIG.CURRENCY_FORMAT;
  }

  ngOnInit() {

    this.getBankWithdrawList();
  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }
  getBankWithdrawList() {
    let data = {
      "tenantid": this.userdetails.tenantid,
      "type": "BANKWITHDRAW",
      "finyear":this.finyear.finyear,
      // "limit":100,
      // "offset":11
    }
    this.JournalsService.getAllJournals(data).then((res) => {
      if (res.status == true) {
        this.allbankwithdraw = res.data;
      }
    });
  };

  addTabViewwithdraw(item,addTabViewwithdraw) {
    this.adddeposit = false;
    this.editdeposit = false;
    this.addnew = false;
    this.editpro = false;
    this.viewdeposit=false;
    this.list = false;
    this.UtilsService.activate_multitab(this.selecteddeps, item, addTabViewwithdraw,"journalno");
  }
  notifyNewBankWithDraw(even) {
    this.getBankWithdrawList();
    this.adddeposit = false;
    this.editdeposit = false;
    this.addnew = false;
    this.editpro = false;
    this.viewdeposit=false;
    this.list = true;
  };

  AddDeposite(bwviewtab) {
    this.adddeposit = true;
    this.editdeposit = false;
    this.addnew = true;
    this.editpro = false;
    this.list = false;
   this. viewdeposit=false;
    this.withdrawls = [];
    this.selecteddeps=[];
    bwviewtab.activeIndex=1;
    // this.UtilsService.active_addAndeditTab(bwviewtab,"Add Withdraw");
  };

  handleClose(event,closetab) {
    this.adddeposit = false;
    this.editdeposit = false;
    this.list = true;
    this.UtilsService.deactivate_multitab(this.selecteddeps,event,closetab,"journalno");
  };

  viewDeposite(withdrawls,bwviewtab) {
    this.list = false;
    this.adddeposit = false;
    this.editdeposit = true;
    this.addnew = false;
    this.editpro = true;
    this.withdrawls = withdrawls;
    this.viewdeposit=false;
    this.selecteddeps=[];
    bwviewtab.activeIndex=1;
  // this.UtilsService.active_addAndeditTab(bwviewtab,"Edit Withdraw");
  };
}
