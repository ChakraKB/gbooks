import { Component, OnInit, EventEmitter, Output, Input,OnChanges } from '@angular/core';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import { DateformatPipe } from '../../pipes/dateformat.pipe';
import { MessagesService } from '../../shared/messages.service';
import {JournalsService } from '../../accounts/service/journals.service';
import{UtilsService} from '../../services/utils.service';

import * as _ from "lodash";
import * as moment from "moment";
@Component({
  selector: 'app-bankwithdraw',
  templateUrl: './bankwithdraw.component.html',
  styleUrls: ['./bankwithdraw.component.scss']
})
export class BankwithdrawComponent implements OnInit,OnChanges {
  public amtlength = AppConstant.API_CONFIG.AMOUNTFORMAT.MAXLENGTH;
  userdetails: any;
  data: any;
  seq: any;
  seqid: any;
  selectedfrom: any;
  selectedfromdetails: any;
  selectedto: any;
  selectedtodetails: any;
  fromlist: Array<any> = [];
  tolist: Array<any> = [];
  filterformlist: Array<any> = [];
  filtertolist: Array<any> = [];
  journals: Array<any> = [];
  ledgers: Array<any> = [];
  validation = true;
  validationmsg = "";
  bankfind: any;
  fromdata: any;
  finyear:any;

  bank: FormGroup;
  amount: FormControl;
  DocmentNo: FormControl;
  Docmentdata: FormControl;

  @Input() withdrawls: any;
  @Output() notifyNewBankWithDraw: EventEmitter<any> = new EventEmitter();


  constructor(
    private fb: FormBuilder,
    private LocalStorageService: LocalStorageService,
    private MessagesService: MessagesService,
    private dateFormatPipeFilter: DateformatPipe,
  private  JournalsService:JournalsService,
  private UtilsService:UtilsService ) {
    this.bank = fb.group({
      'amount': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15)])],
      'Docmentdata': [new Date, Validators.required],
      'selecteduom': [null, Validators.required],
      'selectedfrom': [null, Validators.required],
      'selectedto': [null, Validators.required],
      'DocmentNo': [, Validators.required],
      'Remark': [null,],
    });
    this.userdetails = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear=this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
  }
  callParent() {
    this.notifyNewBankWithDraw.next();
  }
  numberOnly(event){
    this.UtilsService.allowNumberOnly(event);
  }

  ngOnInit() {

    this.data = {
      "tenantid":this.userdetails.tenantid,
      "refkey": "WTDR"
    }
    this.Journalconfig(this.data);
    // if (!this.withdrawls.journalid) {
    //   setTimeout(() => {
    //     this.bank.controls["DocmentNo"].setValue(this.seq);
    //     this.bank.controls["DocmentNo"].disable();
    //   }, 1000);
    // }
    if (this.withdrawls.journalid) {
      setTimeout(() => {
        this.viewBankDeposit()
      },0);
    }
  }
  ngOnChanges(){
    if (this.withdrawls.journalid) {
      setTimeout(() => {
        this.viewBankDeposit()
      },0);
    }
  }
  Journalconfig(data) {
    this.JournalsService.config(this.data).then((res) => {
      this.fromlist = res.data.bank;
      this.tolist = res.data.cash;
    })
  };
  handleDropdownClick(event, label) {
    if (label == "from") {
      this.filterformlist = [];
      setTimeout(() => {
        this.filterformlist = this.fromlist;
      }, 100)
    } else if (label == "to") {
      this.filtertolist = [];
      setTimeout(() => {
        this.filtertolist = this.tolist;
      }, 100);
    }
  }

  searchfrom(event) {
    this.filterformlist = [];
    this.filterformlist = _.filter(this.fromlist, function (res) {
      var fromdata = res.subaccheadname;
      return (fromdata.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
    });
  }
  searchto(event) {
    this.filtertolist = [];
    this.filtertolist = _.filter(this.tolist, function (res) {
      var todata = res.subaccheadname;
      return (todata.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
    })
  }
  selectfrom(item) {
    this.selectedfrom = item.subaccheadname;
    this.selectedfromdetails = item;
  }
  selectto(item) {
    this.selectedto = item.subaccheadname;
    this.selectedtodetails = item;
  }
  clearform() {
    this.bank.reset();
  }

  viewBankDeposit() {
    this.JournalsService.getJournalDetails(this.withdrawls.journalid).then((res) => {
      this.bankfind = res.data[0];
      var from = this.bankfind.ledgers[0];
      this.selectedfromdetails = _.find(this.fromlist, function (res) {
        return res.subaccheadid == from.accheadid;
      });
      this.selectfrom(this.selectedfromdetails);
      var to = this.bankfind.ledgers[1];
      this.selectedtodetails = _.find(this.tolist, function (res) {
        return res.subaccheadid == to.accheadid;
      });
      this.selectto(this.selectedtodetails);
      this.ledgers = this.bankfind.ledgers;
      //2017-09-13T00:00:00.000Z
      let momentdate = moment(this.bankfind.journaldt, "YYYY-MM-DD'T'HH:mm:ss.SSS'Z'");
      this.bank = new FormGroup({
        'amount': new FormControl(this.bankfind.journaltotal),
        'Docmentdata': new FormControl(momentdate.toDate()),
        'selectedfrom': new FormControl(this.selectedfromdetails),
        'selectedto': new FormControl(this.selectedtodetails),
        'DocmentNo': new FormControl(this.bankfind.journalno),
        'Remark': new FormControl(this.bankfind.remarks)
      });
    });
  }


  createbank(data) {                                  
    var validation = true;
    var validationmsg = "";
    var docDate = this.dateFormatPipeFilter.transform(data.Docmentdata, 'y-MM-dd');
    var crdata = this.dateFormatPipeFilter.transform(new Date(), 'y-MM-dd');
    if(this.selectedfromdetails==undefined){
      validation = false;
      validationmsg = "Please choose from the dropDwon menu "
    }
    if(_.isEmpty(docDate)){
      validation = false;
      validationmsg = "Please select date "
    }
   else if (_.isEmpty(data.selectedfrom)) {
      validation = false;
      validationmsg = "Please select account "
    } else if (_.isEmpty(data.selectedto)) {
      validation = false;
      validationmsg = "Please Select Bank"
    } else if (_.isEmpty(data.amount) || data.amount=="0" || data.amount=="0.00"  ) {
      validation = false;
      validationmsg = "Please enter amount"
    }
      if (validation == true) {
        var docmentno = this.bank.controls["DocmentNo"].value;

        if (this.bankfind) {
          this.ledgers[0].accheadid = this.selectedfromdetails.subaccheadid;
          this.ledgers[0].accheadname = this.selectedfromdetails.subaccheadname;
          this.ledgers[0].leadaccheadid = this.selectedfromdetails.accheadid;
          this.ledgers[0].leadaccheadname = this.selectedfromdetails.accheadname;
          this.ledgers[0].lastupdatedby =this.userdetails.loginname;
          this.ledgers[0].lastupdateddt = crdata;
          this.ledgers[0].cramount = data.amount;

          this.ledgers[1].accheadid = this.selectedtodetails.subaccheadid;
          this.ledgers[1].accheadname = this.selectedtodetails.subaccheadname;
          this.ledgers[1].leadaccheadid = this.selectedtodetails.accheadid;
          this.ledgers[1].leadaccheadname = this.selectedtodetails.accheadname;
          this.ledgers[1].lastupdatedby =this.userdetails.loginname;
          this.ledgers[1].lastupdateddt = crdata;
          this.ledgers[1].dramount = data.amount;
          this.fromdata = {
            "header":{
            "journalid": this.bankfind.journalid,
            "type": "BANKWITHDRAW",
            "refkey": "WTDR",
            "journaldt": docDate,
            "finyear": this.finyear.finyear,
            "tenantid": this.userdetails.tenantid,
            "tenantname": this.userdetails.tenantname,
            "remarks": data.Remark,
            "ccyid": "1",
            "ccyname": "INR",
            "subtotal": data.amount,
            "taxtotal": "0",
            "roundoff": "0",
            "journaltotal": data.amount,
            "pymntamount": "0",
            "balamount": data.amount,
            "pymtlink": "http://link.in",
            "status": "Active",
            },
            "detailsForUpdate":{
              "delete": [],
              "update": this.ledgers,
              "insert": [],
            },
          }

          this.JournalsService.updateJournal(this.fromdata)
            .then((res) => {
              this.MessagesService.showSuccessMessage({ severity: 'success', summary: 'Success', detail: res.message })
              this.callParent();
              this.clearform();
            });
        }


        else {
          this.journals = [
            {
              "remarks": "",
              "cramount": data.amount,
              "contactdetails": "",
              "feature": "Journal",
              "type": "From",
              "crdr": "C",
              "accheadid": this.selectedfromdetails.subaccheadid,
              "accheadname": this.selectedfromdetails.subaccheadname,
              "parentaccheadid": this.selectedfromdetails.accheadid,
              "parentaccheadname": this.selectedfromdetails.accheadname,
              "leadaccheadid": this.selectedfromdetails.accheadid,
              "leadaccheadname": this.selectedfromdetails.accheadname
            },
            {
              "remarks": "",
              "dramount": data.amount,
              "contactdetails": "",
              "feature": "Journal",
              "type": "To",
              "crdr": "D",
              "accheadid": this.selectedtodetails.subaccheadid,
              "accheadname": this.selectedtodetails.subaccheadname,
              "parentaccheadid": this.selectedtodetails.accheadid,
              "parentaccheadname": this.selectedtodetails.accheadname,
              "leadaccheadid": this.selectedtodetails.accheadid,
              "leadaccheadname": this.selectedtodetails.accheadname
            }];
          this.fromdata = {
            "header":{
            "type": "BANKWITHDRAW",
            "refkey": "WTDR",
            "journaldt": docDate,
            "finyear":this.finyear.finyear,
            "tenantid": this.userdetails.tenantid,
            "tenantname": this.userdetails.tenantname,
            "remarks": data.Remark,
            "duedate": "01/30/2017",
            "ccyid": "1",
            "ccyname": "INR",
            "subtotal": data.amount,
            "taxtotal": "0",
            "roundoff": "0",
            "journaltotal": data.amount,
            "pymntamount": "0",
            "balamount": data.amount,
            "pymtlink": "http://link.in",
            "templtid": "1",
            "templtname": "HTL",
            "emailyn": "Y",
            "status": "Active",
            "createdby": this.userdetails.loginname,
            "createddt": crdata,
            },
            "journaldetails": this.journals,
            "ledgers": this.journals
          }
          this.JournalsService.saveJournal(this.fromdata)
            .then((res) => {
              if (res.status == true) {
                this.MessagesService.showSuccessMessage({ severity: 'success', summary: 'Success', detail: res.message})
                this.callParent();
                this.clearform();
              } else if (res.status == false) {
                this.MessagesService.showMessage({ severity: 'error', summary: 'Error Message', detail: "Withdarw save failed!" })
              }
            });
        }
      }
      else {
        this.MessagesService.showMessage({ severity: 'error', summary: 'Error Message', detail: validationmsg })
      }
    }


  }
