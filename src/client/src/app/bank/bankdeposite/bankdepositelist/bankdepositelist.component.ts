import { Component, OnInit, Renderer, ElementRef, Input, Output } from '@angular/core';
import { BanksService } from './../../service/banks.service';
import { LocalStorageService } from '../../../shared/local-storage.service';
import { AppConstant } from '../../../app.constant';
import { JournalsService } from '../../../accounts/service/journals.service';
import { UtilsService } from '../../../services/utils.service'
import * as _ from "lodash";


@Component({
  selector: 'app-bankdepositelist',
  templateUrl: './bankdepositelist.component.html',
  styleUrls: ['./bankdepositelist.component.scss']
})
export class BankdepositelistComponent implements OnInit {
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  data: any;
  userdetails: any
  allbankdeposit: any;
  adddeposit: boolean = false;
  addnew: boolean = false;
  list: boolean = true;
  editdeposit: boolean = false;
  editpro: any;
  activeTab = "-";
  depositls: Array<any> = [];
  selecteddeps: Array<any> = [];
  finyear: any;
  datafromat: string;
  activetabindex: number = 0;
  currency_Symbol: string;
  show:boolean=false;
  constructor(private BanksService: BanksService, private LocalStorageService: LocalStorageService,
    private JournalsService: JournalsService, private UtilsService: UtilsService) {
    this.userdetails = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.datafromat = AppConstant.API_CONFIG.DATE.displayFormat;
    this.currency_Symbol = AppConstant.API_CONFIG.CURRENCY_FORMAT;

  }

  ngOnInit() {
    this.data = {
      "tenantid": this.userdetails.tenantid,
      "type": "BANKDEPOSIT",
      "finyear": this.finyear.finyear,
      // "limit":10,
      // "offset":11
    }
    this.getBankDepositeList(this.data);
  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }

  getBankDepositeList(data) {
    this.JournalsService.getAllJournals(data).then((res) => {
      if (res.status == true) {
        this.allbankdeposit = res.data;
      }
    });
  };

  addTabViewdeposite(item, addTabViewdeposite) {
    this.adddeposit = false;
    this.editdeposit = false;
    this.addnew = false;
    this.editpro = false;
    this.list = false;
    this.UtilsService.activate_multitab(this.selecteddeps, item, addTabViewdeposite, "journalno");
  }


  notifyNewBankDeposit(even) {
    this.getBankDepositeList(this.data);
    this.adddeposit = false;
    this.editdeposit = false;
    this.addnew = false;
    this.editpro = false;
    this.list = true;
  };

  AddDeposite(bdtabview) {
    this.selecteddeps=[];
    this.adddeposit = true;
    this.editdeposit = false;
    this.addnew = true;
    this.editpro = false;
    this.list = false;
    this.depositls = [];
     this.UtilsService.active_addAndeditTab(bdtabview,"Add deposit")
  };

  handleClose(e, closetab) {
    this.adddeposit = false;
    this.editdeposit = false;
    this.list = true;
    this.UtilsService.deactivate_multitab(this.selecteddeps, event, closetab, "journalno");
  };

  viewDeposite(depositls, bdtabview) {
    this.selecteddeps=[];
    this.list = false;
    this.adddeposit = false;
    this.editdeposit = true;
    this.addnew = false;
    this.editpro = true;
    this.depositls = depositls;
     this.UtilsService.active_addAndeditTab(bdtabview,"Edit deposit")
  };
}
