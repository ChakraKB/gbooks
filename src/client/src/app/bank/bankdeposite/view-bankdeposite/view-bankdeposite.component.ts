import { Component, OnInit,Input } from '@angular/core';
import {JournalsService } from '../../../accounts/service/journals.service';
import{AppConstant}from '../../../app.constant'

@Component({
  selector: 'app-view-bankdeposite',
  templateUrl: './view-bankdeposite.component.html',
  styleUrls: ['./view-bankdeposite.component.scss']
})
export class ViewBankdepositeComponent implements OnInit {

  depositeheader:any;
  depositedetails:any
  @Input() journalid: number;
  datafromat:string;
  
  constructor(
  private  JournalsService:JournalsService,
  
 ) { 

  this.datafromat= AppConstant.API_CONFIG.DATE.displayFormat; 
  }

  ngOnInit() {
   this. getjournalView();
  }
  getjournalView(){
  this.JournalsService.getJournalDetails(this.journalid).then((res)=>{
    this.depositeheader=res.data[0];
    this.depositedetails=res.data[0].ledgers
  })
  }
}
