import { Component, OnInit,Input } from '@angular/core';
import {JournalsService } from '../../../accounts/service/journals.service';
import{AppConstant}from '../../../app.constant'

@Component({
  selector: 'app-view-banktransfer',
  templateUrl: './view-banktransfer.component.html',
  styleUrls: ['./view-banktransfer.component.scss']
})
export class ViewBanktransferComponent implements OnInit {


  transferheader:any;
  transferdetails:any
  @Input() journalid: number;
  datafromat:string;
  
  constructor(
  private JournalsService:JournalsService, ) { 
    
    this.datafromat= AppConstant.API_CONFIG.DATE.displayFormat; 
  }

  ngOnInit() {
   this. getjournalView();
  }
  getjournalView(){
  this.JournalsService.getJournalDetails(this.journalid).then((res)=>{
    this.transferheader=res.data[0];
    this.transferdetails=res.data[0].ledgers
  })
  }
}
