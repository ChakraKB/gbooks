import { Component, OnInit, Renderer, ElementRef, Input, Output } from '@angular/core';
import { BanksService } from './../../service/banks.service';
import { LocalStorageService } from '../../../shared/local-storage.service';
import { AppConstant } from '../../../app.constant';
import { JournalsService } from '../../../accounts/service/journals.service';
import { UtilsService } from '../../../services/utils.service'
import * as _ from "lodash";
@Component({
  selector: 'app-banktransferlist',
  templateUrl: './banktransferlist.component.html',
  styleUrls: ['./banktransferlist.component.scss']
})
export class BanktransferlistComponent implements OnInit {
  data: any;
  userdetails: any
  allbanktransfer: any;
  addtrf: boolean = false;
  addnew: boolean = false;
  list: boolean = true;
  edittrf: boolean = false;
  editpro: any;
  tranfls: Array<any> = []
  activeTab: string = '-'
  selectedtrf: Array<any> = [];
  finyear: any;
  datafromat: string;
  activetabindex: number = 0
  currency_Symbol:string;
  show:boolean=false;
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  constructor(private BanksService: BanksService, private LocalStorageService: LocalStorageService,
    private JournalsService: JournalsService, private UtilsService: UtilsService
  ) {
    this.userdetails = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
    this.datafromat = AppConstant.API_CONFIG.DATE.displayFormat;
    this.currency_Symbol=AppConstant.API_CONFIG.CURRENCY_FORMAT;
  }

  ngOnInit() {
    var callback = this;
    this.data = {
      "tenantid": this.userdetails.tenantid,
      "type": "BANKTRANSFER",
      "finyear": this.finyear.finyear,
      // "limit":10,
      // "offset":11
    }
    this.getBankTransferList(this.data)
  }
  showhidefilter() {
    if (this.show == true) {
      this.show = false
    }
    else {
      this.show = true;
    }
  }
  getBankTransferList(data) {
    this.JournalsService.getAllJournals(data).then((res) => {
      if (res.status == true) {
        this.allbanktransfer = res.data;
      }
    });
  };

  addTabViewtransfer(item,addTabViewtransfer) {
    this.addtrf = false;
    this.edittrf = false;
    this.addnew = false;
    this.editpro = false;
    this.list = false;
    this.UtilsService.activate_multitab(this.selectedtrf, item, addTabViewtransfer,"journalno");
  }

  notifyNewBankTransfer(even) {
    this.getBankTransferList(this.data);
    this.addtrf = false;
    this.edittrf = false;
    this.addnew = false;
    this.editpro = false;
    this.list = true;
  };

  AddTransfer(btview) {
    this.addtrf = true;
    this.edittrf = false;
    this.addnew = true;
    this.editpro = false;
    this.list = false;
    this.tranfls = [];
    this.selectedtrf=[];
    btview.activeIndex=1;
    // this.UtilsService.active_addAndeditTab(btview,"Add transfer");
  };

  handleClose(event,closetab) {
    this.addtrf = false;
    this.edittrf = false;
    this.list = true;
    this.UtilsService.deactivate_multitab(this.selectedtrf,event,closetab,"journalno");
  };
  viewtransfer(tranfls,btview) {
    this.list = false;
    this.addtrf = false;
    this.edittrf = true;
    this.addnew = false;
    this.editpro = true;
    this.tranfls = tranfls;
    this.selectedtrf=[];
    btview.activeIndex=1;
  // this.UtilsService.active_addAndeditTab(btview,"Edit transfer");
  };
}
