import { Component, OnInit, Input, Output, EventEmitter,OnChanges } from '@angular/core';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { BanksService } from './../service/banks.service';
import { LocalStorageService } from '../../shared/local-storage.service';
import { AppConstant } from '../../app.constant';
import { MessagesService } from '../../shared/messages.service';
import{MasterService } from '../../services/master.service';
import { DateformatPipe } from '../../pipes/dateformat.pipe';
import {JournalsService } from '../../accounts/service/journals.service';
import{UtilsService} from '../../services/utils.service'
import * as _ from "lodash";
import * as moment from "moment";
@Component({
  selector: 'app-banktransfer',
  templateUrl: './banktransfer.component.html',
  styleUrls: ['./banktransfer.component.scss']
})
export class BanktransferComponent implements OnInit,OnChanges {
  public amtlength = AppConstant.API_CONFIG.AMOUNTFORMAT.MAXLENGTH;
  userdetails: any;
  data: any;
  seq: any;
  seqid: any;
  selectedfrom: any;
  selectedfromdetails: any;
  selectedto: any;
  selectedtodetails: any;
  fromlist: Array<any> = [];
  tolist: Array<any> = [];
  filterformlist: Array<any> = [];
  filtertolist: Array<any> = [];
  journals: Array<any> = [];
  journalDetails:Array<any>=[];
  ledger: Array<any>=[];
  bankfind: any;
  fromdata: any;
  BankList: SelectItem[];
  fdata:any;
  finyear:any;
  @Input() tranfls: any;
  @Output() notifyNewBankTransfer: EventEmitter<any> = new EventEmitter();
  bank: FormGroup;
  amount: FormControl;
  // DocmentNo: FormControl;
  Docmentdata: FormControl;
  constructor(private BanksService: BanksService,
     private fb: FormBuilder, 
     private LocalStorageService: LocalStorageService,
    private masterService:MasterService,
  private MessagesService:MessagesService,
  private dateFormatPipeFilter :DateformatPipe,
private JournalsService:JournalsService,
private UtilsService:UtilsService) {
  
      this.bank = fb.group({
        'amount': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15)])],
        'Docmentdata': [new Date, Validators.required],
        'selecteduom': [null, Validators.required],
        'selectedfrom': [null, Validators.required],
        'selectedto': [null, Validators.required],
        'DocmentNo': [, Validators.required],
        'Remark': [null,],
      });
    this.userdetails = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.finyear=this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.FINYEAR);
  }
  callParent() {
    this.notifyNewBankTransfer.next();
  }

  ngOnInit() {
   
  this.data = {
      "tenantid": this.userdetails.tenantid,
      "refkey": "TRNS"
    }
    this.Journalsconfig(this.data);
    // if(!this.tranfls.journalid){
    //   setTimeout(() => {
    //     this.bank.controls["DocmentNo"].setValue(this.seq);
    //     this.bank.controls["DocmentNo"].disable();
    //     this.bank.controls["DocmentNo"].value;
    //   }, 1000);
    // }
 
    if(this.tranfls.journalid){
      setTimeout(() => {
       this.viewBankDeposit()
      }, 0);
     }
  }
  ngOnChanges(){
    if(this.tranfls.journalid){
      setTimeout(() => {
       this.viewBankDeposit()
      }, 0);
     }
  }

numberOnly(event){
  this.UtilsService.allowNumberOnly(event);
}
  Journalsconfig(data){
    var callback:any=this;
    callback.BankList = [];
    this.JournalsService.config(data).then(function(res)  {
      var fdata=res.data.bank;
     callback.BankList = callback.masterService.formatDataforDropdown("subaccheadname",fdata,"Select Bank");
    })
   
  }

  
  // handleDropdownClick(event, label) {
  //   if (label == "from") {
  //     this.filterformlist = [];
  //     setTimeout(() => {
  //       this.filterformlist = this.BankList;
  //     }, 100)
  //   } else if (label == "to") {
  //     this.filtertolist = [];
  //     setTimeout(() => {
  //       this.filtertolist = this.BankList;
  //     }, 100);
  //   }
  // }

  // searchfrom(event) {
  //   this.filterformlist = [];
  //   this.filterformlist = _.filter(this.BankList, function (res) {
  //     var fromdata = res.subaccheadname;
  //     return (fromdata.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
  //   });
  // }
  // searchto(event) {
  //   this.filtertolist = [];
  //   this.filtertolist = _.filter(this.BankList, function (res) {
  //     var todata = res.subaccheadname;
  //     return (todata.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
  //   })
  // }
 
  onSelectedBankTo(){
    let item = this.bank.controls["selectedto"].value;
    this.selectedto=item.subaccheadname;
    this.selectedtodetails=item;
    if( this.selectedtodetails){
      if( this.selectedtodetails.subaccheadid ===  this.selectedfromdetails.subaccheadid ){
        this.bank.controls["selectedto"].reset();
       this.MessagesService.showMessage({severity: 'error', summary: 'Error Message', detail: "Duplicated entries not allowed."})
       
      }else if(this.selectedfromdetails.label ===  this.selectedtodetails.subaccheadname){
        this.bank.controls["selectedfrom"].reset();
        this.MessagesService.showMessage({severity: 'error', summary: 'Error Message', detail: "Duplicated entries not allowed."})

      }
    }
  };
  onSelectedBankFrom(){
    let item = this.bank.controls["selectedfrom"].value;
    this.selectedfromdetails=item;
    this.selectedfrom=item.subaccheadname;
    if( this.selectedtodetails){
      if( this.selectedfromdetails.subaccheadid ===  this.selectedtodetails.subaccheadid ){
        this.bank.controls["selectedfrom"].reset();
        this.MessagesService.showMessage({severity: 'error', summary: 'Error Message', detail: "Duplicated entries not allowed."})
      }else if(this.selectedfromdetails.subaccheadname ===  this.selectedtodetails.label){
        this.bank.controls["selectedfrom"].reset();
        this.MessagesService.showMessage({severity: 'error', summary: 'Error Message', detail: "Duplicated entries not allowed."})

      }
    }

  }
  clearform() {
    this.bank.reset();
  }

  viewBankDeposit() {
    this.JournalsService.getJournalDetails(this.tranfls.journalid).then((res) => {
      this.bankfind = res.data[0];
      var self = this;
      var from = this.bankfind.ledgers[0];
      this.selectedfromdetails = _.find(this.BankList,{label:from.accheadname});
      var to = this.bankfind.ledgers[1];
      this.selectedtodetails = _.find(this.BankList,{label:to.accheadname})
      this.ledger = this.bankfind.ledgers;
      //2017-09-13T00:00:00.000Z
      let momentdate = moment(this.bankfind.journaldt, "YYYY-MM-DD'T'HH:mm:ss.SSS'Z'");
      // this.bank.controls["DocmentNo"].disable();
      this.bank = new FormGroup({
        'amount': new FormControl(this.bankfind.journaltotal),
        'Docmentdata': new FormControl(momentdate.toDate()),
        'selectedfrom': new FormControl(this.selectedfromdetails.value),
        'selectedto': new FormControl(this.selectedtodetails.value),
        // 'DocmentNo': new FormControl(this.bankfind.journalno),
        'Remark': new FormControl(this.bankfind.remarks)
      });
    });
  }


  createbank(data) {
    var docDate= this.dateFormatPipeFilter.transform(data.Docmentdata, 'y-MM-dd');
    var crdata= this.dateFormatPipeFilter.transform(new Date(), 'y-MM-dd');
    var validation = true;
   var  validationmsg = "";
   if(this.selectedfromdetails==undefined){
    validation = false;
    validationmsg = "Please choose from the dropDwon menu "
  }
  if(_.isEmpty(docDate)){
    validation = false;
    validationmsg = "Please select date "
  }
 else if (_.isEmpty(data.selectedfrom)) {
    validation = false;
    validationmsg = "Please select account "
  } else if (_.isEmpty(data.selectedto)) {
    validation = false;
    validationmsg = "Please Select Bank"
  } else if (_.isEmpty(data.amount) || data.amount=="0" || data.amount=="0.00"  ) {
    validation = false;
    validationmsg = "Please enter amount"
  }
    if (validation == true) {
   
   
    // var docmentno = this.bank.controls["DocmentNo"].value;
    if(this.bankfind){
      

      this.ledger[0].accheadid = this.selectedfromdetails.subaccheadid;
      this.ledger[0].accheadname = this.selectedfromdetails.subaccheadname;
      this.ledger[0].leadaccheadid = this.selectedfromdetails.accheadid;
      this.ledger[0].leadaccheadname = this.selectedfromdetails.accheadname;
      this.ledger[0].lastupdatedby =this.userdetails.loginname;
      this.ledger[0].lastupdateddt = crdata;
      this.ledger[0].cramount = data.amount;

      this.ledger[1].accheadid = this.selectedtodetails.subaccheadid;
      this.ledger[1].accheadname = this.selectedtodetails.subaccheadname;
      this.ledger[1].leadaccheadid = this.selectedtodetails.accheadid;
      this.ledger[1].leadaccheadname = this.selectedtodetails.accheadname;
      this.ledger[1].lastupdatedby =this.userdetails.loginname;
      this.ledger[1].lastupdateddt = crdata;
      this.ledger[1].dramount = data.amount;
      this.fromdata = {
        "header":{
        "journalid":this.bankfind.journalid,
        "type": "BANKTRANSFER",
        "journaldt": docDate,
        "finyear":this.finyear.finyear,
        "tenantid": this.userdetails.tenantid,
        "tenantname": this.userdetails.tenantname,
        "remarks": data.Remark,
        "ccyid": "1",
        "ccyname": "INR",
        "subtotal": data.amount,
        "taxtotal": "0",
        "roundoff": "0",
        "journaltotal": data.amount,
        "pymntamount": "0",
        "balamount": data.amount,
        "pymtlink": "http://link.in",
        "templtid": "1",
        "templtname": "HTL",
        "emailyn": "Y",
        "status": "Active",
        "lastupdatedby": this.userdetails.loginname,
        "lastupdateddt": crdata,
        },
        "detailsForUpdate":{
          "delete": [],
          "update": this.ledger,
          "insert": [],
        },
      }
      
        this.JournalsService.updateJournal(this.fromdata)
          .then((res) => {
             this.MessagesService.showSuccessMessage({severity: 'success', summary: 'Success', detail:"Update With Documentid #"})
              this.callParent();
              this.clearform();
          });
    }
    else{
      this.journals = [
        {
          "remarks": "",
          "cramount": data.amount,
          "contactdetails": "",
          "feature": "Journal",
          "type": "From",
          "crdr": "C",
          "accheadid": this.selectedfromdetails.subaccheadid,
          "accheadname": this.selectedfromdetails.subaccheadname,
          "parentaccheadid": this.selectedfromdetails.accheadid,
          "parentaccheadname": this.selectedfromdetails.accheadname,
          "leadaccheadid": this.selectedfromdetails.accheadid,
          "leadaccheadname": this.selectedfromdetails.accheadname
        },
        {
          "remarks": "",
          "dramount": data.amount,
          "contactdetails": "",
          "feature": "Journal",
          "type": "To",
          "crdr": "D",
          "accheadid": this.selectedtodetails.subaccheadid,
          "accheadname": this.selectedtodetails.subaccheadname,
          "parentaccheadid": this.selectedtodetails.accheadid,
          "parentaccheadname": this.selectedtodetails.accheadname,
          "leadaccheadid": this.selectedtodetails.accheadid,
          "leadaccheadname": this.selectedtodetails.accheadname
        }];
    let fromdata: any = {
      "header":{
      "type": "BANKTRANSFER",
      "refkey": "TRNS",
      "journaldt": docDate,
      "finyear":this.finyear.finyear,
      "tenantid": this.userdetails.tenantid,
      "tenantname": this.userdetails.tenantname,
      "remarks": data.Remark,
      "duedate": "01/30/2017",
      "ccyid": "1",
      "ccyname": "INR",
      "subtotal": data.amount,
      "taxtotal": "0",
      "roundoff": "0",
      "journaltotal": data.amount,
      "pymntamount": "0",
      "balamount": data.amount,
      "pymtlink": "http://link.in",
      "templtid": "1",
      "templtname": "HTL",
      "emailyn": "Y",
      "status": "Active",
      "createdby": this.userdetails.loginname,
      "createddt":  crdata,
      },
      "journaldetails":  this.journals,
      "ledgers": this.journals
    }
    
      this.JournalsService.saveJournal(fromdata)
        .then((res) => {
          if(res.status==true){
            this.MessagesService.showSuccessMessage({severity: 'success', summary: 'Success', detail: res.message})
             this.callParent();
             this.clearform();
           }else if(res.status==false){
             this.MessagesService.showMessage({severity: 'error', summary: 'Error Message', detail:"Transfer save failed!"})
           }
        });
    }
  
    
    }
    else {
      this.MessagesService.showMessage({severity: 'error', summary: 'Error Message', detail:validationmsg})
  }
}
}
