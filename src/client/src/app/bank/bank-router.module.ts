import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BankdepositeComponent } from './bankdeposite/bankdeposite.component';
import { BankwithdrawComponent } from './bankwithdraw/bankwithdraw.component';
import { BanktransferComponent } from './banktransfer/banktransfer.component';
import { BankdepositelistComponent } from './bankdeposite/bankdepositelist/bankdepositelist.component';
import { BankwithdrawlistComponent } from './bankwithdraw/bankwithdrawlist/bankwithdrawlist.component';
import { BanktransferlistComponent } from './banktransfer/banktransferlist/banktransferlist.component';
import{BRSComponent} from './brs/brs.component';
import { BRSUploadComponent } from './brs/csvupload/brsupload.component' ;
const routes: Routes = [
  {
    path: 'deposit', 
    component: BankdepositelistComponent,   
    data: {
      title: 'Bank Deposite'
    },
  },
  {
    path: 'withdraw',
    component: BankwithdrawlistComponent,
    data: {
      title: 'Bank WithDraw'
    },
  },
  {
    path: 'transfer',
    component: BanktransferlistComponent,
    data: {
      title: 'Bank Transfer'
    },
  },
  {
    path: 'brslist',
    component: BRSComponent,
    data: {
      title: 'BRS'
    },
  },
  {
    path: 'brsupload',
    component: BRSUploadComponent,
    data: {
      title: 'BRS Upload'
    },
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class BankRouterModule { }
