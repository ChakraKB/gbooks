import { Component, OnInit, Directive,Input,Output, EventEmitter  } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray, ValidatorFn, AbstractControl }
  from '@angular/forms';
import { BanksService } from '../../service/banks.service';
import { MasterService } from '../../../services/master.service';
import { LocalStorageService } from '../../../shared/local-storage.service';
import { AppConstant } from '../../../app.constant';
import { SelectItem } from 'primeng/primeng';
import { Http, Response } from '@angular/http';
import { PapaParseService } from 'ngx-papaparse';
import { UtilsService } from "../../../services/utils.service";
import { Message } from 'primeng/primeng';
import * as _ from "lodash";
import { MessagesService } from '../../../shared/messages.service';
import * as moment from "moment";
import { DateformatPipe } from '../../../pipes/dateformat.pipe';
@Component({
  selector: 'app-brsmatching',
  templateUrl: './brsmatching.component.html',
  styleUrls: ['./brsmatching.component.scss']
})

export class BRSMatchingComponent implements OnInit {
  @Output()
  brsUpdateevent: EventEmitter<object> = new EventEmitter<object>();
  private currency_sy = AppConstant.API_CONFIG.CURRENCY_FORMAT;
  private date_dformat = AppConstant.API_CONFIG.ANG_DATE.displayFormat;
  private date_apiformat = AppConstant.API_CONFIG.ANG_DATE.apiFormat;
  private userdetails: any;
  fromtodate: Date[] = [];
  currentdate: any;
  BankList: SelectItem[];
  brslist: any = [];
  brstypeList = [
    { label: 'Unreconciliated', value: "U" },
    { label: 'Reconciliated', value: "R" }
  ];
  constructor(private BanksService: BanksService,
    private LocalStorageService: LocalStorageService,
    private masterService: MasterService,
    private PapaParseService: PapaParseService, private http: Http, private utilservice: UtilsService,
    private messageservice: MessagesService, private dateFormatPipeFilter: DateformatPipe) {
    this.userdetails = this.LocalStorageService.getItem(AppConstant.API_CONFIG.LOCALSTORAGE.USER);
    this.currentdate = this.dateFormatPipeFilter.transform(new Date(), this.date_apiformat)
    // this.getAllBankList();
    var fromdate = new Date();
    var todate = new Date();
    this.fromtodate = [
      fromdate,
      todate
    ];
    this.getbrslist();
  }
  onBRSTypeSelect(event, selectedbrs, ridx, brslisttable) {
    this.brslist[ridx].recostatus = event.value;
    this.brslist[ridx].reconstatus = event.value;
  }
  getbrslist() {
    var stmtfromdt = this.dateFormatPipeFilter.transform(this.fromtodate[0], this.date_apiformat);
    var stmttodt = this.dateFormatPipeFilter.transform(this.fromtodate[1], this.date_apiformat);
    var self = this;
    var data = {
      "startdate": stmtfromdt,
      "enddate": stmttodt,
      "limitsize": 1000,
      "offsetsize": 0
    };
    self.brslist = [];
    this.BanksService.brsMatchList(data)
      .then(function (res) {
        if (res.status) {
          self.brslist = res.data;
        }
      });
  }
  getAllBankList() {
    var self = this;
    this.BanksService.getAllBanks({ tenantid: this.userdetails.tenantid, limit: 1000, offset: 0 })
      .then(res => {
        if (res.status) {
          self.BankList = self.masterService.formatDataforDropdown("bankname", res.data, "Select Bank");
        }
        else {
          console.log("no bank found");
        }
      });
  }
  updatebrsstatement() {
    var self=this;
    if (this.checkFormValid()) {
      var formatbrsupdatedata = this.getformatedbrsdata(this.brslist);

      this.BanksService.brsUpdate(formatbrsupdatedata)
      .then(res => {
        if (res.status) {
          self.resetbrsform();
          self.brsUpdateevent.emit({mode:"brsupdates",update:true});
          self.messageservice.showSuccessMessage({
            severity: 'success',
            summary: res.message
          });

        }
        else {
          self.messageservice.showMessage({
            severity: 'error',
            summary: 'Error', detail: res.message
          }, true);
        }
      });
    }
  }
  getformatedbrsdata(brslist) {
    var paymentheader = [];
    var banksTransdetails = [];
    var self = this;
    _.forEach(brslist, function (data: any) {
      var bdata = {

        "bankstmtdtlid": data.bankstmtid,
        "pymtrectid": data.pymtrectid,
        "reconstatus": data.reconstatus,
        "recondate": self.currentdate,
        "remarks": "",
        "lastupdatedby": self.userdetails.loginname,
        "lastupdateddt": self.currentdate,
      };
      var tdata = {
        "pymtrectid": data.pymtrectid,
        "recodate": self.currentdate,
        "recostatus": data.recostatus,
        "remarks": ""
      };
      banksTransdetails.push(bdata);
      paymentheader.push(tdata);

    });
    return {
      "bankstmtdtl": banksTransdetails,
      "pymtrectHdr": paymentheader
    }
  }
  checkFormValid() {
    return true;
  }
  resetbrsform()
  {
    this.brslist=[];
    var fromdate = new Date();
    var todate = new Date();
    this.fromtodate = [
      fromdate,
      todate
    ];
  }
  ngOnInit() {
  }

}
