/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* ============================================================
 * Plugin Core Init
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
$(document).ready(function() {
    'use strict';
    var resource = 'http://27.250.1.52:3003/gbooks/';
    
    /*$.post(resource+'country/findAll', {"emptyArray":[]}
      , function (data, textStatus, jqxhr) {
            console.log(data.length === 0 ? new Error("error").message : data);
    });*/
    
    $.ajax({
        type: 'POST', 
        url: resource+'country/findAll', 
        dataType: 'json',
        async:false,
    }).done(function (data) {
        var str= '<option value="--Please select--">--Please select--</option>';
        $.each(data,function(index, value){
            for(var i=0;i<value.length;i++){
                if(value[i].countryid){
                    str += '<option value="' + value[i].countryid + '">' + value[i].countryname + '</option>';
                }
            }
        });
        $("#reg_countryName").html(str);
    });
    
    $.ajax({
        type: 'POST', 
        url: resource+'city/findAll', 
        dataType: 'json',
        async:false,
    }).done(function (data) {
        var str= '<option value="--Please select--">--Please select--</option>';
        $.each(data,function(index, value){
            for(var i=0;i<value.length;i++){
                if(value[i].cityid){
                    str += '<option value="' + value[i].cityid + '">' + value[i].cityname + '</option>';
                }
            }
        });
        $("#reg_cityName").html(str);
                
    });
    
    $.ajax({
        type: 'POST', 
        url: resource+'state/findAll', 
        dataType: 'json',
        async:false,
    }).done(function (data) {
        var str= '<option value="--Please select--">--Please select--</option>';
        $.each(data,function(index, value){
            
            for(var i=0;i<value.length;i++){
                if(value[i].stateid){
                    str += '<option value="' + value[i].stateid + '">' + value[i].statename + '</option>';
                    console.log(value[i].countryid);
                }
            }
        });
        $("#reg_stateName").html(str);                
    });
    
    
    
    /*$.postJSON(resource+"country/findAll",{}, function(data){
        var options = '<option value="">-- Select please --</option>';
        for (var i = 0; i < data.length; i++) {
                 options += '<option value="' + data[i].value + '">' + data[i].label + '</option>';
        }
        $("#reg_countryName").html(options);
    });
    */
    
    /*$('#signup-form').validate({
        // Override to submit the form via ajaxssssssss
        submitHandler: function(form) {
            $('#signup-panel').portlet({
				refresh:true
			});
            $.ajax({
			     type:$(form).attr('method'),
			     url: $(form).attr('action'),
			     data: $(form).serialize(),
			     dataType: 'json',
			     success: function(data){
			     	console.log(data);
			       //Set your Succss Message
			       clearForm("Thank you for Contacting Us! We will be in touch");
			     },
			     error: function(err){
		            $('#signup-panel').portlet({
						refresh:false,
						//Set your ERROR Message
						error:"We could not send your message, Please try Again"
					});
			     }
		    });
		    return false; // required to block normal submit since you used ajax
        }
    });*/
    
    
    
    
    function clearForm(msg){
    	$('#signup-panel').html('<div class="alert alert-success" role="alert">'+msg+'</div>');
    }
    
    $( "#btnSignup" ).click(function() {
        var user = { 'username': $('[name=Email]').val()};
        localStorage.setItem('ngStorage-isAuthenticated', true );
        localStorage.setItem('ngStorage-user', JSON.stringify(user));
        window.location.href = "/Angular/index.html#/app/dashboard";
    });
    
    /*function loadState(countryid){
        $.ajax({
        type: 'POST', 
        url: resource+'state/findAll', 
        dataType: 'json',
        async:false,
    }).done(function (data) {
        //var countries = JSON.parse(data);
        //alert(countries.length);
        var str='';
        $.each(data,function(index, value){
            console.log(value.length);
            for(i=0;i<value.length;i++){
                if(value[i].countryid){
                    str += '<option value="' + value[i].countryid + '">' + value[i].countryname + '</option>';
                    console.log(value[i].countryid);
                }
            }
        });
        $("#reg_stateName").html(str);
                
    });
        
    }*/
    
    $('#signup-panel').portlet({
        onRefresh: function() {
        }
    });
});


